import os
from abc import ABC, abstractmethod
from typing import List, Any

import psutil
import torch
from class_registry import ClassRegistry

from src.constants.names import UNFREEZE_AFTER_EPOCH, LR_SHEDULE, SEMISUPERVISED_SHEDULE
from src.constants.miscellaneous import TRAIN, VALIDATION, TEST, TABLE_LOGGER_SEPARATOR, TRAILING_HASHTAG
from src.journaling.logs import log_named_dictionary
from src.journaling.table_logger import TableLogger
from src.models.metrics import SingleValueMetric, MetricAbstract
from src.models.workers import SimpleTrainerWorker, PatchDDSMEntropyMinWorker

callback_registry = ClassRegistry('callback_name')


class Callback(ABC):  # TODO: rename?
    """
    This class should be inherited by any callback that would be called on the end of the epoch
    """
    callback_name = None

    def __init__(self, trainer: SimpleTrainerWorker):
        self.trainer: SimpleTrainerWorker = trainer

    @abstractmethod
    def run(self):
        pass


@callback_registry.register
class ModelSaverCallback(Callback):
    """
    This callback saves model each `self.trainer.model_saving_period` number of steps, and updates best saved model
    when the loss value improved.
    If `self.trainer.model_saving_period` is zero, saves only best model.
    """
    callback_name = 'model_saver'

    def __init__(self, trainer: SimpleTrainerWorker):
        super(ModelSaverCallback, self).__init__(trainer)

        self.metric_best_value = None
        all_stopping_metrics = [m for m in self.trainer._validation_metrics if m.is_main()]
        assert len(all_stopping_metrics) == 1  # there should be only one relevant metric
        self.relevant_metric: SingleValueMetric = all_stopping_metrics[0]

        self.model_saving_period = self.trainer.params.get('save_each_n_step', 0)

        self.counter = 0

    def run(self):
        if self.model_saving_period and (self.counter % self.model_saving_period == 0):
            self._save_model('epoch_{}'.format(self.counter))

        if self.metric_best_value is None or \
                self.relevant_metric.is_better_than(self.metric_best_value):
            self.metric_best_value = self.relevant_metric.get_results()
            self.trainer._save_model('best')

        self.trainer._save_model('last')

        self.counter += 1


@callback_registry.register
class EarlyStoppingCallback(Callback):  # TODO: make general parent class from this and ModelSaverCallback
    callback_name = 'early_stopping'

    def __init__(self, trainer: SimpleTrainerWorker):
        super(EarlyStoppingCallback, self).__init__(trainer)

        self.metric_best_value = None
        relevant_metrics = [m for m in self.trainer._validation_metrics if m.is_main()]
        assert len(relevant_metrics) == 1
        self.relevant_metric: SingleValueMetric = relevant_metrics[0]

        self._es_epochs = self.trainer.params['early_stopping_epoch_number']

        self.counter = 0

    def run(self):
        if self._es_epochs == 0:
            return

        if self.metric_best_value is not None and \
                not self.relevant_metric.is_better_than(self.metric_best_value):
            if self.counter > self._es_epochs:
                self.trainer.continue_training = False
            self.counter += 1
        else:
            self.metric_best_value = self.relevant_metric.get_results()
            self.counter = 0


@callback_registry.register
class MetricLogger(Callback):
    callback_name = 'metric_logger'

    def __init__(self, trainer: SimpleTrainerWorker):
        super(MetricLogger, self).__init__(trainer)
        self.counter = 0
        self.logger = self.trainer.logger
        self.tensorboard_logger = self.trainer.tensorboard_logger

        self.counter = 0

    def run(self):
        self._log_metrics(epoch_number=self.counter)
        self.counter += 1

    def _log_metrics(self, metric_lists: List[List[MetricAbstract]] = None, names: List[str] = None,
                     epoch_number: int = None):
        """
        Goes through the metrics and their names one by one ang logs their values.
        There should be equal number of elements in metrics and names lists or the method raises an exception.
        :param metric_lists:
        :param names:
        :param epoch_number: If not none, the epoch number is also logged.
        :return:
        """
        if metric_lists is None:
            metric_lists = [self.trainer._train_metrics, self.trainer._validation_metrics, self.trainer._test_metrics]
            if names is None:
                names = [TRAIN, VALIDATION, TEST]

        if epoch_number is not None:
            self.logger.info('\n{}\nEpoch {}:\n{}'.format(TRAILING_HASHTAG, epoch_number, TRAILING_HASHTAG))

        for metrics, name in zip(metric_lists, names):
            for metric in metrics:
                metric.get_and_log_results(self.tensorboard_logger, self.logger, tag=name, abscissa=epoch_number)


@callback_registry.register
class ModelLogger(Callback):
    callback_name = 'model_logger'

    def __init__(self, trainer: SimpleTrainerWorker):
        super(ModelLogger, self).__init__(trainer)

        self.tensorboard_logger = self.trainer.tensorboard_logger
        self.tag = os.path.join(self.trainer.run_path_resolver.experiment_name, self.trainer.run_path_resolver.run_name)

        self.counter = 0

    def run(self):
        self.tensorboard_logger.log_model(name=self.tag, model=self.trainer._model, abscissa=self.counter)

        self.counter += 1


@callback_registry.register
class ModelFreezerCallback(Callback):
    callback_name = 'model_freezer'

    def __init__(self, trainer: SimpleTrainerWorker):
        super(ModelFreezerCallback, self).__init__(trainer)

        self.model = trainer._model
        self.unfreeze_on_epoch: int = trainer.params[UNFREEZE_AFTER_EPOCH]
        self.counter = 0

    def run(self):
        if self.counter == 0:
            self.model.feature_extractor_update_trainability(False)
        if self.counter > self.unfreeze_on_epoch:
            self.model.feature_extractor_update_trainability(True)
        self.counter += 1


@callback_registry.register
class ResourceUsageLoggerCallback(Callback):
    callback_name = 'resource_logger'

    def __init__(self, trainer: SimpleTrainerWorker):
        super(ResourceUsageLoggerCallback, self).__init__(trainer)

        self.counter = 0

        self.run_dir_path = trainer.run_path
        self.table_file_name = 'resource_usage.csv'
        self.column_names = ['EPOCH', 'CPU', 'MEMORY', 'GPU_TOTAL', 'GPU_CACHED']

        self.table_logger = TableLogger(run_dir_path=self.run_dir_path, file_name=self.table_file_name,
                                        column_names=self.column_names, separator=TABLE_LOGGER_SEPARATOR)
        self.logger = self.trainer.logger
        self.tensorboard_logger = trainer.tensorboard_logger
        self.process_info = psutil.Process(os.getpid())

    def run(self):
        # getting values
        virt_mem = self.process_info.memory_info()[0] / 2. ** 30  # usage in GB
        cpu = self.process_info.cpu_percent()
        cuda_is_used = self.trainer.device.type == 'cuda'
        if cuda_is_used:
            gpu_total = torch.cuda.memory_allocated(self.trainer.device)
            gpu_cached = torch.cuda.memory_cached(self.trainer.device) - gpu_total
        else:
            gpu_total, gpu_cached = 0.0, 0.0

        values = [self.counter, cpu, virt_mem, gpu_total, gpu_cached]
        self.table_logger.append_row(values)

        # logging them
        values_dict = {k: float(v) for k, v in zip(self.column_names, values)}
        self.tensorboard_logger.log_named_dictionary(values_dict, self.counter)
        log_named_dictionary(self.logger, 'Resources usage', values_dict)

        self.counter += 1


@callback_registry.register
class TrainParametersLoggerCallback(Callback):
    callback_name = 'param_logger'

    def __init__(self, trainer: SimpleTrainerWorker):
        super(TrainParametersLoggerCallback, self).__init__(trainer)

        self.counter = 0
        self.tensorboard_logger = trainer.tensorboard_logger
        self.optimizer = trainer.optimizer

    def run(self):
        # getting values

        values_dict = self.optimizer.param_groups[0].copy()
        values_dict = {k: v for k, v in values_dict.items() if type(v) == float or type(v) == int}

        self.tensorboard_logger.log_named_dictionary(values_dict, self.counter)

        self.counter += 1


@callback_registry.register
class LearningRateDecreaseCallback(Callback):
    callback_name = 'decrease_lr'

    def __init__(self, trainer: SimpleTrainerWorker):
        super(LearningRateDecreaseCallback, self).__init__(trainer)

        self.optimizer = trainer.optimizer
        self.lr_shedule: dict = trainer.params[LR_SHEDULE]
        self.counter = 0

    def run(self):
        if self.counter in self.lr_shedule:
            for param_group in self.optimizer.param_groups:
                param_group['lr'] = self.lr_shedule[self.counter]

        self.counter += 1


@callback_registry.register
class KerberRenewCallback(Callback):
    callback_name = 'renew_task'

    def __init__(self, trainer: SimpleTrainerWorker):
        super(KerberRenewCallback, self).__init__(trainer)
        self.counter = 0

    def run(self):
        if self.counter % 50 == 0:
            os.system('kinit -R')
        self.counter += 1


@callback_registry.register
class SemiSupervisedSwitchCallback(Callback):
    callback_name = 'semisupervised_switch'

    def __init__(self, trainer: SimpleTrainerWorker):
        super(SemiSupervisedSwitchCallback, self).__init__(trainer)
        assert type(self.trainer) == PatchDDSMEntropyMinWorker
        self.semisupervised_shedule: dict = trainer.params[SEMISUPERVISED_SHEDULE]
        self.counter = 0

    def run(self):
        if self.counter in self.semisupervised_shedule:
            self.trainer.supervised_is_on = self.semisupervised_shedule[self.counter][0]
            self.trainer.unsupervised_is_on = self.semisupervised_shedule[self.counter][1]

            if self.semisupervised_shedule[self.counter][2] is not None:
                self.trainer.unsupervised_weight = self.semisupervised_shedule[self.counter][2]

        self.counter += 1
