from abc import ABC, abstractmethod

import torch
from class_registry import ClassRegistry
from efficientnet_pytorch import EfficientNet
from torch import nn
from torchvision import models

from src.constants import names
from src.utils import set_torch_seed

model_registry = ClassRegistry('net_name')


class BaseModelAbstract(nn.Module, ABC):
    """
    Abstract class to be inherited by the classes implementing models.
    Model should implement calculation of the feature map, output layer predictions.

    Feature extractor and last layer should be separate modules to allow freezing the parameters of the feature extractor.
    If possible it should allow loading pretrained weights.
    """

    # This value will be used to select which Model class to instantiate from the model register.
    net_name = None

    def __init__(self, params: dict):
        super(BaseModelAbstract, self).__init__()

        # It is made separate module to make it easier to freeze it's parameters
        self.feature_extractor: nn.Module = None  # TODO: make private?
        self.classifier: nn.Module = None

        self.num_ftrs = None
        self.number_of_classes = None  # TODO: remove?

        self.torch_seed = params[names.TORCH_SEED]
        self.pretrained = params['use_pretrained_weights']
        self.number_of_classes = params[names.NUMBER_OF_CLASSES]

        set_torch_seed(self.torch_seed)

    def create_classifier(self):
        self.classifier = nn.Linear(self.num_ftrs, self.number_of_classes)

    def get_feature_map(self, input_: torch.Tensor) -> torch.Tensor:
        return self.feature_extractor(input_)

    def get_flatten_feature_map(self, input_: torch.Tensor) -> torch.Tensor:
        return self.get_feature_map(input_)

    def forward(self, _input: torch.Tensor) -> torch.Tensor:
        _input = self.get_flatten_feature_map(_input)
        _input = self.classifier(_input)
        return _input

    def feature_extractor_update_trainability(self, is_trainable=True):
        """
        This method is freezing(or unfreezing) the weights of the feature extractor, so that only the last
        fully-connected layer is trained
        :param is_trainable:
        :return:
        """
        for m in self.feature_extractor.parameters():
            m.requires_grad = is_trainable


@model_registry.register
class FullyConnectedNet(BaseModelAbstract):
    """
    Works only with mnist data set.
    Flattens one channel of the input image anf applies fully connected layer twice with tanh activation.
    """
    net_name = 'fcn'

    def __init__(self, params: dict):
        super(FullyConnectedNet, self).__init__(params)

        self.tanh = torch.nn.Tanh()
        size_of_image = params['image_size_flatten']
        hidden_layer_size = params['hidden_layer_size']

        self.avgpool = nn.AdaptiveAvgPool2d((5, 5))

        self.batch_norm = nn.BatchNorm1d(hidden_layer_size)

        self.feature_extractor = nn.Linear(25, hidden_layer_size)

        self.num_ftrs = hidden_layer_size
        self.create_classifier()

    def get_feature_map(self, input_: torch.Tensor):
        input_ = self.avgpool(input_)
        input_ = torch.flatten(input_[:, 0, ...], start_dim=1)  # transform input to fit he fully-connected layer
        return self.tanh(self.batch_norm(self.feature_extractor(input_)))
        return input_


@model_registry.register
class SimpleConvNet(BaseModelAbstract):
    """
    Works only with mnist data set.
    Flattens one channel of the input image anf applies fully connected layer twice with tanh activation.
    """
    net_name = 'simple_cnn'

    def __init__(self, params: dict):
        super(SimpleConvNet, self).__init__(params)
        self.hidden_layer_size = params['hidden_layer_size']

        self.feature_extractor = nn.Sequential(
            torch.nn.Conv2d(1, 24, kernel_size=5, stride=1, padding=1),
            torch.nn.ReLU(),
            torch.nn.MaxPool2d(kernel_size=2),
            torch.nn.Conv2d(24, 48, kernel_size=5, stride=1, padding=1),
            torch.nn.ReLU(),
            torch.nn.MaxPool2d(kernel_size=2),
            torch.nn.Conv2d(48, self.hidden_layer_size, kernel_size=5, stride=1, padding=1),
            torch.nn.ReLU(),
            torch.nn.AdaptiveMaxPool2d((1, 1)),  # TODO: move outside?
        )

        self.num_ftrs = self.hidden_layer_size
        self.create_classifier()

    def get_flatten_feature_map(self, input_: torch.Tensor):
        return torch.flatten(self.get_feature_map(input_), start_dim=1)

    def forward(self, input_: torch.Tensor):
        input_ = input_[:, 0, ...].unsqueeze(1)
        input_ = self.get_flatten_feature_map(input_)
        input_ = self.classifier(input_)

        return input_


class ResNetXX(BaseModelAbstract):
    """
    Only for ResNet 18 and 34
    """
    net_name = None  # TODO: parametrise to use other resnets

    def __init__(self, params: dict):
        super(ResNetXX, self).__init__(params)
        model = self._get_model(pretrained=self.pretrained)

        self.feature_extractor = nn.Sequential(
            model.conv1,
            model.bn1,
            model.relu,
            model.maxpool,
            model.layer1,
            model.layer2,
            model.layer3,
            model.layer4,
            model.avgpool)  # TODO: check whether max would be better, or maybe percentile?

        self.num_ftrs = model.fc.in_features
        self.create_classifier()

    def get_flatten_feature_map(self, input_: torch.Tensor) -> torch.Tensor:
        input_ = self.get_feature_map(input_)
        input_ = input_.reshape(input_.size(0), -1)
        return input_

    @abstractmethod
    def _get_model(self, pretrained: bool):
        pass


@model_registry.register
class ResNet18(ResNetXX):
    net_name = 'resnet18'  # TODO: parametrise to use other resnets

    def _get_model(self, pretrained: bool):
        return models.resnet18(pretrained=pretrained)


@model_registry.register
class ResNet34(ResNetXX):
    net_name = 'resnet34'  # TODO: parametrise to use other resnets

    def _get_model(self, pretrained: bool):
        return models.resnet34(pretrained=pretrained)


@model_registry.register
class ResNet50(ResNetXX):
    net_name = 'resnet50'  # TODO: parametrise to use other resnets

    def _get_model(self, pretrained: bool):
        return models.resnet50(pretrained=pretrained)

@model_registry.register
class ResNet101(ResNetXX):
    net_name = 'resnet101'  # TODO: parametrise to use other resnets

    def _get_model(self, pretrained: bool):
        return models.resnet101(pretrained=pretrained)

@model_registry.register
class VGG11_BN(BaseModelAbstract):
    net_name = 'vgg11'

    def __init__(self, params: dict):
        super(VGG11_BN, self).__init__(params)
        model = models.vgg11_bn(pretrained=self.pretrained)
        # model = models.vgg11_bn(pretrained=self.pretrained, num_classes=params['number_of_classes'])
        self.avgpool = nn.AdaptiveAvgPool2d((7, 7))

        self.feature_extractor = model.features

        self.num_ftrs = model.classifier[0].in_features
        self.create_classifier()

    def get_flatten_feature_map(self, input_: torch.Tensor) -> torch.Tensor:
        input_ = self.get_feature_map(input_)
        input_ = self.avgpool(input_)
        input_ = input_.view(input_.size(0), -1)

        return input_


@model_registry.register
class MobileNetV2(BaseModelAbstract):
    net_name = 'mobilenet_v2'

    def __init__(self, params: dict):
        super(MobileNetV2, self).__init__(params)
        model = models.mobilenet.mobilenet_v2(pretrained=self.pretrained)

        self.feature_extractor = model.features
        self.classifier = nn.Sequential(
            nn.Dropout(0.2),
            nn.Linear(model.last_channel, params['number_of_classes']),
        )

        self.num_ftrs = model.last_channel

    def get_feature_map(self, input_: torch.Tensor) -> torch.Tensor:
        return self.feature_extractor(input_)

    def get_flatten_feature_map(self, input_: torch.Tensor) -> torch.Tensor:
        input_ = self.get_feature_map(input_)
        input_ = input_.mean([2, 3])

        return input_


class EfficientNetBX(BaseModelAbstract):
    net_name = 'efficientnetbx'
    model_id = None

    def __init__(self, params: dict):
        super(EfficientNetBX, self).__init__(params)
        model = EfficientNet.from_pretrained('efficientnet-b' + str(self.model_id), num_classes=2)
        model.forward = model.extract_features

        self.feature_extractor = model

        self.num_ftrs = model._fc.in_features
        del model._fc
        self.create_classifier()


    def create_classifier(self):
        self.classifier = nn.Sequential(
            nn.Dropout(0.2),
            nn.Linear(self.num_ftrs, self.number_of_classes),
        )

    def get_flatten_feature_map(self, input_: torch.Tensor) -> torch.Tensor:
        bs = input_.size(0)
        input_ = self.get_feature_map(input_)

        # Pooling and final linear layer
        input_ = self.feature_extractor._avg_pooling(input_)
        input_ = input_.view(bs, -1)

        return input_


@model_registry.register
class EfficientNetB0(EfficientNetBX):
    net_name = 'efficientnetb0'
    model_id = 0

@model_registry.register
class EfficientNetB1(EfficientNetBX):
    net_name = 'efficientnetb1'
    model_id = 1

@model_registry.register
class EfficientNetB2(EfficientNetBX):
    net_name = 'efficientnetb2'
    model_id = 2

@model_registry.register
class EfficientNetB3(EfficientNetBX):
    net_name = 'efficientnetb3'
    model_id = 3

@model_registry.register
class EfficientNetB4(EfficientNetBX):
    net_name = 'efficientnetb4'
    model_id = 4
