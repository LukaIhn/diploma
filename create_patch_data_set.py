import os
from math import floor, ceil

import numpy as np

import pandas as pd
from PIL import Image
from tqdm import tqdm

from create_small_dataset_from_dicoms import METAFEATURE_STORAGE_NAME
from src.data.CBIS.cbis_directory_structure_helper import PNGMetafeatureColumns, PngImageType, AbnormalityType, \
    DataSetTypes, DICOMMetadataCols, Pathology, PatchMetadataCols, PatchDatasetFileNames
from src.utils import save_data_frame_as_hdf, read_hdf, check_dir

PATCH_SIZE = 900


class PatchDataSetCreator:
    def __init__(self, full_images_dir, patches_dir):
        self.full_images_dir = full_images_dir
        self.patches_dir = patches_dir

        check_dir(self.patches_dir, create=True, recursively=True)

        self.metadata: pd.DataFrame = read_hdf(os.path.join(self.full_images_dir, METAFEATURE_STORAGE_NAME))

    def create(self):
        """
        For each annotated mask (row from metadata) it loads full image (png), crops patch around masked area
        and stores patch in PNG format.
        :return:
        """
        for i, row in tqdm(self.metadata.iterrows(), total=len(self.metadata)):
            full_image_path = os.path.join(row[PNGMetafeatureColumns.CASE_DIR], PngImageType.FULL).replace('/PNGS/',
                                                                                                           '/PNGS_FINAL/') + '.png'
            mask_path = os.path.join(row[PNGMetafeatureColumns.CASE_DIR], row[PNGMetafeatureColumns.MASK_NAME]).replace(
                '/PNGS/', '/PNGS_FINAL/')

            # open and crop
            full_image = Image.open(full_image_path)
            mask = Image.open(mask_path)
            crop_array = self.get_crop(full_image, mask)

            # save crop
            patch_path = self.patch_file_path_from_row(row)
            patch = Image.fromarray(crop_array)
            patch.save(patch_path)

    def patch_file_path_from_row(self, row) -> str:
        return os.path.join(self.patches_dir, self.patch_file_name_from_row(row))

    def patch_file_name_from_row(self, row) -> str:
        return '|'.join([row[PNGMetafeatureColumns.PATIENT_ID],
                         row[DICOMMetadataCols.LEFT_OR_RIGHT_BREAST],
                         row[DICOMMetadataCols.IMAGE_VIEW],
                         row[PNGMetafeatureColumns.MASK_NAME]])

    def get_crop(self, full_image: np.ndarray, mask: np.ndarray) -> np.ndarray:
        """
        Finds center of mask and then cuts patch with size PATCH_SIZE around this center point.
        It is assumed that smallest image dimension is bigger than PATCH_SIZE
        :param full_image:
        :param mask:
        :return:
        """
        pos = np.where(mask)

        mask_np_array = np.asarray(mask)
        assert len(mask_np_array.shape) == 2
        x_max, y_max = mask_np_array.shape[0] - 1, mask_np_array.shape[1] - 1

        xmin_mask = np.min(pos[0])
        xmax_mask = np.max(pos[0])
        ymin_mask = np.min(pos[1])
        ymax_mask = np.max(pos[1])

        x_mask_center = (xmin_mask + xmax_mask) // 2
        y_mask_center = (ymin_mask + ymax_mask) // 2

        half_patch_size = PATCH_SIZE / 2
        half_patch_size_floored = floor(half_patch_size)
        half_patch_size_ceiled = ceil(half_patch_size)

        assert not ((x_mask_center - half_patch_size_floored < 0) and (x_mask_center + half_patch_size_ceiled > x_max))
        if (x_mask_center - half_patch_size_floored < 0):  # crosses left border of image
            x_mask_center -= x_mask_center - half_patch_size_floored
        elif (x_mask_center + half_patch_size_ceiled > x_max):  # crosses right border of image
            x_mask_center -= x_mask_center + half_patch_size_ceiled - x_max

        assert not ((y_mask_center - half_patch_size_floored < 0) and (y_mask_center + half_patch_size_ceiled > y_max))
        if (y_mask_center - half_patch_size_floored < 0):  # crosses left border of image
            y_mask_center -= y_mask_center - half_patch_size_floored
        elif (y_mask_center + half_patch_size_ceiled > y_max):  # crosses right border of image
            y_mask_center -= y_mask_center + half_patch_size_ceiled - y_max

        patch = np.asarray(full_image)[x_mask_center - half_patch_size_floored:x_mask_center + half_patch_size_ceiled,
                y_mask_center - half_patch_size_floored:y_mask_center + half_patch_size_ceiled]

        # TODO: maybe remove
        if not len(np.unique(patch)) > 1:  ## check it is not monotonic image
            print('_____')

        return patch

    def to_numpy(self, directory_to_store_numpy_files) -> None:
        """
        Creates numpy files storing data and labels for train and test sets
        :param directory_to_store_numpy_files:
        :return:
        """
        self._update_metadata()

        for is_test in [True, False]:
            df = self.metadata[self.metadata[PatchMetadataCols.IS_TEST] == is_test]
            df = df[df[DICOMMetadataCols.PATHOLOGY] != Pathology.BENIGN_WITHOUT_CALLBACK]

            df.reset_index(drop=True, inplace=True)

            data_set_type = 'test' if is_test else 'train'
            images_path = PatchDatasetFileNames.data_for(data_set_type, directory_to_store_numpy_files)
            labels_path = PatchDatasetFileNames.label_for_set(data_set_type, directory_to_store_numpy_files)
            metadata_path = PatchDatasetFileNames.metadata_for(data_set_type, directory_to_store_numpy_files)

            save_data_frame_as_hdf(df, metadata_path)

            images_memmap = np.memmap(images_path, dtype='uint8', mode='w+',
                                      shape=(len(df), PATCH_SIZE, PATCH_SIZE))
            labels_memmap = np.memmap(labels_path, dtype='uint8', mode='w+',
                                      shape=(len(df)))

            self._fill_memmaps(images_memmap, labels_memmap, df)

    def _fill_memmaps(self, images: np.memmap, labels: np.memmap, metadata_df: pd.DataFrame) -> None:
        for i, row in tqdm(metadata_df.iterrows(), total=len(metadata_df)):
            patch_image_path = self.patch_file_path_from_row(row)
            image = np.asarray(Image.open(patch_image_path))

            images[i, ...] = np.asarray(image)
        labels[:] = metadata_df[PatchMetadataCols.NEW_LABELS].values

    def _update_metadata(self) -> None:
        """
        Updates separation into test and train so that in there won't be any patient that is in both test and train
        and using both task simultaneously could be possible.
        It also creates new label {0;1;2;3}: 0,1 - benign, 2,3 - malignant, 0,2 - mass, 1,3 - calcification
        :return:
        """
        test_patient_ids = []
        for patient_id, group in self.metadata.groupby(DICOMMetadataCols.PATIENT_ID):
            if any(group['data_set_original_type'].values == 'test'):
                test_patient_ids.append(patient_id)

        self.metadata[PatchMetadataCols.IS_TEST] = self.metadata[DICOMMetadataCols.PATIENT_ID].isin(
            test_patient_ids)

        is_malignant = (self.metadata[DICOMMetadataCols.PATHOLOGY].values == Pathology.MALIGNANT) \
            .astype('uint8')
        is_calcification = (self.metadata[DICOMMetadataCols.ABNORMALITY_TYPE] == AbnormalityType.CALCIFICATION) \
            .astype('uint8')

        self.metadata[PatchMetadataCols.NEW_LABELS] = 2 * (is_malignant) + is_calcification


if __name__ == '__main__':
    ds_creator = PatchDataSetCreator('/media/wm/Windows8_OS/CBIS_FULL/PNGS_FINAL',
                                     '/media/wm/Windows8_OS/CBIS_FULL/PATCHES')
    ds_creator.create()
    ds_creator.to_numpy('/home/wm/diploma/diploma/data/processed/patch_ddsm_cleaned')
