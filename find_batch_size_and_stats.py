import click
import os

import torch

from src.constants import names
from src.constants.miscellaneous import DATA
from src.experiment_runner import ExperimentRunner
from src.journaling.logs import initialize_logger
from src.models.workers import ModelBatchSizeAndEvaluationTimeAanlyser
from src.utils import load_experiment_config, update_dictionary

PATH_TO_PROJECT = os.path.join(os.path.dirname(__file__))

class BatchSizeSearchRunner(ExperimentRunner):
    worker_class = ModelBatchSizeAndEvaluationTimeAanlyser

@click.command()
@click.option('--contact-me', type=str, default=None, help='This is visible in htop so that others would know who ran the process.')
@click.option('--use-cuda', type=bool, default=False)
@click.option('--seed', default=42, type=int, help='Seed for experiment reproduction.')
@click.option('--experiment_name', default='batch_search', type=str, help='Path of the experiment in the models directory.')
@click.option('--dtype', default='float', type=str, help='Which dtype to use.')
@click.option('--data_directory', default=os.path.join(PATH_TO_PROJECT, DATA), type=str,
              help='Location of the directory with the data.')
@click.option('--gc-aggressive', default=False, type=bool, help='Chooses whether to collect garbage aggressively.')
@click.option('--reproducible', default=False, type=bool, help='Whether to enforce nondeterminism.')
def main(**kwargs):
    if kwargs['reproducible']:
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
    experiment_directory = os.path.join(PATH_TO_PROJECT, names.MODELS, kwargs['experiment_name'])
    initialize_logger(os.path.join(experiment_directory, 'experiment.log'))

    config = load_experiment_config(kwargs['experiment_name'])
    update_dictionary(source_dict=kwargs, target_dict=config)

    runner = BatchSizeSearchRunner(**config)

    runner.run_grid_search()


if __name__ == '__main__':
    # try:
    main()
    # except Exception as e:
    #     logger.error('Running script{} ended with exception {}'.format(__file__, e))


