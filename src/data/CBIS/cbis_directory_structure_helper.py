import os
from enum import Enum
import pandas as pd


class EnumFromString(Enum):
    @classmethod
    def value_of(cls, value):
        for m, mm in cls.__members__.items():
            if m == value.upper():
                return mm

    def __str__(self):
        return self.value


class DataSetTypes(EnumFromString):
    TRAIN = 'training'
    TEST = 'test'


class AbnormalityType():
    CALCIFICATION_SHORT = 'calc'
    CALCIFICATION = 'calcification'
    MASS = 'mass'


class CbisDicomImageType(EnumFromString):
    FULL = 'image_file_path'
    CROPPED = 'ROI_mask_file_path'
    MASK = 'cropped_image_file_path'


class PngImageType:
    FULL = 'full'
    CROPPED = 'cropped'
    MASK = 'mask'


class ViewType(EnumFromString):
    CC = 'CC'
    MLO = 'MLO'


class BreastSideType(EnumFromString):
    LEFT = 'LEFT'
    RIGHT = 'RIGHT'


class DICOMMetadataCols:
    ROI_MASK_FILE_PATH = 'ROI_mask_file_path'
    ABNORMALITY_ID = 'abnormality_id'
    ABNORMALITY_TYPE = 'abnormality_type'
    ASSESSMENT = 'assessment'
    BREAST_DENSITY = 'breast_density'
    CALC_DISTRIBUTION = 'calc_distribution'
    CALC_TYPE = 'calc_type'
    DATA_SET_ORIGINAL_TYPE = 'data_set_original_type'
    IMAGE_FILE_PATH = 'image_file_path'
    IMAGE_VIEW = 'image_view'
    LEFT_OR_RIGHT_BREAST = 'left_or_right_breast'
    MASS_MARGINS = 'mass_margins'
    MASS_SHAPE = 'mass_shape'
    PATHOLOGY = 'pathology'
    PATIENT_ID = 'patient_id'
    SUBTLETY = 'subtlety'
    CROPPED_IMAGE_FILE_PATH = 'cropped_image_file_path'


class PNGMetafeatureColumns:
    CASE_DIR = 'case_dir'
    MASK_NAME = 'mask_name'
    PATIENT_ID = 'patient_id'


class CbisDetectionColumns:
    CASE_NAME = 'case_name'
    CASE_NAME_ORDER_ID = 'case_name_order_id'
    SCALES = 'scales'
    X_MIN = 'xmin'
    X_MAX = 'xmax'
    Y_MIN = 'ymin'
    Y_MAX = 'ymax'

    IS_TEST = 'is_test'


class PatchMetadataCols:
    IS_TEST = 'is_test'
    NEW_LABELS = 'new_labels'


class Pathology:
    BENIGN = 'BENIGN'
    BENIGN_WITHOUT_CALLBACK = 'BENIGN_WITHOUT_CALLBACK'
    MALIGNANT = 'MALIGNANT'


class PatchDatasetFileNames:
    DATA = '_data.npy'
    LABELS = '_labels.npy'
    METADATA = '_meta.h5'

    @classmethod
    def data_for(cls, set: str, dir='') -> str:
        return os.path.join(dir, set + cls.DATA)

    @classmethod
    def label_for_set(cls, set: str, dir='') -> str:
        return os.path.join(dir, set + cls.LABELS)

    @classmethod
    def metadata_for(cls, set: str, dir='') -> str:
        return os.path.join(dir, set + cls.METADATA)


IMAGES_DIR = 'CBIS-DDSM'
PATIENT_IMAGES_DIR = 'PATIENT-PNGS'
CSVS_DIR = 'CSVS'

DIRECTORY_NAME_TEMPLATE = '{AbnormalityType}-{DataSetType}_P_{PatientId}_{Side}_{MammographyView}'
CASE_DESCRIPTION_FILE_NAME_TEMPLATE = '{AbnormalityType}_case_description_{DataSetType}_set.csv'


def get_png_dir(data_root_dir: str, patient_id: str, view: ViewType, left_or_right: BreastSideType):
    name = '|'.join([patient_id, view.value, left_or_right.value])
    return os.path.join(data_root_dir, PATIENT_IMAGES_DIR, name).replace('\n', '')


class CBISDicomPathHelper:
    """
    Class that helps with reading and raw CBIS DDSM data.
    More at https://wiki.cancerimagingarchive.net/display/Public/CBIS-DDSM
    Data should be downloaded with NBIIA data retriever, using "Classic Directory Name"
    """

    def __init__(self, data_root_source: str, data_set_type: DataSetTypes, malignancy_type: AbnormalityType,
                 data_root_target: None):
        """
        :param data_root_source: directory in which data from CBIS DDSM are downloaded.

        :return:
        """
        self.data_root_dir = data_root_source
        self.data_destination_root_dir = data_root_target if data_root_target else data_root_source

        self.data_set_type = data_set_type
        self.malignancy_type = malignancy_type

        self._data_info = pd.read_csv(self.case_descripion_file_name())
        self._data_info.columns = [c.replace(' ', '_') for c in self._data_info.columns]
        # self._data_info = self._data_info[(self._data_info['patient_id'] == 'P_01039') & (self._data_info['image_view'] == 'MLO') &  (self._data_info['left_or_right_breast'] == 'RIGHT')]
        # self._data_info = self._data_info[(self._data_info.patient_id == 'P_00353') & (self._data_info['left_or_right_breast'] == 'LEFT')]# & (self._data_info['image_view'] == 'CC')]

    def case_descripion_file_name(self, with_root=True):
        name = CASE_DESCRIPTION_FILE_NAME_TEMPLATE \
            .replace('{AbnormalityType}', self.malignancy_type.value) \
            .replace('{DataSetType}', self.data_set_type.name.lower())
        if with_root:
            name = os.path.join(self.data_root_dir, CSVS_DIR, name)
        return name

    def get_patient_image_path(self, index_in_df: int, image_type: CbisDicomImageType):
        name = self._data_info.iloc[index_in_df][image_type.value]
        return os.path.join(self.data_root_dir, IMAGES_DIR, name).replace('\n', '')

    def get_file_png_directory_by_index(self, index_in_df: int):
        row = self._data_info.iloc[index_in_df]
        patient_id = row['patient_id']
        view = ViewType.value_of(row['image_view'])
        left_or_right = BreastSideType.value_of(row['left_or_right_breast'])

        return get_png_dir(self.data_destination_root_dir, patient_id, view, left_or_right)

    def get_mask_df(self, index_in_df: int):
        return self._data_info.iloc[index_in_df:index_in_df + 1]


class CBISDicomPathHelper:
    """
    Class that helps with reading and raw CBIS DDSM data.
    More at https://wiki.cancerimagingarchive.net/display/Public/CBIS-DDSM
    Data should be downloaded with NBIIA data retriever, using "Classic Directory Name"
    """

    def __init__(self, data_root_source: str, data_set_type: DataSetTypes, malignancy_type: AbnormalityType,
                 data_root_target: None):
        """
        :param data_root_source: directory in which data from CBIS DDSM are downloaded.

        :return:
        """
        self.data_root_dir = data_root_source
        self.data_destination_root_dir = data_root_target if data_root_target else data_root_source

        self.data_set_type = data_set_type
        self.malignancy_type = malignancy_type

        self._data_info = pd.read_csv(self.case_descripion_file_name())
        self._data_info.columns = [c.replace(' ', '_') for c in self._data_info.columns]
        # self._data_info = self._data_info[(self._data_info['patient_id'] == 'P_01039') & (self._data_info['image_view'] == 'MLO') &  (self._data_info['left_or_right_breast'] == 'RIGHT')]
        # self._data_info = self._data_info[(self._data_info.patient_id == 'P_00353') & (self._data_info['left_or_right_breast'] == 'LEFT')]# & (self._data_info['image_view'] == 'CC')]

    def case_descripion_file_name(self, with_root=True):
        name = CASE_DESCRIPTION_FILE_NAME_TEMPLATE \
            .replace('{AbnormalityType}', self.malignancy_type.value) \
            .replace('{DataSetType}', self.data_set_type.name.lower())
        if with_root:
            name = os.path.join(self.data_root_dir, CSVS_DIR, name)
        return name

    def get_patient_image_path(self, index_in_df: int, image_type: CbisDicomImageType):
        name = self._data_info.iloc[index_in_df][image_type.value]
        return os.path.join(self.data_root_dir, IMAGES_DIR, name).replace('\n', '')

    def get_file_png_directory_by_index(self, index_in_df: int):
        row = self._data_info.iloc[index_in_df]
        patient_id = row['patient_id']
        view = ViewType.value_of(row['image_view'])
        left_or_right = BreastSideType.value_of(row['left_or_right_breast'])

        return get_png_dir(self.data_destination_root_dir, patient_id, view, left_or_right)

    def get_mask_df(self, index_in_df: int):
        return self._data_info.iloc[index_in_df:index_in_df + 1]
