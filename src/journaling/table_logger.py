import os
from typing import List

from src.utils import check_dir


class TableLogger():
    def __init__(self, run_dir_path: str, file_name: str, column_names: List[str], separator=','):
        assert check_dir(run_dir_path, create=False), 'Incorrect run directory.'

        self.run_dir_path = run_dir_path
        self.file_name = file_name
        self.file_path = os.path.join(run_dir_path, file_name)

        self.separator = separator
        self.column_names = column_names

        # initialize first row with column names
        self.append_row(row=column_names)

    def append_row(self, row: list):
        with open(self.file_path, 'a') as file:
            message = 'Trying to append incorrect number of elements to the table {}'.format(self.file_path)
            assert len(self.column_names) == len(row), message

            row = [str(v) for v in row]

            str_row = self.separator.join(row)
            str_row += '\n'
            file.write(str_row)
