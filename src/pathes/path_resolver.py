import hashlib
import os

from src.constants import names
from src.constants.miscellaneous import PARAM_SEPARATOR, DATA, PARAM_KEY_VALUE_SEPARATOR
from src.utils import check_dir

_PROJECT_PATH = os.path.join(os.path.dirname(__file__), '..', '..')


class RunPathResolver:
    def __init__(self, **config):
        self.config = config

        self.initialize_path_variables()

        if config.get('create_directories', True):
            check_dir(self.get_run_path(), create=True)

    def initialize_path_variables(self):
        run_number = self.config[names.RUN_NUMBER]
        experiment_name = self.config[names.EXPERIMENT_NAME]

        key_value_paires = ['{}{}{}'.format(key, PARAM_KEY_VALUE_SEPARATOR, value) for key, value in
                            self.config[names.GRIDSEARCH_UPDATED].items()
                            if key != names.RUNHASH]

        self.experiment_name = experiment_name
        self.experiment_path = os.path.join(_PROJECT_PATH, names.MODELS, experiment_name)
        self.run_name = PARAM_SEPARATOR.join([f'run_number{PARAM_KEY_VALUE_SEPARATOR}{run_number}'] + key_value_paires)
        self.run_hash = self.get_experiment_hash(self.config)
        if self.config.get(names.GET_DATA_HASH, True):
            self.data_hash = self.get_data_hash(self.config)

    def get_run_path(self):
        return os.path.join(self.experiment_path, self.run_hash, self.run_name)

    @staticmethod
    def get_data_hash(config):
        result = ''
        for key in [names.NUMBER_OF_FOLDS, names.SEED, names.BALANCE_CLASSES]:
            result += key + '_' + str(config[DATA][key])

        hash_object = hashlib.md5(result.encode())
        return hash_object.hexdigest()

    @staticmethod
    def get_experiment_hash(config):
        result = ''
        keys = [k for k in config.keys() if (k != DATA) & (k != names.TORCH_SEED)]
        keys.sort()

        for key in keys:
            result += key + ':' + str(config[key])

        hash_object = hashlib.md5(result.encode())
        return hash_object.hexdigest()


# TODO: try remove these classes
class NoHashExperimentHashResolver(RunPathResolver):
    def get_run_path(self):
        return os.path.join(self.experiment_path, self.run_name)


class DummyHashResolver(RunPathResolver):
    def get_run_path(self):
        return os.path.join(self.data_dir, self.new_data_set_dir)

    def initialize_path_variables(self):
        self.new_data_set_dir = self.config['new_data_directory']
        self.data_dir = self.config[names.DATA_DIRECTORY]
