from abc import ABC, abstractmethod
from typing import List

import cv2
from PIL import Image
from class_registry import ClassRegistry
import cv2 as cv
import numpy as np
from torchvision.transforms import RandomResizedCrop

from src.constants.miscellaneous import IMAGE, LABEL

processor_registry = ClassRegistry('processor_name')


class Processor(ABC):
    """
    Base class for any pattern processors
    Patterns are expected to be the dictionaries with 3 keys: [IMAGE, LABEL, ID]
    Images are expected in format [channels, height, width]
    """
    processor_name = None

    def __init__(self, params):
        self.params = params

    @abstractmethod
    def process(self, pattern):
        pass


class ProcessorHelper:
    def __init__(self, processors: List[Processor]):
        self.processors = processors

    def get(self, pattern):
        result = pattern.copy()
        for processor in self.processors:
            result = processor.process(result)
        result[IMAGE] = result[IMAGE].copy()
        return result


@processor_registry.register
class CLAHErocessor(Processor):
    processor_name = 'clahe'

    def __init__(self, params):
        super(CLAHErocessor, self).__init__(params)
        self.clipLimit = params.get('clipLimit', 8.0)
        self.tileGridSize = params.get('tileGridSize', 8)
        self.channels = params.get('channels', 3)

        self.clache = cv.createCLAHE(clipLimit=self.clipLimit, tileGridSize=(self.tileGridSize, self.tileGridSize))

    def process(self, pattern):
        res = np.expand_dims(self.clache.apply(pattern[IMAGE].astype('uint8')[0, :, :]), axis=0)
        pattern[IMAGE] = np.repeat(res, self.channels, axis=0)
        return pattern


@processor_registry.register
class DivideBy255(Processor):
    processor_name = 'divide_255'

    def process(self, pattern):
        pattern[IMAGE] = pattern[IMAGE] / 255
        return pattern


@processor_registry.register
class GroupLabels(Processor):
    processor_name = 'group_classes'

    def __init__(self, params):
        super(GroupLabels, self).__init__(params)
        self.grouping = params['grouping']

    def process(self, pattern) -> dict:
        pattern[LABEL] = pattern[LABEL] * 0 + self.grouping[pattern[LABEL]]
        return pattern


@processor_registry.register
class Resize(Processor):
    processor_name = 'resize_image'

    def __init__(self, params):
        super(Resize, self).__init__(params)
        self.scale = params.get('scale', 0.5)
        assert self.scale > 0, f'Resizing scale should be bigger than zero but was :{self.scale}'

    def process(self, pattern) -> dict:
        image = pattern[IMAGE]
        image = np.moveaxis(image, 0, -1)  # cv works with ither axis order

        new_width, new_height = round(image.shape[0] * self.scale), round(image.shape[0] * self.scale)
        new_image = cv2.resize(image, (new_width, new_height), interpolation=cv2.INTER_AREA)

        new_image = np.moveaxis(new_image, -1, 0)
        pattern[IMAGE] = new_image
        return pattern


@processor_registry.register
class RotateAndCrop(Processor):
    processor_name = 'rotate_and_crop'

    max_angle = 90

    def __init__(self, params):
        super(RotateAndCrop, self).__init__(params)
        self.cut_after_rotation = params.get('cut_after_rotation', True)
        self.max_angle = params.get('max_angle', 90)

    def process(self, pattern):
        pattern[IMAGE] = pattern[IMAGE]

        rotation_angle = int((np.random.rand() - 0.5) * 2 * self.max_angle)

        # # TODO: use rotation from other lib
        # if len(pattern[IMAGE].shape) > 2:  # ROTATE FUNCTION REQUIRES OTHER ORDER OF AXES
        #     assert (pattern[IMAGE].shape[0] == 3)
        #     pattern[IMAGE] = np.moveaxis(pattern[IMAGE], 0, -1)
        #
        # pattern[IMAGE] = t.rotate(pattern[IMAGE], rotation_angle,
        #                           resize=False, cval=0,
        #                           clip=True, preserve_range=True,
        #                           mode='symmetric')
        #
        # if len(pattern[IMAGE].shape) > 2:  # RESTORE ORDER OF AXES
        #     pattern[IMAGE] = np.moveaxis(pattern[IMAGE], -1, 0)

        if self.cut_after_rotation:
            old_shape = pattern[IMAGE].shape
            smallest_side = min(old_shape[-1], old_shape[-2])
            new_halfside_size = int(smallest_side / (2 * 2 ** (1 / 2)))
            shift = int(smallest_side / 2 - new_halfside_size)

            pattern[IMAGE] = pattern[IMAGE][:, shift:-shift, shift:-shift]

        return pattern


@processor_registry.register
class RandomFlipProcessor(Processor):
    processor_name = 'flip'

    def __init__(self, params):
        super(RandomFlipProcessor, self).__init__(params)
        self.flip_vertically = params.get('flip_vertically', True)
        self.flip_horizontally = params.get('flip_horizontally', True)

    def process(self, pattern):
        if self.flip_vertically and np.random.random() > 0.5:
            pattern[IMAGE] = np.flip(pattern[IMAGE], axis=1)
        if self.flip_horizontally and np.random.random() > 0.5:
            pattern[IMAGE] = np.flip(pattern[IMAGE], axis=2)
        return pattern


@processor_registry.register
class RandomNoiseProcessor(Processor):
    processor_name = 'noise'

    def __init__(self, params):
        super(RandomNoiseProcessor, self).__init__(params)
        self.noise_std = params.get('noise_std', 0)

    def process(self, pattern):
        image = pattern[IMAGE]
        pattern[IMAGE] = image + np.random.standard_normal(image.shape) * np.std(image) * self.noise_std
        return pattern

@processor_registry.register
class RandomCropProcessor(Processor):
    processor_name = 'random_crop'

    def __init__(self, params):
        super(RandomCropProcessor, self).__init__(params)
        self.scale_from = params.get('scale_from', 0.1)
        self.scale_to = params.get('scale_to', 0.9)
        self.probability = params.get('probability', 0.5)
        self.processor = None

    def process(self, pattern):
        if np.random.rand() > self.probability:
            image = pattern[IMAGE]
            processed_image = np.moveaxis(image, 0, 2)
            im = Image.fromarray(processed_image, mode='RGB')

            processor = self.get_processor(im)
            im = processor(im)
            pattern[IMAGE] = np.moveaxis(np.asarray(im), -1, 0)

        return pattern

    def get_processor(self, image):
        if self.processor is None:
            self.processor = RandomResizedCrop(size=(image.size),scale=(self.scale_from, self.scale_to), ratio=(1,1))
        return self.processor