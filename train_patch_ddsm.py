import click
import os

import torch

from src.constants import names
from src.constants.miscellaneous import DATA
from src.experiment_runner import ExperimentRunner
from src.journaling.logs import initialize_logger
from src.models.workers import PatchDDSMTrainerWorker, PatchDDSMEntropyMinWorker, PatchDDSMOldTrainerWorker
from src.utils import load_experiment_config, update_dictionary

PATH_TO_PROJECT = os.path.join(os.path.dirname(__file__))

class DDSMTrainRunner(ExperimentRunner):
    worker_class = PatchDDSMTrainerWorker

class DDSMOldTrainRunner(ExperimentRunner):
    worker_class = PatchDDSMOldTrainerWorker

class EntropyMinDDSMTrainRunner(ExperimentRunner):
    worker_class = PatchDDSMEntropyMinWorker

@click.command()
@click.option('--contact-me', type=str, default=None, help='This is visible in htop so that others would know who ran the process.')
@click.option('--use-cuda', type=bool, default=False)
@click.option('--seed', default=42, type=int, help='Seed for experiment reproduction.')
@click.option('--experiment_name', default='default', type=str, help='Path of the experiment in the models directory.')
@click.option('--dtype', default='float', type=str, help='Which dtype to use.')
@click.option('--data_directory', default=os.path.join(PATH_TO_PROJECT, DATA), type=str,
              help='Location of the directory with the data.')
@click.option('--gc-aggressive', default=False, type=bool, help='Chooses whether to collect garbage aggressively.')
@click.option('--reproducible', default=False, type=bool, help='Whether to allow nondeterminism.')
@click.option('--semisupervised', default=False, type=bool, help='')
@click.option('--use_old_version', default=False, type=bool, help='Use kaggle-ddsm dataset.')
@click.option('--run_separate_process', default=False, type=bool, help='Use kaggle-ddsm dataset.')
def main(**kwargs):
    if kwargs['reproducible']:
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
    experiment_directory = os.path.join(PATH_TO_PROJECT, names.MODELS, kwargs['experiment_name'])
    initialize_logger(os.path.join(experiment_directory, 'experiment.log'))

    config = load_experiment_config(kwargs['experiment_name'])
    update_dictionary(source_dict=kwargs, target_dict=config)

    if kwargs['use_old_version']:
        runner = DDSMOldTrainRunner(**config)
    elif kwargs['semisupervised']:
        runner = EntropyMinDDSMTrainRunner(**config)
    else:
        runner = DDSMTrainRunner(**config)

    runner.run_grid_search()


if __name__ == '__main__':
    main()


