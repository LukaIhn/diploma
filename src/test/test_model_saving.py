import os
import unittest
from copy import deepcopy

import torch
from torch.nn import CrossEntropyLoss

from src.configs.patch_ddsm_config_example import RUN_PARAMETERS
from src.constants.miscellaneous import MODEL, IMAGE, LABEL
from src.experiment_runner import ExperimentRunner
from src.models.patch_ddsm_models import model_registry
from src.models.workers import PatchDDSMTrainerWorker

import numpy as np

_PROJECT_PATH = os.path.join(os.path.dirname(__file__), '..', '..')


class TestTrainer(object):
    model_name = None
    params = deepcopy(RUN_PARAMETERS)

    @staticmethod
    def simple_loader(data: np.ndarray, labels: np.ndarray):
        for row in range(data.shape[0]):
            pattern = dict()
            pattern[IMAGE] = data[row:row + 1, ...]
            pattern[LABEL] = labels[row:row + 1]
            yield pattern

    def setUp(self):
        self.params['use_cuda'] = False

        model = model_registry.get(self.params[MODEL], self.params)
        self.trainer = PatchDDSMTrainerWorker(
            params=self.params,
            model=model,
            train_metrics=CrossEntropyLoss(),
            device=ExperimentRunner.get_device(self.params),
            optimizer=None,
            experiment_name='test',
            run_name='0',
            experiment_id=0,
            train_loader=None,
            validation_loader=None,
            test_loader=None)

        self.device = torch.device('cpu')

    def test_model_saving(self):
        """
        Tests that models before saving and after loading have the same loss
        :return:
        """
        # noinspection PyUnresolvedReferences
        data = np.random.random((2, 3, 299, 299))
        labels = np.random.randint(4, size=2)
        # noinspection PyUnresolvedReferences
        loss = self.trainer._evaluate_metrics_for_loader(self.simple_loader(data, labels))
        self.trainer._save_model('test')
        self.trainer._load_model('test')
        reloaded_model_loss = self.trainer._evaluate_metrics_for_loader(self.simple_loader(data, labels))

        self.assertTrue(loss != 0)
        self.assertTrue(loss == reloaded_model_loss)


class TestDefault(TestTrainer, unittest.TestCase):
    model_name = 'default'


class TestResNet18(TestTrainer, unittest.TestCase):
    model_name = 'resnet18'
