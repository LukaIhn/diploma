import pandas as pd
import numpy as np
import os
import cv2

from PIL import Image
from tqdm import tqdm

from src.data.CBIS.cbis_directory_structure_helper import ViewType, BreastSideType, PATIENT_IMAGES_DIR, \
    PNGMetafeatureColumns, DICOMMetadataCols, CbisDetectionColumns, Pathology

DATA_WIDTH = 1700
DATA_HEIGHT = 2100

DATA_FILE_NAME = 'data.npy'
DATA_MASKS_FILE_NAME = 'data_masks.npy'

PNG_DATA_DIR = '/media/wm/Seagate 4TB/mff/CBIS_FULL/PNGS_FINAL'
METADATA_PATH = os.path.join(PNG_DATA_DIR, 'metafeatures.h5')

MM_DIR = './data/FRCNN_CBIS/'
MM_PATH = os.path.join(MM_DIR, DATA_FILE_NAME)
MM_MASKS_PATH = os.path.join(MM_DIR, DATA_MASKS_FILE_NAME)


def _prepare_new_set_division(df):
    new_labels = np.zeros((len(df)))
    for i, r in tqdm(df.iterrows(), total=len(df)):
        patient_set = ((df[df[DICOMMetadataCols.PATIENT_ID] == r[DICOMMetadataCols.PATIENT_ID]][
                            DICOMMetadataCols.DATA_SET_ORIGINAL_TYPE] == 'test') * 1).mean() > 0
        new_labels[i] = patient_set
    df[CbisDetectionColumns.IS_TEST] = new_labels
    return df


if __name__ == '__main__':
    metadata = pd.read_hdf(METADATA_PATH, key='data')
    metadata = metadata[metadata[DICOMMetadataCols.PATHOLOGY] != Pathology.BENIGN_WITHOUT_CALLBACK]
    metadata.reset_index(drop=True, inplace=True)
    metadata = _prepare_new_set_division(metadata)

    os.makedirs(MM_DIR, exist_ok=True)


    def get_png_dir(data_root_dir: str, patient_id: str, view: ViewType, left_or_right: BreastSideType):
        name = '|'.join([patient_id, view.value, left_or_right.value])
        return os.path.join(data_root_dir, PATIENT_IMAGES_DIR, name).replace('\n', '')


    def get_file_png_directory_by_index(index_in_df: int):
        row = metadata.iloc[index_in_df]
        patient_id = row['patient_id']
        view = ViewType.value_of(row['image_view'])
        left_or_right = BreastSideType.value_of(row['left_or_right_breast'])

        return get_png_dir(PNG_DATA_DIR, patient_id, view, left_or_right)


    def resize_image(image: np.ndarray) -> (np.ndarray, float):
        width, height = image.shape[1], image.shape[0]

        scale = min(round(DATA_WIDTH) / width, round(DATA_HEIGHT) / height)

        new_width, new_height = round(image.shape[1] * scale), round(image.shape[0] * scale)
        new_image = cv2.resize(image, (new_width, new_height), interpolation=cv2.INTER_AREA)

        fill_height = np.zeros((DATA_HEIGHT - new_height, new_image.shape[1]))
        new_image = np.concatenate([new_image, fill_height], axis=0)

        fill_width = np.zeros((new_image.shape[0], DATA_WIDTH - new_width))
        new_image = np.concatenate([new_image, fill_width], axis=1)

        return new_image, scale


    # add names of main images
    full_image_dirs = sorted(
        [get_png_dir(PNG_DATA_DIR, a[0], ViewType.value_of(a[1]), BreastSideType.value_of(a[2])) for a, b in
         metadata.groupby(
             [PNGMetafeatureColumns.PATIENT_ID, DICOMMetadataCols.IMAGE_VIEW, DICOMMetadataCols.LEFT_OR_RIGHT_BREAST])])
    names = []
    for i, r in tqdm(metadata.iterrows(), total=len(metadata)):
        names.append(
            get_png_dir('', r[PNGMetafeatureColumns.PATIENT_ID], ViewType.value_of(r[DICOMMetadataCols.IMAGE_VIEW]),
                        BreastSideType.value_of(r[DICOMMetadataCols.LEFT_OR_RIGHT_BREAST])).split('/')[1])
    metadata[CbisDetectionColumns.CASE_NAME] = names
    case_order_id_dict = {n: i for i, n in
                          enumerate(sorted(np.unique(metadata[CbisDetectionColumns.CASE_NAME].values)))}
    metadata[CbisDetectionColumns.CASE_NAME_ORDER_ID] = metadata[CbisDetectionColumns.CASE_NAME].apply(
        lambda v: case_order_id_dict[v])

    assert len(full_image_dirs) == len(case_order_id_dict), 'length of unique full images is not equal'
    assert all([a.split('/')[-1] == b for a, b in
                zip(full_image_dirs, case_order_id_dict.keys())]), 'directories were ordered differently'

    # prepare full_images
    images_memmap = np.memmap(MM_PATH, dtype='uint8', mode='w+', shape=(len(full_image_dirs), DATA_HEIGHT, DATA_WIDTH))
    for i, full_img_dir in tqdm(enumerate(full_image_dirs), total=len(full_image_dirs)):
        image = Image.open(os.path.join(full_img_dir, 'full.png'))
        image_np = np.asarray(image)

        new_img, _ = resize_image(image_np)
        images_memmap[i, ...] = new_img

    # prepare bboxes
    masks_memmap = np.memmap(MM_MASKS_PATH, dtype='bool', mode='w+', shape=(len(metadata), DATA_HEIGHT, DATA_WIDTH))
    bboxes = []
    scales = []

    for i, row in tqdm(metadata.iterrows(), total=len(metadata)):
        case_dir = get_file_png_directory_by_index(i)
        mask_name = row[PNGMetafeatureColumns.MASK_NAME]
        image = Image.open(os.path.join(case_dir, mask_name))
        image_np = np.asarray(image)

        mask, scale = resize_image(image_np)
        mask = mask > 0
        scales.append(scale)

        pos = np.where(mask)

        mask_np_array = np.asarray(mask)
        assert len(mask_np_array.shape) == 2
        x_max, y_max = mask_np_array.shape[0] - 1, mask_np_array.shape[1] - 1

        xmin_mask = np.min(pos[0])
        xmax_mask = np.max(pos[0])
        ymin_mask = np.min(pos[1])
        ymax_mask = np.max(pos[1])

        bboxes.append(np.array([xmin_mask, xmax_mask, ymin_mask, ymax_mask]))
        masks_memmap[i, ...] = mask
        # plt.imshow(mask)
        # plt.show()
        # plt.imshow(mask[xmin_mask:xmax_mask, ymin_mask:ymax_mask])
        # plt.show()

    bboxes = np.array(np.array(bboxes), dtype=np.int32)
    metadata[CbisDetectionColumns.X_MIN] = bboxes[:, 0]
    metadata[CbisDetectionColumns.X_MAX] = bboxes[:, 1]
    metadata[CbisDetectionColumns.Y_MIN] = bboxes[:, 2]
    metadata[CbisDetectionColumns.Y_MAX] = bboxes[:, 3]
    metadata[CbisDetectionColumns.SCALES] = scales

    metadata.to_hdf(os.path.join(MM_DIR, 'metafeatures.h5'), 'data')
