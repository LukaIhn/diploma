import os
import unittest
from copy import deepcopy

import torch

from src.constants import names
from src.constants.miscellaneous import TEST

from src.models.metrics import metrics_registry, MetricAbstract

_PROJECT_PATH = os.path.join(os.path.dirname(__file__), '..', '..')


class TestAccuracyMetric(unittest.TestCase):
    metric_name = 'accuracy'

    def setUp(self):
        self.device = torch.device('cpu')
        number_of_classes = 2

        parameters = {
            'device': self.device,
            names.NUMBER_OF_CLASSES: number_of_classes,
            names.IMAGE_SHAPE: (3, 2, 2),
            names.RUN_DIR_PATH: os.path.join(_PROJECT_PATH, names.MODELS, TEST),
            names.CLASS_NAMES: [str(i) for i in range(number_of_classes)],
            names.SET_NAME: TEST,
        }
        self.metric: MetricAbstract = metrics_registry.get(self.metric_name, **parameters)

    @staticmethod
    def _generate_true_values(number_of_elements: int, dtype=torch.int64):
        """
        generates
        :param number_of_elements:
        :return:
        """
        ones = torch.ones((number_of_elements), dtype=dtype)
        zeros = torch.zeros((number_of_elements), dtype=dtype)

        target = ones
        prediction = torch.cat((zeros.unsqueeze(1), ones.unsqueeze(1)), 1)

        return target, prediction

    @staticmethod
    def _generate_false_values(number_of_elements: int, dtype=torch.int64):
        ones = torch.ones((number_of_elements, 1), dtype=dtype)
        zeros = torch.zeros((number_of_elements, 1), dtype=dtype)

        target = ones
        prediction = torch.cat((ones, zeros), 1)

        return target, prediction

    def test_max_value_is_one(self):
        number_of_patterns = 101
        target, input_ = self._generate_true_values(number_of_patterns)
        self.metric.accumulate(target, input_)

        self.assertTrue(self.metric.get_results() == 1)

    def test_min_value_is_one(self):
        number_of_patterns = 101
        target, input_ = self._generate_false_values(number_of_patterns)
        self.metric.accumulate(target, input_)

        self.assertTrue(self.metric.get_results() == 0)

    def test_accumulation(self):
        number_of_patterns = 10
        target, input_ = self._generate_false_values(number_of_patterns)
        self.assertTrue(self.metric.accumulate(target, input_).item() == 0)

        target, input_ = self._generate_true_values(number_of_patterns)
        self.assertTrue(self.metric.accumulate(target, input_).item() == 1)

        self.assertTrue(self.metric.get_results() == 0.5)

    def test_reset(self):
        number_of_patterns = 10

        target, input_ = self._generate_false_values(number_of_patterns)
        self.metric.accumulate(target, input_)

        target, input_ = self._generate_true_values(number_of_patterns)
        self.metric.accumulate(target, input_)

        self.assertTrue(self.metric.get_results() == 0.5)
        self.assertTrue(self.metric.get_results() == 0.5)

        self.metric.reset()

        self.assertRaises(ValueError, lambda: (self.metric.get_results() == 0.5))

    def test_accumulation_multiple(self):
        for i in range(2):
            target, input_ = self._generate_false_values(10)
            self.metric.accumulate(target, input_)

            target, input_ = self._generate_true_values(10)
            self.metric.accumulate(target, input_)

        self.assertTrue(self.metric.get_results() == 0.5)
