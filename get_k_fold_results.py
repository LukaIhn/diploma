import json

import click
import os
import pandas as pd
import numpy as np

from scipy.stats import ttest_rel, ttest_ind

from src.constants.names import METRIC_FILE_ENDING, TORCH_SEED, CHOSEN_FOLD, MODELS
from src.constants.miscellaneous import CONFIG_FILE_NAME, TABLE_LOGGER_SEPARATOR, DATA, MODEL_COMPARISON_RESULT
from src.utils import check_dir

PATH_TO_PROJECT = os.path.join(os.path.dirname(__file__))


@click.command()
@click.option('--config_path', default='./src/configs/model_comparison_config.json', type=str,
              help='File with test parameters.')
@click.option('--root_path', default=os.path.join(PATH_TO_PROJECT, MODELS), type=str,
             help='File with test parameters.')
def main(**kwargs):
    parameters = json.load(open(kwargs['config_path'], 'rb'))

    exp_A_dir = os.path.join(kwargs['root_path'], parameters['exp_a_path'])
    exp_B_dir = os.path.join(kwargs['root_path'], parameters['exp_b_path'])

    metric_to_compare = parameters['metric_to_compare']
    set_name = parameters['set_to_compare']

    message = 'Incorrect directory for experiment {}'
    assert check_dir(exp_A_dir), message.format('A')
    assert check_dir(exp_B_dir), message.format('B')

    def get_exp_configs(exp_path, data_seed, torch_seed) -> dict:
        """
        Recursively goes down through all the directories inside the exp_path and returns dictionary of pathes of
        runs as keys and their grid search configs as the values.
        :param exp_path:
        :return:
        """
        run_pathes = []
        for root, subFolders, files in os.walk(exp_path):
            directory_contains_run = any([CONFIG_FILE_NAME == f for f in files])
            if directory_contains_run:
                run_pathes.append(root)

        pathes_with_configs = dict()
        for run in run_pathes:
            loaded_config = json.load(open(os.path.join(run, CONFIG_FILE_NAME), 'rb'))
            if (loaded_config[DATA]['seed'] == data_seed): # and (loaded_config[TORCH_SEED] == torch_seed):
                pathes_with_configs[run] = loaded_config

        return pathes_with_configs

    exp_A_runs = get_exp_configs(exp_A_dir, data_seed=parameters['exp_a_data_seed'],
                                 torch_seed=parameters['exp_a_seed'])
    exp_B_runs = get_exp_configs(exp_B_dir, data_seed=parameters['exp_b_data_seed'],
                                 torch_seed=parameters['exp_b_seed'])

    assert len(exp_A_runs) > 0 and len(exp_B_runs) > 0, 'Experiment directories must contain runs.'

    # loading metric values(last row from table log)
    def load_metric_for_run(run_path, metric_name, set_name):
        metric_file_name = '_'.join([set_name, metric_name]) + METRIC_FILE_ENDING
        df = pd.read_csv(os.path.join(run_path, metric_file_name))
        if parameters['best_or_last'] == 'best':
            return df.iloc[-1].values[1]
        else:
            return df.iloc[-2].values[1]

    for runs in [exp_A_runs, exp_B_runs]:
        for run_path, parameter_dict in runs.items():
            parameter_dict[metric_to_compare] = load_metric_for_run(run_path, metric_to_compare, set_name)

    # assert runs are paired and get metric differences
    def get_data_fold(name_param_pair):
        return name_param_pair[1][DATA][CHOSEN_FOLD]

    message = 'There should be equal number of runs for paired test. Most likely tests have different number of folds.'
    assert len(exp_A_runs) == len(exp_B_runs), message

    # runs are sorted according to their data fold
    a_values, b_values = list(), list()
    for run_a_pair, run_b_pair in zip(sorted(exp_A_runs.items(), key=get_data_fold),
                                      sorted(exp_B_runs.items(), key=get_data_fold)):
        rau_a_path, run_a_params = run_a_pair
        rau_b_path, run_b_params = run_b_pair

        assert get_data_fold(run_a_pair) == get_data_fold(run_b_pair)

        a_value = load_metric_for_run(rau_a_path, metric_to_compare, set_name)
        b_value = load_metric_for_run(rau_b_path, metric_to_compare, set_name)

        a_values.append(a_value)
        b_values.append(b_value)

    # run paired test
    if parameters['test_type'] == 'paired':
        statistic, p_value = ttest_rel(a_values, b_values)
    else:
        statistic, p_value = ttest_ind(a_values, b_values)

    # save results
    results = dict()
    results['statistic'] = statistic
    results['p_value'] = p_value

    results['a_std'] = np.array(a_values).std()
    results['b_std'] = np.array(b_values).std()

    results['a_mean'] = np.array(a_values).mean()
    results['b_mean'] = np.array(b_values).mean()

    a_path, a_params = list(exp_A_runs.items())[0]
    b_path, b_params = list(exp_B_runs.items())[0]

    results['a_config_example'] = a_params
    results['b_config_example'] = b_params
    results['a_path'] = a_path
    results['b_path'] = b_path

    results['test_configurations'] = parameters

    with open(os.path.join(os.path.dirname(kwargs['config_path']), MODEL_COMPARISON_RESULT), mode='w') as file:
        json.dump(results, file, indent=2)


if __name__ == '__main__':
    main()
