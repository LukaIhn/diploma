import src.constants.names as names
from src.constants.miscellaneous import PARAMS, DATA, MULTITASK_LABEL_GROUPING

number_of_folds = 9

RUN_PARAMETERS = {
    'model': 'fcn',
    'use_pretrained_weights': True,
    names.FEATURE_MAP_DIM: 200,

    # TODO: make DataHandler handle size of image without config
    'image_size_flatten': 900 * 900,
    names.IMAGE_SHAPE: (3, 900, 900),

    DATA: {
        names.NUMBER_OF_FOLDS: number_of_folds,
        names.CHOSEN_FOLD: 0,
        names.SEED: 42,
        names.TORCH_SAMPLING_SEED: 0,
        'batch_size': 20,
        'evaluation_batch_size': 50,
        names.BALANCE_CLASSES: True,
        'data_set_directory_name': None,
    },

    'number_of_epochs': 60,
    'num_workers': 0,
    'early_stopping_epoch_number': 0,
    names.NUMBER_OF_CLASSES: 4,
    'experiment_id': 0,

    # OPTIMIZER
    'optimizer': 'sgd',
    'learning_rate': 1e-3,
    'weight_decay': 0,
    'momentum': 0.8,

    names.UNSUPERVISED_WEIGHT: 1.0,

    names.TORCH_SEED: 42,

    # 'train_loss_function': {names.NAME: 'cross_entropy_loss_metric', PARAMS: {}},
    'train_loss_function': {names.NAME: 'cross_entropy_loss_multitask_metric', PARAMS: {}},
    names.ENTROPY_LOSS_FUNCTION: {names.NAME: 'entropy_loss_multitask_metric', PARAMS: {names.LOSS_GROUPING: [[0, 1]]}},

    'train_metrics': [
        {names.NAME: 'cross_entropy_loss_multitask_metric', PARAMS: {}},
        {names.NAME: 'accuracy', PARAMS: {}}
    ],

    'evaluation_metrics': [
        {names.NAME: 'cross_entropy_loss_multitask_metric', PARAMS: {}},
        {names.NAME: 'accuracy_multitask', PARAMS: {names.METRIC_IS_SELECTIVE_PARAM: True}},
        {names.NAME: 'accuracy', PARAMS: {}},
        # {names.NAME: 'class_representative_metric', PARAMS: {'dpi': 50, names.SAVE_IMAGE_STATISTICS: False}},
        {names.NAME: 'auc_grouped_metric', PARAMS: {names.LOSS_GROUPING: MULTITASK_LABEL_GROUPING[0]}},
        {names.NAME: 'auc_grouped_metric', PARAMS: {names.LOSS_GROUPING: MULTITASK_LABEL_GROUPING[1]}},
        {names.NAME: 'accuracy_binary_grouped', PARAMS: {names.LOSS_GROUPING: MULTITASK_LABEL_GROUPING[0]}},
        {names.NAME: 'accuracy_binary_grouped', PARAMS: {names.LOSS_GROUPING: MULTITASK_LABEL_GROUPING[1]}},
        # {names.NAME: 'confusion_matrix_metric', PARAMS: {'dpi': 210}},
        {names.NAME: 'confusion_matrix_multitask_metric', PARAMS: {'dpi': 210}},
    ],

    names.CALLBACK_NAMES: [
        'model_saver',
        'early_stopping',
        'metric_logger',
        'model_logger',
        'model_freezer',
        'resource_logger',
        'decrease_lr',
        'param_logger',
        'renew_task',
        # 'semisupervised_switch',
    ],

    names.COMMON_PROCESSORS: [
        {names.NAME: 'resize_image', PARAMS: {}},
        {names.NAME: 'random_crop', PARAMS: {}},
        # {names.NAME: 'resize_image', PARAMS: {'scale': 450/900}},
        # {names.NAME: 'clahe', PARAMS: {}},
        # {names.NAME: 'rotate_and_crop', PARAMS: {}},
        {names.NAME: 'divide_255', PARAMS: {}},
    ],

    names.TRAIN_PROCESSORS: [  # TODO: separate to general and train processors
        {names.NAME: 'flip', PARAMS: {}},
        {names.NAME: 'noise', PARAMS: {}},
    ],
    names.VALIDATION_PROCESSORS: [],
    names.TEST_PROCESSORS: [],

    names.TRAIN_ONLY: ['random_crop'],

    # CALLBACK PARAMETERS
    names.UNFREEZE_AFTER_EPOCH: 0,
    names.LR_SHEDULE: {35: 0.4 * 1e-5},
    'log_patterns': False,

    names.SEMISUPERVISED_SHEDULE: {
        0: [False, True, None],
        15: [True, True, None],
    }
}

grid_search = {
    names.TORCH_SEED: list(range(2)),
    # 'hidden_layer_size': [30],
    'model': ['fcn'],  # , 'simple_cnn', 'resnet18', 'vgg11', 'mobilenet_v2'],
    # names.DATA_KEY_WORD + names.CHOOSED_FOLD: list(range(number_of_folds)),
    # names.DATA_KEY_WORD + 'seed': [1, 2, 2, 1],
    # names.OPTIMIZER: ['radam', 'adam']
    # names.TRAIN_PROCESSORS: [get_data_transformers(std) for std in stds_for_processing],
}
