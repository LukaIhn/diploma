import copy
import gc
import itertools
import logging
import os

import torch
from torch.nn import Parameter

import src.constants.names as names
from src.constants.miscellaneous import DATA, TRAILING_HASHTAG

from src.journaling.logs import log_named_dictionary
from src.models.workers import BaseWorker
from src.pathes.path_resolver import RunPathResolver
from src.utils import switch_garbage_collection_mode, update_model_training_config

_PROJECT_PATH = os.path.join(os.path.dirname(__file__), '..')
assert os.path.join('src', '..') in _PROJECT_PATH, 'Directory structure has changed! Cant load {}'.format(__file__)


def run_single_experiment(run_config: dict, path_resolver_class: type, device, data_directory, logger, worker_class):
    """
    Creates metric objects, data loaders and experiment directories. After that runs the trainer.
    :param path_resolver_class:
    :param run_config:
    :return:
    """
    try:
        import gc
        import torch

        if logger is None:
            logger = logging.getLogger('main')

        if device.type != 'cpu':
            torch.cuda.empty_cache()
            torch.cuda.synchronize('cuda')
        gc.collect()

        path_resolver = path_resolver_class(**run_config)

        run_config[names.DEVICE] = device
        run_config[names.DATA_DIRECTORY] = data_directory
        run_config[names.RUN_PATH_RESOLVER] = path_resolver

        if len(os.listdir(path_resolver.get_run_path())) > 1:
            logger.warning(TRAILING_HASHTAG + '\nExperiment directory is not empty!\n' + TRAILING_HASHTAG)

        worker: BaseWorker = worker_class(**run_config)
        # worker: BaseWorker = worker_registry.get(self.worker_name, **run_config)
        worker.load_dependencies()
        worker.start()

        del worker
        if device.type != 'cpu':
            torch.cuda.empty_cache()
            torch.cuda.synchronize('cuda')
    except Exception as e:
        print(e)

def print_usage(msg=''):
    print(msg + ': \n')
    import torch
    tensors = 0
    params_ = 0
    others_ = 0

    gpu_tensors = 0
    gpu_params_ = 0
    gpu_others_ = 0

    for obj in gc.get_objects():
        try:
            if type(obj) is Parameter:
                if obj.device.type == 'cpu':
                    params_ += 1
                else:
                    gpu_params_ += 1

            if torch.is_tensor(obj):
                if obj.device.type == 'cpu':
                    tensors += 1
                else:
                    gpu_tensors += 1

            if not torch.is_tensor(obj) and (hasattr(obj, 'data') and torch.is_tensor(obj.data)):
                # print(type(obj), obj.size())
                if obj.device.type == 'cpu':
                    others_ += 1
                else:
                    gpu_others_ += 1
        except:
            pass
    print(f'CPU      TENSORS: {tensors}, PARAMS:{params_}, OTHERS:{others_}')
    print(f'GPU      TENSORS: {gpu_tensors}, PARAMS:{gpu_params_}, OTHERS:{gpu_others_}')


class ExperimentRunner:
    worker_class = None
    path_resolver_class = RunPathResolver

    def __init__(self, **params: dict):
        self.params = params
        self.logger = logging.getLogger('main')

        self.device = ExperimentRunner.get_device(params)
        # self.dtype = ExperimentRunner._update_dtype(params)

        self.experiment_name = self.params['experiment_name']
        self.data_directory = self.params['data_directory']

        switch_garbage_collection_mode(self.params['gc_aggressive'])

    # def get_data_handler(self, data_directory: str, params: dict):
    #     self.logger.debug('Getting data handler...')
    #
    #     if self._data_handler is None or \
    #             (not self._data_handler.check_parameters_are_compatible(data_directory, params)):
    #         self.worker.create_data_handler(data_directory, params)
    #     else:
    #         self._data_handler.update_batch_sizes(params)
    #
    #     self._data_handler.initialize_loaders(fold_number=params[DATA][names.CHOOSED_FOLD])
    #
    #     return self._data_handler

    @staticmethod
    def get_device(params: dict):
        if params['use_cuda']:
            device = torch.device('cuda')
        else:
            device = torch.device('cpu')
        return device

    @staticmethod
    def _update_dtype(params: dict):
        if params['dtype'] == 'half':
            torch.set_default_tensor_type('torch.HalfTensor')
            dtype = torch.half
        elif params['dtype'] == 'float':
            torch.set_default_tensor_type('torch.FloatTensor')
            dtype = torch.float
        else:
            raise ValueError('Unknown torch dtype. {}'.format(params['dtype']))
        return dtype

    def run_grid_search(self):
        """
        creates all possible combinations of values of different variables stored in 'gridsearch_params'
        and runs the experiments
        """
        gridsearch_params = self.params['grid_search']
        log_named_dictionary(self.logger, name='grid search parameters', dictionary=gridsearch_params)

        assert all(type(v) is list for v in gridsearch_params.values())
        combinations = list(itertools.product(*gridsearch_params.values()))
        search_keys = gridsearch_params.keys()

        # iterating over runs
        for run_number, combination in enumerate(combinations):  # TODO: move to separate function get_run_config
            gridsearch_params_dict = {}
            run_config = copy.deepcopy(self.params[names.RUN_PARAMETERS])
            new_values = list(zip(search_keys, combination))

            update_model_training_config(run_config, new_values)

            for key, value in new_values:
                gridsearch_params_dict[key] = value

            # for the sake of logging
            run_config[names.GRIDSEARCH_UPDATED] = gridsearch_params_dict
            run_config[names.RUN_NUMBER] = run_number
            run_config[names.EXPERIMENT_NAME] = self.experiment_name
            run_config[names.MODEL_NAME] = run_config[DATA].get(names.MODEL_NAME, 'no_model')

            self.run_single_experiment(run_config)

    def run_single_experiment(self, run_config: dict, **kwargs):
        # torch has bug in releasing GPU memory after OOMs, hence each run now has separate process
        import torch.multiprocessing as mp

        exp_params = (run_config, self.path_resolver_class, self.device, self.data_directory, None, self.worker_class)

        if kwargs.get('run_separate_process', False):
            p = mp.Process(target=run_single_experiment, args=exp_params)
            p.start()
            p.join()
        else:
            run_single_experiment(*exp_params)

