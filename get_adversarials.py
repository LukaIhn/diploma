
import torch
import numpy as np
import pandas as pd
from PIL import Image
from torch.nn import BCELoss
from tqdm import tqdm

from src.models.prediction_storage import Storage
from src.models.workers import TorchModelLoader

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

TO_MALIGNANT = True

img_pathes = [
    './TMP/benign_incorrect_example/P_00979|LEFT|CC|mask|mass|BENIGN|0.png',
    './TMP/benign_incorrect_example/P_01204|RIGHT|MLO|mask|mass|MALIGNANT|0.png',
    './TMP/benign_incorrect_example/P_01617|LEFT|CC|mask|mass|MALIGNANT|0.png',

'./TMP/malign_incorrect_examp/P_00723|LEFT|MLO|mask|calc|BENIGN|0.png',
'./TMP/malign_incorrect_examp/P_01237|LEFT|CC|mask|calc|BENIGN|0.png',
'./TMP/malign_incorrect_examp/P_01803|RIGHT|MLO|mask|calc|MALIGNANT|0.png',
]


advs = []

for img_path in img_pathes:
    #img_path = './TMP/P_00118|RIGHT|CC|mask|mass|MALIGNANT|0.png'
    #img_path = './TMP/P_00041|LEFT|CC|mask|calc|BENIGN_WITHOUT_CALLBACK|0.png'
    #img_path = './TMP/P_00047|LEFT|CC|mask|calc|MALIGNANT|1.png'
    #img_path = './TMP/full.png'


    m = TorchModelLoader.load('./TMP/6.checkpoint',device)

    i = Image.open(img_path)
    nump_i = np.asarray(i)
    nump_i = np.expand_dims(nump_i, axis=0).repeat(3, 0) /255
    # nump_i = np.expand_dims(np.expand_dims(nump_i, axis=2).repeat(3, 2), axis=0)


    image = np.moveaxis(nump_i, 0, -1)  # cv works with ither axis order
    # resize
    scale = 0.5
    new_width, new_height = round(image.shape[1] * scale), round(image.shape[0] * scale)
    import cv2
    new_image = cv2.resize(image, (new_width, new_height), interpolation=cv2.INTER_AREA)
    new_image = np.moveaxis(new_image, -1, 0)
    new_image = np.expand_dims(new_image, axis=0)

    blacking_size = 25
    step = 10
    import matplotlib.pyplot as plt

    img_mean = new_image.mean()
    new_image = torch.from_numpy(new_image).to(device=device)

    rows = []

    shaded_image = new_image.clone()

    shaded_image.requires_grad = True

    m[0].eval()

    prediction = m[0](shaded_image.float())
    probability = torch.softmax(prediction, dim=1)[0][1]

    loss = BCELoss(reduction='sum')
    error = loss(probability, torch.Tensor([1]))
    error.backward()

    perturbation = torch.sign(shaded_image.grad.sum(1)).detach()

    maxx = 0.001
    number = 10
    scales = sorted(np.concatenate([-(np.linspace(0,maxx,number)), (np.linspace(0,maxx,number))]))

    image_probs = np.zeros(len(scales))
    image_featuremaps = np.zeros((len(scales), m[0].num_ftrs))

    with torch.no_grad():
        for i, e in enumerate(tqdm(scales)):
            adversarial_torch = (-e * perturbation.repeat_interleave(3, 0) + shaded_image)
            adversarial = np.moveaxis(adversarial_torch[0].detach().cpu().numpy(), 0, -1)
            adversarial_tensor = torch.from_numpy(adversarial).to(device=device)

            prediction_adv = m[0](adversarial_torch.float())
            probability_adv = torch.softmax(prediction_adv, dim=1)[0][1]
            feature_map = m[0].get_flatten_feature_map(adversarial_torch.float())

            # plt.imshow(adversarial)
            # plt.title(f'adversarial_{e}, probability: {probability_adv}')
            # plt.savefig(img_path+f'adversarial_{e}.png')
            # plt.close()
            # print(probability_adv, e)

            image_probs[i] = probability_adv.detach().cpu().item()
            image_featuremaps[i,:] = (feature_map.cpu().numpy())

            # advs.append((adversarial_torch.detach(), e))

    df = pd.DataFrame()
    df['scale'] = scales
    df['feature_map'] = [fm for fm in image_featuremaps]
    df['probability'] = image_probs
    df.to_hdf(img_path+'.h5', 'data')

    # plot probs
    # fig = plt.figure()
    # heatmap = plt.imshow(probs)
    # plt.colorbar(heatmap)
    # fig.tight_layout()
    # plt.savefig(img_path+'modified.png')
    # plt.close()


    # plot probs with image

    # fig, ax = plt.subplots()
    #    fig = plt.figure()

    # scale = new_height / probs.shape[0]
    # new_width, new_height = round(probs.shape[1] * scale), round(probs.shape[0] * scale)
    # import cv2
    # new_image_2 = cv2.resize(probs, (new_width, new_height), interpolation=cv2.INTER_AREA)
    # if PLOT_MALIGNANCY_SCORE:
    #     new_image_2 = (1-new_image_2)
    # #    highlight = 0.99
    # #    new_image_2 = highlight*(new_image_2  / new_image_2.max()) + 1 - highlight
    # #    new_image_2 = new_image_2 > np.percentile(new_image_2, 75)
    # new_image_2 = np.clip(new_image_2,new_image_2.mean(), new_image_2.max())
    # new_image_2 = new_image_2 - new_image_2.min()
    # new_image_2 = new_image_2 / new_image_2.max()
    #
    # plot_image = new_image.cpu().numpy()[0,...]
    # plot_image[0, ...] = new_image_2 + plot_image[0, ...]
    # plot_image = np.moveaxis(plot_image, 0, -1)
    # heatmap = ax.imshow(plot_image)
    # #heatmap = plt.imshow(new_image.cpu().numpy()[0,0,...] * new_image_2)
    # ax.spines['top'].set_visible(False)
    # ax.spines['left'].set_visible(False)
    # ax.spines['bottom'].set_visible(False)
    # ax.spines['right'].set_visible(False)
    # ax.set_xticks([])
    # ax.set_yticks([])
    #
    # #fig.tight_layout()
    # plt.gca().set_axis_off()
    # plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
    #
    # plt.margins(0,0)
    # plt.gca().xaxis.set_major_locator(plt.NullLocator())
    # plt.gca().yaxis.set_major_locator(plt.NullLocator())
    #
    # plt.savefig(img_path+'modified_plus_origin.png', bbox_inches = 'tight',
    #             pad_inches = 0)
    #
    # plt.close()

#
# for model_path in [0,1,2,3,4,5,6,7,8]:
#     mp = str(model_path)+'.checkpoint'
#     m = TorchModelLoader.load('./TMP/'+mp,device)
#     m[0].eval()
#     print(mp)
#
#     for adv, e in advs:
#         prediction = m[0](adv.float())
#         probability = torch.softmax(prediction, dim=1)[0][1]
#         print(f'{e:.{5}f}', '\t', f'{probability.item():.{10}f}')
#         if e == 0:
#             print()
