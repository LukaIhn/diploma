import os

import click
import torch
import numpy as np
import cv2
from PIL import Image
import matplotlib.pyplot as plt
from src.models.workers import TorchModelLoader

PLOT_MALIGNANCY_SCORE = True

img_pathes = [
    './TMP/benign_incorrect_example/P_00979|LEFT|CC|mask|mass|BENIGN|0.png',
    './TMP/benign_incorrect_example/P_01204|RIGHT|MLO|mask|mass|MALIGNANT|0.png',
    './TMP/benign_incorrect_example/P_01617|LEFT|CC|mask|mass|MALIGNANT|0.png',
]

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')


@click.command()
@click.option('--image_path', default=img_pathes[1], type=str, help='Relative or absolute path to the image')
@click.option('--image_model', default=None, type=str, help='Relative or absolute path to the model checkpoint.')
@click.option('--size_of_occlusion', default=50, type=int,
              help='Defines how bid part of an image would be occluded in pixels.')
@click.option('--step_of_occlusion', default=100, type=int, help='Use kaggle-ddsm dataset.')
def main(**kwargs):
    img_path = kwargs['image_path']
    img_dir = os.path.dirname(img_path)
    new_images_dir = os.path.join(img_dir, 'GENERATED')
    img_name = os.path.basename(img_path)
    os.makedirs(new_images_dir, exist_ok=True)

    new_image_path = os.path.join(new_images_dir, img_name)

    step = kwargs['step_of_occlusion']
    occlusion_size = kwargs['size_of_occlusion']

    model, config = TorchModelLoader.load('./TMP/0.checkpoint', device)
    model.eval()

    i = Image.open(img_path)
    nump_i = np.asarray(i)
    nump_i = np.expand_dims(nump_i, axis=0).repeat(3, 0) / 255

    image = np.moveaxis(nump_i, 0, -1)  # cv works with other axis order than pytorch

    # resize image so that it has the same size as images that were used while training the model
    scale = 0.5
    new_width, new_height = round(image.shape[1] * scale), round(image.shape[0] * scale)
    resized_image = cv2.resize(image, (new_width, new_height), interpolation=cv2.INTER_AREA)
    resized_image = np.moveaxis(resized_image, -1, 0)
    resized_image = np.expand_dims(resized_image, axis=0)
    resized_image_torch = torch.from_numpy(resized_image).to(device=device)

    rows = []

    from tqdm import tqdm
    with torch.no_grad():
        for i in tqdm(np.arange(resized_image_torch.shape[2], step=step)):
            occlusion_row = []
            for j in np.arange(resized_image_torch.shape[3], step=step):
                shaded_image = resized_image_torch.clone()
                x_min = max(0, i - occlusion_size // 2)
                x_max = min(resized_image_torch.shape[2], i + occlusion_size // 2)

                y_min = max(0, j - occlusion_size // 2)
                y_max = min(resized_image_torch.shape[3], j + occlusion_size // 2)

                shaded_image[:, :, x_min:x_max, y_min:y_max] = 0.5

                prediction = model(shaded_image.float())
                occluded_image_probability = torch.softmax(prediction, dim=1)[0][1]
                occlusion_row.append(occluded_image_probability)
            rows.append(occlusion_row)

    # move to cpu if they were on GPU
    new_rows = []
    for row in rows:
        row_np = np.array([elem.cpu().item() for elem in row])
        new_rows.append(row_np)
    probs = np.array(new_rows)

    # plot probs
    fig = plt.figure()
    heatmap = plt.imshow(probs)
    cbar = plt.colorbar(heatmap)
    cbar.set_label('probability when occluded', rotation=270)
    cbar.set_clim(0, 1)
    fig.tight_layout()
    plt.savefig(new_image_path + '_occlusion_probabilities.png')
    plt.close()

    resized_image_torch = torch.from_numpy(resized_image).to(device=device)
    resized_image_torch.requires_grad = True
    prediction = model(resized_image_torch.float())
    probability = torch.softmax(prediction, dim=1)[0][1]
    from torch.nn import BCELoss
    loss = BCELoss(reduction='sum')
    target = torch.Tensor([1]).to(device=device)
    error = loss(probability*(1-1e-4), target)
    error.backward()


    # plot image with highlighted regions that are responsible for the classificaion
    fig, ax = plt.subplots()
    #    fig = plt.figure()

    scale = new_height / probs.shape[0]
    new_width, new_height = round(probs.shape[1] * scale), round(probs.shape[0] * scale)

    occlusion_probabilities = cv2.resize(probs, (new_width, new_height), interpolation=cv2.INTER_AREA)
    if PLOT_MALIGNANCY_SCORE:
        occlusion_probabilities = (1 - occlusion_probabilities)

    occlusion_probabilities = np.clip(occlusion_probabilities, occlusion_probabilities.mean(), occlusion_probabilities.max())
    occlusion_probabilities = occlusion_probabilities - occlusion_probabilities.min()
    occlusion_probabilities = occlusion_probabilities / occlusion_probabilities.max()

    resized_image_torch = torch.from_numpy(resized_image).to(device=device)
    plot_image = resized_image_torch.cpu().numpy()[0, ...]
    plot_image[0, ...] = occlusion_probabilities + plot_image[0, ...]
    plot_image = np.moveaxis(plot_image, 0, -1)
    ax.imshow(plot_image)
    ax.spines['top'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.set_xticks([])
    ax.set_yticks([])

    # fig.tight_layout()
    plt.gca().set_axis_off()
    plt.subplots_adjust(left=0, right=1, top=1, bottom=0)

    plt.margins(0, 0)
    plt.gca().xaxis.set_major_locator(plt.NullLocator())
    plt.gca().yaxis.set_major_locator(plt.NullLocator())

    plt.savefig(new_image_path + '_occlusion_over_original.png', bbox_inches='tight',
                pad_inches=0)

    plt.close()

if __name__ == '__main__':
    main()
