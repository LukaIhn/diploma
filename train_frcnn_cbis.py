# Based on sample code from the TorchVision 0.3 Object Detection Finetuning Tutorial
# http://pytorch.org/tutorials/intermediate/torchvision_tutorial.html
from collections import OrderedDict

import click
import torch
import numpy as np
import pandas as pd

import torchvision
from torch import nn
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor, FasterRCNN
from torchvision.models.detection.mask_rcnn import MaskRCNNPredictor
from torchvision.models.detection.roi_heads import RoIHeads
from torchvision.models.detection.rpn import AnchorGenerator

from src.data.CBIS.cbis_directory_structure_helper import CbisDetectionColumns
from src.frcnn.cbis_detection_dataset import CBISDetectionDataset
from src.frcnn.engine import train_one_epoch, evaluate
import src.frcnn.utils as utils
import src.frcnn.transforms as T

def new_forward(self, features, proposals, image_shapes, targets=None):
    """
    Arguments:
        features (List[Tensor])
        proposals (List[Tensor[N, 4]])
        image_shapes (List[Tuple[H, W]])
        targets (List[Dict])
    """
    if self.training:
        proposals, matched_idxs, labels, regression_targets = self.select_training_samples(proposals, targets)

    box_features = self.box_roi_pool(features, proposals, image_shapes)
    box_features = self.box_head(box_features)
    class_logits, box_regression = self.box_predictor(box_features)

    result, losses = [], {}
    if self.training:
        from torchvision.models.detection.roi_heads import fastrcnn_loss
        loss_classifier, loss_box_reg = fastrcnn_loss(
            class_logits, box_regression, labels, regression_targets)
        losses = dict(loss_classifier=loss_classifier, loss_box_reg=loss_box_reg)
    else:
        boxes, scores, labels = self.postprocess_detections(class_logits, box_regression, proposals, image_shapes)
        num_images = len(boxes)
        import torch.nn.functional as FF
        for i in range(num_images):
            result.append(
                dict(
                    boxes=boxes[i],
                    labels=labels[i],
                    scores=scores[i],
                    predictioins=FF.softmax(class_logits, -1)
                )
            )

    if self.has_mask:
        mask_proposals = [p["boxes"] for p in result]
        if self.training:
            # during training, only focus on positive boxes
            num_images = len(proposals)
            mask_proposals = []
            pos_matched_idxs = []
            for img_id in range(num_images):
                pos = torch.nonzero(labels[img_id] > 0).squeeze(1)
                mask_proposals.append(proposals[img_id][pos])
                pos_matched_idxs.append(matched_idxs[img_id][pos])

        mask_features = self.mask_roi_pool(features, mask_proposals, image_shapes)
        mask_features = self.mask_head(mask_features)
        mask_logits = self.mask_predictor(mask_features)

        loss_mask = {}
        if self.training:
            gt_masks = [t["masks"] for t in targets]
            gt_labels = [t["labels"] for t in targets]
            from torchvision.models.detection.roi_heads import maskrcnn_loss
            loss_mask = maskrcnn_loss(
                mask_logits, mask_proposals,
                gt_masks, gt_labels, pos_matched_idxs)
            loss_mask = dict(loss_mask=loss_mask)
        else:
            labels = [r["labels"] for r in result]
            from torchvision.models.detection.roi_heads import maskrcnn_inference
            masks_probs = maskrcnn_inference(mask_logits, labels)
            for mask_prob, r in zip(masks_probs, result):
                r["masks"] = mask_prob

        losses.update(loss_mask)

    if self.has_keypoint:
        keypoint_proposals = [p["boxes"] for p in result]
        if self.training:
            # during training, only focus on positive boxes
            num_images = len(proposals)
            keypoint_proposals = []
            pos_matched_idxs = []
            for img_id in range(num_images):
                pos = torch.nonzero(labels[img_id] > 0).squeeze(1)
                keypoint_proposals.append(proposals[img_id][pos])
                pos_matched_idxs.append(matched_idxs[img_id][pos])

        keypoint_features = self.keypoint_roi_pool(features, keypoint_proposals, image_shapes)
        keypoint_features = self.keypoint_head(keypoint_features)
        keypoint_logits = self.keypoint_predictor(keypoint_features)

        loss_keypoint = {}
        if self.training:
            gt_keypoints = [t["keypoints"] for t in targets]
            from torchvision.models.detection.roi_heads import keypointrcnn_loss
            loss_keypoint = keypointrcnn_loss(
                keypoint_logits, keypoint_proposals,
                gt_keypoints, pos_matched_idxs)
            loss_keypoint = dict(loss_keypoint=loss_keypoint)
        else:
            from torchvision.models.detection.roi_heads import keypointrcnn_inference
            keypoints_probs, kp_scores = keypointrcnn_inference(keypoint_logits, keypoint_proposals)
            for keypoint_prob, kps, r in zip(keypoints_probs, kp_scores, result):
                r["keypoints"] = keypoint_prob
                r["keypoints_scores"] = kps

        losses.update(loss_keypoint)

    return result, losses

RoIHeads.forward = new_forward



def getprob(self, images, targets=None):
    """
    Arguments:
        images (list[Tensor]): images to be processed
        targets (list[Dict[Tensor]]): ground-truth boxes present in the image (optional)

    Returns:
        result (list[BoxList] or dict[Tensor]): the output from the model.
            During training, it returns a dict[Tensor] which contains the losses.
            During testing, it returns list[BoxList] contains additional fields
            like `scores`, `labels` and `mask` (for Mask R-CNN models).

    """
    if self.training and targets is None:
        raise ValueError("In training mode, targets should be passed")
    original_image_sizes = [img.shape[-2:] for img in images]
    images, targets = self.transform(images, targets)
    features = self.backbone(images.tensors)
    if isinstance(features, torch.Tensor):
        features = OrderedDict([(0, features)])
    proposals, proposal_losses = self.rpn(images, features, targets)
    detections, detector_losses = self.roi_heads(features, proposals, images.image_sizes, targets)
    detections_new = self.transform.postprocess(detections, images.image_sizes, original_image_sizes)

    losses = {}
    losses.update(detector_losses)
    losses.update(proposal_losses)

    if self.training:
        return losses

    return detections, detections_new


def get_model_instance_segmentation(num_classes):
    # load an instance segmentation model pre-trained pre-trained on COCO
    model = torchvision.models.detection.maskrcnn_resnet50_fpn(pretrained=True)

    # get number of input features for the classifier
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    # replace the pre-trained head with a new one
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)

    # now get the number of input features for the mask classifier
    in_features_mask = model.roi_heads.mask_predictor.conv5_mask.in_channels
    hidden_layer = 256
    # and replace the mask predictor with a new one
    model.roi_heads.mask_predictor = MaskRCNNPredictor(in_features_mask,
                                                       hidden_layer,
                                                       num_classes)

    return model


class AdaptiveAveragePooling(nn.Module):
    def __init__(self, **kwargs):
        super(AdaptiveAveragePooling, self).__init__()
        self.avgpool = nn.AdaptiveAvgPool2d((500, 300))

    def forward(self, _input: torch.Tensor) -> torch.Tensor:
        return self.avgpool(_input)


def get_frcnn_model(num_classes=3, debug: bool = False, kwargs=None):
    # load a pre-trained model for classification and return
    # only the features
    if debug:
        backbone = AdaptiveAveragePooling()
        backbone.out_channels = 3
    else:
        backbone = torchvision.models.mobilenet_v2(pretrained=True).features
        backbone.out_channels = 1280

    anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                       aspect_ratios=((0.5, 1.0, 2.0),))

    roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=[0],
                                                    output_size=7,
                                                    sampling_ratio=2)

    # put the pieces together inside a FasterRCNN model
    model = FasterRCNN(backbone,
                       num_classes=num_classes,
                       rpn_anchor_generator=anchor_generator,
                       box_roi_pool=roi_pooler,
                       max_size=kwargs['max_size'],
                       rpn_fg_iou_thresh=kwargs['rpn_fg_iou_thresh'],
                       rpn_nms_thresh=kwargs['rpn_nms_thresh'],
                       box_score_thresh=kwargs['box_score_thresh'],
                       )
    return model


def get_transform(train):
    transforms = []
    transforms.append(T.ToTensor())
    if train:
        transforms.append(T.RandomHorizontalFlip(0.5))
    return T.Compose(transforms)


@click.command()
@click.option('--train-batch-size', type=int, default=2)
@click.option('--load_data_into_ram', type=bool, default=False)
@click.option('--debug', type=bool, default=True)
@click.option('--num_epochs', type=int, default=10)
@click.option('--data_dir', type=str, default='./data/FRCNN_CBIS2/')

@click.option('--use_pretrained', type=str, default=True)
@click.option('--max_size', type=int, default=2100)
@click.option('--rpn_fg_iou_thresh', type=float, default=0.5)
@click.option('--rpn_nms_thresh', type=float, default=0.1)
@click.option('--box_score_thresh', type=float, default=0.1)
@click.option('--lr', type=float, default=0.005)
@click.option('--lr_step_size', type=int, default=3)
@click.option('--lr_gamma', type=float, default=0.1)
def main(**kwargs):
    # train on the GPU or on the CPU, if a GPU is not available
    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    # read configurations
    train_batch_size = kwargs['train_batch_size']
    debug = kwargs['debug']
    num_epochs = kwargs['num_epochs']
    data_dir = kwargs['data_dir']

    # our dataset has three classes - background, malignant and benign
    num_classes = 3
    # use our dataset and defined transformations
    if kwargs['load_data_into_ram']:
        metadata = CBISDetectionDataset.load_metadata(data_dir)
        unique_cases = len(np.unique(metadata[CbisDetectionColumns.CASE_NAME_ORDER_ID].values))

        image_data = np.array(CBISDetectionDataset.load_image_data(data_dir, unique_cases))
    else:
        image_data = None


    dataset = CBISDetectionDataset(data_dir, get_transform(train=True), data_array=image_data)
    dataset_test = CBISDetectionDataset(data_dir, get_transform(train=False), data_array=image_data)

    # split the dataset in train and test set
    # indices = torch.randperm(len(dataset)).tolist()
    dataset = torch.utils.data.Subset(dataset, dataset.get_train_ids())
    dataset_test = torch.utils.data.Subset(dataset_test, dataset_test.get_test_ids())

    # define training and validation data loaders
    data_loader = torch.utils.data.DataLoader(
        dataset, batch_size=train_batch_size, shuffle=True, num_workers=4,
        collate_fn=utils.collate_fn)

    data_loader_test = torch.utils.data.DataLoader(
        dataset_test, batch_size=1, shuffle=False, num_workers=4,
        collate_fn=utils.collate_fn)

    # get the model using our helper function
    if kwargs['use_pretrained']:
        kwargs['num_classes'] = num_classes
        model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True,
                                                                     max_size=kwargs['max_size'],
                                                                     rpn_fg_iou_thresh=kwargs['rpn_fg_iou_thresh'],
                                                                     rpn_nms_thresh=kwargs['rpn_nms_thresh'],
                                                                     box_score_thresh=kwargs['box_score_thresh'])
        # replace the classifier with a new one, that has
        # num_classes which is user-defined
        # get number of input features for the classifier
        in_features = model.roi_heads.box_predictor.cls_score.in_features
        # replace the pre-trained head with a new one
        model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)
    else:
        model = get_frcnn_model(num_classes, debug, kwargs)

    # model.getprob = getprob
    # model.roi_heads.forward = Helper.forward
    # move model to the right device
    model.to(device)

    # construct an optimizer
    params = [p for p in model.parameters() if p.requires_grad]
    optimizer = torch.optim.SGD(params, lr=kwargs['lr'],
                                momentum=0.9, weight_decay=0.0005)
    # and a learning rate scheduler
    lr_scheduler = torch.optim.lr_scheduler.StepLR(optimizer,
                                                   step_size=kwargs['lr_step_size'],
                                                   gamma=kwargs['lr_gamma'])

    for epoch in range(num_epochs):
        # train for one epoch, printing every 10 iterations
        train_one_epoch(model, optimizer, data_loader, device, epoch, print_freq=500)
        # update the learning rate
        lr_scheduler.step()
        # evaluate on the test dataset
        evaluate(model, data_loader_test, device=device)

    print("That's it!")


if __name__ == "__main__":
    main()
