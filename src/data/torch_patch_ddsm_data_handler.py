import logging
import os
from typing import Tuple, Optional

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from torch.utils.data import DataLoader
from torch.utils.data.dataset import Dataset

from src.constants.names import TRAIN_PROCESSORS, VALIDATION_PROCESSORS, TEST_PROCESSORS, NAME, UNMODIFIED, \
    NUMBER_OF_FOLDS, BALANCE_CLASSES, COMMON_PROCESSORS, TRAIN_ONLY
from src.constants.miscellaneous import PATCH_DDSM_PROCESSED, TRAIN, TEST, IMAGE, LABEL, DATA, VALIDATION, ID, PARAMS, \
    NUMBER_OF_PATTERNS_TO_LOG, NUMBER_OF_NOISED_PATTERNS_TO_LOG
from src.data.pattern_processors import processor_registry, ProcessorHelper


class PatchDDSMDataset(Dataset):
    """
    Implementation of PyTorch dataset
    """

    def __init__(self, images: np.array, labels: np.array, processor: ProcessorHelper = None, seed=42):
        """

        :param images:
        :param labels:
        :param processor: defines the transformations that would be applied before returning the sample.
        Attention, ordering of the list is important as it defines the order in which the processors would be applied.
        :param seed:
        """
        self.processor = processor
        self.images = images
        self.labels = labels
        self.seed = seed  # TODO: use somewhere

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, idx):
        image = np.squeeze(
            np.expand_dims(self.images[idx, ...], axis=0).repeat(3, 0))  # TODO: rewrite dataset processing

        sample = self.processor.get({IMAGE: image, LABEL: self.labels[idx], ID: idx})

        return sample


class DataHandler:
    def __init__(self, data_directory: str, params: dict, train_data, train_labels, test_data, test_labels,
                 provided_fold_id_initializer,
                 train_metafeatures: Optional[pd.DataFrame] = None, test_metafeatures: Optional[pd.DataFrame] = None):
        self.logger = logging.getLogger('main')

        self._train_loader, self._validation_loader, self._test_loader = [None] * 3
        self._train_idxs, self.label_fold_dictionary = [None] * 2

        self.data_directory = data_directory

        # read train data
        self.logger.debug('Reading train npy images...')
        self.train_data = train_data
        self.logger.debug('Reading train npy labels...')
        self.train_labels = train_labels

        # fixing random seed for reproducibility
        self.np_seed = params[DATA]['seed']

        # read test data
        self.logger.debug('Reading the test data...')
        self.test_data = test_data
        self.test_labels = test_labels

        # folding
        self._number_of_folds = params[DATA][NUMBER_OF_FOLDS]  # for k-fold cross validation
        self._provided_fold_id_initializer = provided_fold_id_initializer

        # metadata
        self.train_metafeatures = train_metafeatures
        self.test_metafeatures = test_metafeatures

        # default parameters for the loaders
        self._batch_size = params[DATA]['batch_size']
        self._evaluation_batch_size = params[DATA]['evaluation_batch_size']

        self._balance_classes = params[DATA][BALANCE_CLASSES]
        if self._balance_classes:
            self._ballance_test_set()
        else:
            self._test_idxs = np.arange(start=0, stop=len(self.test_labels), dtype=np.uint32)

        # append common processors
        for processor in [TRAIN_PROCESSORS, VALIDATION_PROCESSORS, TEST_PROCESSORS]:
            params[processor] = params[COMMON_PROCESSORS] + params[processor]

        # data processors
        self.train_processor = ProcessorHelper(
            [processor_registry.get(p[NAME], p[PARAMS]) for p in params[TRAIN_PROCESSORS]])
        self.validation_processor = ProcessorHelper(
            [processor_registry.get(p[NAME], p[PARAMS]) for p in params[VALIDATION_PROCESSORS] if
             p[NAME] not in params[TRAIN_ONLY]])
        self.test_processor = ProcessorHelper(
            [processor_registry.get(p[NAME], p[PARAMS]) for p in params[TEST_PROCESSORS] if
             p[NAME] not in params[TRAIN_ONLY]])

        self._num_workers = params['num_workers']

        self.is_initialized = False

    def update_batch_sizes(self, params):
        """
        updates default batch sizes
        :param params:
        :return:
        """
        self._batch_size = params[DATA]['batch_size']
        self._evaluation_batch_size = params[DATA]['evaluation_batch_size']

    def check_parameters_are_compatible(self, data_directory: str, params: dict):
        """
        Checks whether the data sets generated with given parameters return the same shuffle and division into the folds
        as the ones generated with current parameters.
        :param params:
        :return:
        """
        seed = params[DATA]['seed']
        number_of_folds = params[DATA][NUMBER_OF_FOLDS]

        return (seed == self.np_seed) and \
               (number_of_folds == self._number_of_folds) and \
               (data_directory == self.data_directory)

    def initialize_loaders(self, fold_number: int = 0, batch_size: int = None, evaluation_batch_size: int = None):
        self.logger.debug('Initializing loaders.')

        if batch_size is None:
            batch_size = self._batch_size
        if evaluation_batch_size is None:
            evaluation_batch_size = self._evaluation_batch_size

        self._train_idxs, self._validation_idxs = self._get_fold_ids(fold_number)

        train_set = PatchDDSMDataset(images=self.train_data[self._train_idxs],
                                     # FIXME: put whole train data, do not subselect
                                     labels=self.train_labels[self._train_idxs], processor=self.train_processor)
        validation_set = PatchDDSMDataset(images=self.train_data[self._validation_idxs],
                                          labels=self.train_labels[self._validation_idxs],
                                          processor=self.validation_processor)

        test_set = PatchDDSMDataset(images=self.test_data, labels=self.test_labels, processor=self.test_processor)
        augmented_test = PatchDDSMDataset(images=self.test_data, labels=self.test_labels,
                                          processor=self.train_processor)

        num_workers = self._num_workers
        self._train_loader = DataLoader(dataset=train_set, batch_size=batch_size, shuffle=True, num_workers=num_workers)
        self._validation_loader = DataLoader(dataset=validation_set, batch_size=evaluation_batch_size,
                                             shuffle=False, num_workers=num_workers)
        self._test_loader = DataLoader(dataset=test_set, batch_size=evaluation_batch_size, shuffle=False,
                                       num_workers=num_workers)

        self._augmented_test_loader = DataLoader(dataset=augmented_test, batch_size=batch_size, shuffle=True,
                                                 num_workers=num_workers)
        self._augmented_test_loader.name = 'augmented_test'

        self.is_initialized = True
        self._name_the_loaders()
        self.logger.info('Loaders initialized.')

    def get_loaders(self) -> Tuple[DataLoader, DataLoader, DataLoader]:
        if self.is_initialized:
            return self._train_loader, self._validation_loader, self._test_loader
        else:
            raise ValueError('Loaders were not initialized.')

    def get_augmented_test_loader(self) -> DataLoader:
        return self._augmented_test_loader

    def log_loader_pattern_examples(self, dir_to_save: str):
        """
        Is used for saving examples of the patterns.
        For each target class NUMBER_OF_PATTERNS_TO_LOG patterns are selected. For each of them the unmodified image
         would be saved. As data processing is often stochastic NUMBER_OF_NOISED_PATTERNS_TO_LOG will be saved for each
         of the selected images.
        :param dir_to_save:
        :return:
        """
        assert os.path.isdir(dir_to_save)
        assert self.is_initialized

        def log_image(name, image):
            plt.imshow(image)
            plt.savefig(name)
            plt.close()

        for loader in self.get_loaders():
            dataset: PatchDDSMDataset = loader.dataset
            all_ids = np.arange(len(dataset.labels))
            for label in np.unique(dataset.labels):
                label_ids = all_ids[dataset.labels == label][:NUMBER_OF_PATTERNS_TO_LOG]
                for id_ in label_ids:
                    name_tag = os.path.join(dir_to_save, loader.name) + '_'.join([LABEL, str(label), ID, str(id_)])

                    image = dataset.images[id_, ...]
                    if len(image.shape) == 2:  # if there is no channel dimension add it
                        image = np.expand_dims(image, axis=0)

                        # we need to mode channel axis to be the last axis or visualizing purposes
                    axis_to_swap = image.shape.index(
                        min(image.shape))  # corresponds to the channel axis
                    unmodified_image = np.repeat(image, 3, axis=axis_to_swap)
                    unmodified_image = np.moveaxis(unmodified_image, axis_to_swap, -1)

                    log_image(name_tag + UNMODIFIED, unmodified_image)
                    for i in range(NUMBER_OF_NOISED_PATTERNS_TO_LOG):
                        name = f'{name_tag}_{i}'
                        processed_image = np.moveaxis(dataset.__getitem__(id_)[IMAGE], 0, 2)
                        processed_image = (processed_image - np.min(processed_image))
                        processed_image = processed_image / np.max(processed_image)
                        log_image(name, processed_image)

    def _get_fold_ids(self, fold_number: int):
        """
        Returns pattern idxs for train and test validation set for given fold_number.
        If fold dictionary was not created it creates one. Otherwise it reuses the one created earlier.

        Fold dictionary contains separation into folds for each unique label to assure folds have
        the same distribution of the labels.
        :param fold_number: indicates which fold would be used as the validation set
        :return:
        """
        if (fold_number >= self._number_of_folds) or (fold_number < 0):
            self.logger.error('Choosen fold should be in interval [0; {}]'.format(self._number_of_folds))

        train_id_list, validation_id_list = [], []
        if self.label_fold_dictionary is None:  # TODO: rewrite to be property
            self._initialize_fold_id_dict()

        # choosen fold goes to the validation set, all others go to the train set
        # each fold stores idxs
        for label, folds in self.label_fold_dictionary.items():
            for i, fold in enumerate(folds):
                if i == fold_number:
                    validation_id_list.extend(fold)
                else:
                    train_id_list.extend(fold)

        # to numpy
        train_idxs = np.array(train_id_list, dtype=np.uint32)
        validation_idxs = np.array(validation_id_list, dtype=np.uint32)

        # reorder to avoid having patterns being groupped by label
        np.random.seed(self.np_seed)
        np.random.shuffle(train_idxs)
        np.random.shuffle(validation_idxs)

        return train_idxs, validation_idxs

    def _initialize_fold_id_dict(self) -> None:
        """
        Separate data into k-folds using function provided by DataProvider.
        Creates dictionary with unique label values as keys and k arrays(folds) of idxs of the patterns labeled
        the same as their key value
        :return:
        """
        return self._provided_fold_id_initializer(data_handler=self)  # TODO: find some better solution

    def _name_the_loaders(self, train_name=TRAIN, validation_name=VALIDATION, test_name=TEST):
        """
        Names are added only for the sake of the logging purposes
        :param train_name: name
        :param validation_name: name
        :param test_name: name
        """
        if self.is_initialized == False:
            raise ValueError('Loaders should be initialized before adding names to them.')

        self._train_loader.name = train_name
        self._validation_loader.name = validation_name
        self._test_loader.name = test_name

    def _ballance_test_set(self):
        """
        ! WARNING ! ballances data inplace

        Finds the class with the smallest counts and cuts other classes to contain the same count.

        :return:
        """
        test_idxs = np.arange(start=0, stop=len(self.test_labels), dtype=np.uint32)
        selected_test_idxs_array = []

        unique_labels, counts = np.unique(self.test_labels, return_counts=True)
        min_count = min(counts)

        for label_value in unique_labels:
            idxs = test_idxs[self.test_labels == label_value][:min_count]
            selected_test_idxs_array.extend(idxs)

        test_idxs = np.array(sorted(selected_test_idxs_array))
        self._test_idxs = test_idxs

        self.test_data = self.test_data[test_idxs, ...]
        self.test_labels = self.test_labels[test_idxs, ...]

        self.logger.info('Test set was ballanced to have {} elements of each class.'.format(min_count))

    def _reset_data_seed(self):
        """Should be used before any usage of random sampling to guarantee the reproducibility."""
        np.random.seed(self.np_seed)

    def log_set_idxs(self, run_path):
        """
        Saving selected train and validation pattern ids to be able to make sure runs with the same data seed has the same idxs.

        :param run_path:
        :return:
        """
        for file_name, idxs in zip(['train_ids', 'validation_ids', 'test_ids'],
                                   [self._train_idxs, self._validation_idxs, self._test_idxs]):
            with open(os.path.join(run_path, file_name), mode='w') as file:
                for id_ in sorted(idxs):
                    file.write(str(id_) + ', ')
