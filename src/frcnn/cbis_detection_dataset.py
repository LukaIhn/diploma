from tqdm import tqdm

from prepare_cbis_detection_dataset import DATA_HEIGHT, DATA_WIDTH, DATA_FILE_NAME
from src.data.CBIS.cbis_directory_structure_helper import CbisDetectionColumns, DICOMMetadataCols, Pathology
import numpy as np
import pandas as pd
import os
import torch


class CBISDetectionDataset(object):
    def __init__(self, root: str, transforms, drop_benign_without_callback: bool = True, data_array=None):
        self.root = root
        self.transforms = transforms

        # load bboxes
        self.abnormalities: pd.DataFrame = self.load_metadata(self.root)
        self._unique_cases = len(np.unique(self.abnormalities[CbisDetectionColumns.CASE_NAME_ORDER_ID].values))

        # # load masks
        # self.data_masks = np.memmap(os.path.join(self.root, DATA_MASKS_FILE_NAME), dtype='bool',
        #                       mode='r', shape=(len(self.abnormalities), DATA_HEIGHT, DATA_WIDTH))

        # load memstore with images
        if data_array is None:
            self.imgs = self.load_image_data(self.root, self._unique_cases)
        else:
            self.imgs = data_array

    @staticmethod
    def load_metadata(root_dir) -> pd.DataFrame:
        return pd.read_hdf(os.path.join(root_dir, 'metafeatures.h5'))

    @staticmethod
    def load_image_data(root_dir, number_of_cases) -> np.ndarray:
        return np.memmap(os.path.join(root_dir, DATA_FILE_NAME), dtype=np.uint8, mode='r',
                  shape=(number_of_cases, DATA_HEIGHT, DATA_WIDTH))

    def __getitem__(self, idx):
        assert (idx < self._unique_cases), 'Incorrect ID!'

        # load images
        img = self.imgs[idx, ...]

        # load anomalies info
        anomaly_info = self.abnormalities[self.abnormalities[CbisDetectionColumns.CASE_NAME_ORDER_ID] == idx]
        anomaly_ids = anomaly_info.reset_index()['index'].values
        # # set of binary masks
        # masks = self.data_masks[anomaly_ids, ...]

        # get bounding box coordinates for each mask
        num_objs = len(anomaly_ids)
        boxes = []
        for i, r in anomaly_info.iterrows():
            boxes.append([r[CbisDetectionColumns.Y_MIN], r[CbisDetectionColumns.X_MIN],
                          r[CbisDetectionColumns.Y_MAX], r[CbisDetectionColumns.X_MAX]])

        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        # there is only one class
        labels = torch.as_tensor((anomaly_info['pathology'].values == 'MALIGNANT'), dtype=torch.int64) + 1

        # # masks
        # masks = torch.as_tensor(masks, dtype=torch.uint8)

        image_id = torch.tensor([idx])
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])
        # suppose all instances are not crowd
        iscrowd = torch.zeros((num_objs,), dtype=torch.int64)

        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["image_id"] = image_id
        target["area"] = area
        target["iscrowd"] = iscrowd
        # target["masks"] = None
        # target["masks"] = masks

        if self.transforms is not None:
            img, target = self.transforms(img, target)

        return img, target

    def __len__(self):
        return self._unique_cases

    def get_train_ids(self):
        case_ids = np.unique(self.abnormalities[self.abnormalities[CbisDetectionColumns.IS_TEST] != 1][
                                 CbisDetectionColumns.CASE_NAME_ORDER_ID].values)
        return case_ids

    def get_test_ids(self):
        case_ids = np.unique(self.abnormalities[self.abnormalities[CbisDetectionColumns.IS_TEST] == 1][
                                 CbisDetectionColumns.CASE_NAME_ORDER_ID].values)
        return case_ids

    def plot_id(self, id_):
        import matplotlib.pyplot as plt
        plt.figure()
        plt.close()
        p = self[id_]
        plt.imshow(p[0].numpy()[0,:,:])
        ymax, xmax, ymin, xmin, = p[1]['boxes'][0]
        x = [xmin, xmin, xmax, xmax]
        y = [ymin, ymax, ymin, ymax]
        plt.plot(x, y)


        plt.show()

