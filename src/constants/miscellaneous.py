import os

PATCH_DDSM_KAGGLE = 'skooch/ddsm-mammography'
PATCH_DDSM_ARCHIVE_NAME = 'ddsm-mammography.zip'

DATA = 'data'
TRAIN = 'train'
TEST = 'test'
VALIDATION = 'validation'

# DICTIONARY KEYS
H5_DATA_KEY = 'data'

# DIRECTORIES
RAW = 'raw'
INTERIM = 'interim'
PROCESSED = 'processed'
PATCH_DDSM = 'patch_ddsm'
PATCH_DDSM_CLEANED = 'patch_ddsm_cleaned'
PATCH_DDSM_RAW = os.path.join(RAW, PATCH_DDSM)
PATCH_DDSM_INTERIM = os.path.join(INTERIM, PATCH_DDSM)
PATCH_DDSM_PROCESSED = os.path.join(PROCESSED, PATCH_DDSM)
PATCH_DDSM_CLEANED_PROCESSED = os.path.join(PROCESSED, PATCH_DDSM_CLEANED)

IMAGE_DATA_FILE_NAME = '{}_data.npy'
LABELS_FILE_NAME = '{}_labels.npy'

CHECKPOINT_ENDING = '.checkpoint'

CONFIG_ALLOWED_TYPES = [dict, int, float]

# SAMPLE DICTIONARY KEYS
IMAGE = 'image'
LABEL = 'label'
ID = 'id'

MODEL = 'model'
PARAMS = 'params'

# NUMBERS
PATCH_DDSM_TRAIN_SIZE = 55885
PATCH_DDSM_IMAGE_SIZE = 299
MNIST_IMAGE_SIZE = 28

CONFIG_FILE_NAME = 'config.json'
MODEL_COMPARISON_CONFIG_FILE_NAME = 'model_comparison_config.json'
MODEL_COMPARISON_RESULT = 'model_comparison_result.json'

TABLE_LOGGER_SEPARATOR = ','
DICTIONARY_NAME_SEPARATOR = '%'
PARAM_SEPARATOR = '-'
PARAM_KEY_VALUE_SEPARATOR = ':'


NUMBER_OF_PATTERNS_TO_LOG = 5
NUMBER_OF_NOISED_PATTERNS_TO_LOG = 1

MULTITASK_LABEL_GROUPING = [[0, 2], [1, 3]]

TRAILING_HASHTAG = '###################'

DEFAULT_TRAINING_CONFIG_FILE_NAME = 'patch_ddsm_config_example'
DEFAULT_ANALYSIS_CONFIG_FILE_NAME = 'analysis_config_example'