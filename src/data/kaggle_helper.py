import os

from configs.kaggle_connection_data import USERNAME, KEY

class KaggleDataLoader:
    def __init__(self, data_set_name, destination_dir):
        os.environ['KAGGLEUSERNAME'] = USERNAME
        os.environ['KAGGLEKEY'] = KEY
        self.destination_dir = destination_dir
        self.data_set_name = data_set_name

    def download_data(self):
        os.system('kaggle datasets download  -p {} -d {}'.format(self.destination_dir, self.data_set_name))