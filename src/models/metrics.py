import itertools
import logging
from abc import ABC, abstractmethod
from typing import Optional, List

import torch
import numpy as np
from class_registry import ClassRegistry
from torch import nn
from torch.nn import CrossEntropyLoss, BCELoss

import src.constants.names as names
from src.constants.miscellaneous import ID, IMAGE, TABLE_LOGGER_SEPARATOR, MULTITASK_LABEL_GROUPING

from src.journaling.table_logger import TableLogger
from src.journaling.tensorboard_logger import TensorboardLogger

import sklearn
import sklearn.metrics

import matplotlib.pyplot as plt

from src.utils import plot_to_image

metrics_registry = ClassRegistry('name')


class MetricAbstract(ABC):
    """
    Thiss class should be inherited by any class implementing metric or loss.
    """

    # This value will be used to select which metric to instantiate from the register.
    name = None
    tag = ''

    def __init__(self, **parameters: dict):
        self.number_of_classes: int = parameters[names.NUMBER_OF_CLASSES]
        self.run_dir_path: str = parameters[names.RUN_DIR_PATH]
        self.set_name: str = parameters[names.SET_NAME]
        self.class_names = parameters[names.CLASS_NAMES]

    @abstractmethod
    def accumulate(self, target: torch.Tensor, prediction: torch.Tensor,
                   input_: dict = None) -> torch.Tensor:
        """
        This function is used to accumulate statistics of the batch.

        :param target: target labels
        :param prediction: predicted values
        :param input_: input values that were passed into model
        :return:
        """
        pass

    @abstractmethod
    def get_results(self):
        """
        This function is used to calculate the resulting statistic over the calculated data
        :return:
        """
        pass

    @abstractmethod
    def reset(self):
        """
        This method should be called after aggregation to reset accumulated data
        :return:
        """
        pass

    @abstractmethod
    def get_and_log_results(self, tensorboard_logger: TensorboardLogger, console_logger: logging.Logger, tag: str,
                            abscissa: int):
        """
        Logging into file, console and tensorboard

        :param tensorboard_logger:
        :param console_logger:
        :param tag:
        :param abscissa:
        :return:
        """
        pass

    def is_main(self) -> bool:
        """
        As only one metric at a time can be used to decide when to stop training or update the best model, there should
        be a way to mark this metric. This function returns boolean variable indicating such a metric.
        :return:
        """
        return False


class ComparableMetric(MetricAbstract):
    """
    Such metric can be used for early stopping or for saving the best model while the training
    """

    def __init__(self, device, **parameters: dict):
        super(ComparableMetric, self).__init__(**parameters)

        self.device = device

        self.is_stopping_metric: bool = parameters.get(names.METRIC_IS_SELECTIVE_PARAM, False)

    @abstractmethod
    def is_better_than(self, value: float):
        pass

    def is_main(self) -> bool:
        """
        As only one metric at a time can be used to decide when to stop training or update the best model, there should
        be a way to mark this metric. This function returns boolean variable indicating such a metric.
        :return:
        """
        return self.is_stopping_metric


class SingleValueMetric(ComparableMetric):
    """
    Such metric can be expressed in one number.
    accumulate and get_results returns single value
    """

    def __init__(self, **parameters: dict):
        super(SingleValueMetric, self).__init__(**parameters)
        self.table_file_name = '_'.join([self.set_name, self.name, self.tag]) + names.METRIC_FILE_ENDING
        self.column_names = [names.EPOCH, self.set_name]
        self._table_logger = TableLogger(run_dir_path=self.run_dir_path, file_name=self.table_file_name,
                                         column_names=self.column_names, separator=TABLE_LOGGER_SEPARATOR)

        self.reset()

    @abstractmethod
    def _calculate_value(self, target: torch.Tensor, input_: torch.Tensor):
        pass

    def reset(self):
        self._total_number = 0
        try:
            del self._accumulator
        except AttributeError as e:
            pass
        self._accumulator = torch.zeros((1), dtype=torch.float32, device=self.device)

    def accumulate(self, target: torch.Tensor, prediction: torch.Tensor,
                   input_: Optional[torch.Tensor] = None) -> torch.Tensor:
        batch_size = target.numel()
        new_value = self._calculate_value(target, prediction)

        self._total_number += batch_size
        # TODO: check if detachingis necessary (batch_size_search)
        self._accumulator += new_value.detach().cpu()

        return new_value / batch_size

    def get_results(self):
        if self._total_number == 0:
            raise ValueError('No values were accumulated. You need call `accumulate` at least on one non empty batch.')

        result = self._accumulator.item() / self._total_number

        return result

    def get_and_log_results(self, tensorboard_logger: TensorboardLogger, console_logger: logging.Logger, tag: str,
                            abscissa: int):
        value = self.get_results()
        name = '{}/{}'.format(tag, self.name)

        if self.tag:
            name += '_' + self.tag

        tensorboard_logger.log_float(name=name, value=value, abscissa=abscissa)
        console_logger.info('{} :{}'.format(name, str(value)))
        self._table_logger.append_row([abscissa, value])


@metrics_registry.register
class AccuracyMetric(SingleValueMetric):
    name = 'accuracy'

    def _calculate_value(self, target: torch.Tensor, input_: torch.Tensor):
        max_index = input_.argmax(dim=1)
        return (max_index == target).sum().type(torch.float32)

    def is_better_than(self, value: float):
        return self.get_results() > value


@metrics_registry.register
class AccuracyMultitaskMetric(AccuracyMetric):
    name = 'accuracy_multitask'

    def __init__(self, **parameters: dict):
        super(AccuracyMultitaskMetric, self).__init__(**parameters)
        self.grouping = parameters.get(names.LOSS_GROUPING, MULTITASK_LABEL_GROUPING)

    def _calculate_value(self, target: torch.Tensor, input_: torch.Tensor):
        new_predictions = input_ - input_.min(dim=1, keepdim=True)[0]

        g1 = self.grouping[0]
        g2 = self.grouping[1]

        # make values non relevant for task be equal to 0
        task_mask = (target == g1[0]) | (target == g1[1])
        subselection1 = new_predictions[task_mask]
        subselection1[:, g2] = 0
        subselection2 = new_predictions[~task_mask]
        subselection2[:, g1] = 0

        new_predictions[task_mask] = subselection1
        new_predictions[~task_mask] = subselection2

        return super(AccuracyMultitaskMetric, self)._calculate_value(target, new_predictions)

    def is_better_than(self, value: float):
        return self.get_results() > value


# TODO: check that AccuracyMultitaskMetricTwo equals to AccuracyMultitaskMetric
@metrics_registry.register
class AccuracyMultitaskMetricTwo(AccuracyMetric):
    name = 'accuracy_multitask_2'

    def __init__(self, **parameters: dict):
        super(AccuracyMultitaskMetricTwo, self).__init__(**parameters)
        self.grouping: List[List[int]] = parameters.get(names.LOSS_GROUPING, MULTITASK_LABEL_GROUPING)

        for g in self.grouping:
            assert len(g) == 2, \
                f'Loss {self.name} works only for class groups of 2 elements, have {len(g)} instead: {self.grouping}'

    def accumulate(self, target: torch.Tensor, prediction: torch.Tensor,
                   input_: Optional[torch.Tensor] = None) -> torch.Tensor:
        group_results = torch.zeros_like(self._accumulator)

        for group_indicies in self.grouping:
            task_mask = (target == group_indicies[0]) | (target == group_indicies[1])
            task_predictions = prediction[task_mask][:, group_indicies]
            task_targets = (target[task_mask] == group_indicies[1]).type(torch.int64)
            if len(task_predictions):
                group_results += super(AccuracyMultitaskMetricTwo, self).accumulate(task_targets,
                                                                                    task_predictions) * task_mask.numel()

        return group_results


@metrics_registry.register
class AccuracyGroupMetric(AccuracyMetric):
    name = 'accuracy_binary_grouped'

    def __init__(self, **parameters: dict):
        self.group = parameters[names.LOSS_GROUPING]
        self.tag = '_group_' + '_'.join([str(g) for g in self.group])

        super(AccuracyGroupMetric, self).__init__(**parameters)

        assert len(self.group) == 2, \
            f'Loss {self.name} works only for class groups of 2 elements, have {len(self.group)} instead: {self.group}'

    def accumulate(self, target: torch.Tensor, prediction: torch.Tensor,
                   input_: Optional[torch.Tensor] = None) -> torch.Tensor:
        new_predictions = prediction - prediction.min(dim=1, keepdim=True)[0]

        group_mask = (target == self.group[0]) | (target == self.group[1])

        new_predictions = new_predictions[group_mask][:, self.group]
        new_targets = (target[group_mask] == self.group[1]).type(torch.int64)

        res = torch.zeros_like(self._accumulator)
        if len(new_targets):
            res = super(AccuracyGroupMetric, self).accumulate(new_targets,
                                                              new_predictions) * group_mask.numel()
        return res

    # def _calculate_value(self, target: torch.Tensor, input_: torch.Tensor):
    #     new_predictions = input_ - input_.min(dim=1, keepdim=True)[0]
    #
    #     group_mask = (target == self.group[0]) | (target == self.group[1])
    #
    #     new_predictions = new_predictions[group_mask][:, self.group]
    #     new_targets = (target[group_mask] == self.group[1]).type(torch.int64)
    #
    #     return super(AccuracyGroupMetric, self)._calculate_value(new_targets, new_predictions)  # .to(target.device)


@metrics_registry.register
class CrossEntropyLossMetric(SingleValueMetric):
    name = 'cross_entropy_loss_metric'

    def __init__(self, **parameters: dict):
        super(CrossEntropyLossMetric, self).__init__(**parameters)
        self.loss_function = CrossEntropyLoss(reduction='sum')

    def _calculate_value(self, target: torch.Tensor, input_: torch.Tensor):
        return self.loss_function(input=input_, target=target)

    def is_better_than(self, value: float):
        return self.get_results() < value


@metrics_registry.register
class CrossEntropyLossMultitaskMetric(SingleValueMetric):
    name = 'cross_entropy_loss_multitask_metric'

    def __init__(self, **parameters: dict):
        super(CrossEntropyLossMultitaskMetric, self).__init__(**parameters)
        self.loss_function = BCELoss(reduction='sum')
        self.grouping = parameters.get('loss_grouping', MULTITASK_LABEL_GROUPING)
        self.smoothing = parameters.get('smoothing', 0.)

    def _calculate_value(self, target: torch.Tensor, input_: torch.Tensor):
        i_list = []
        t_list = []

        # task 1
        for g in self.grouping:
            task_mask = (target == g[0]) | (target == g[1])
            group_i = torch.softmax(input_[task_mask][:, g], dim=1)[:, 0]
            group_t = (target[task_mask] == g[0]).type(torch.FloatTensor).to(input_.device)

            i_list.append(group_i)
            t_list.append(group_t)

        i = torch.cat(i_list, 0)
        t = torch.cat(t_list, 0)

        if self.smoothing:
            t[t==1] = 1 - self.smoothing
            t[t==0] = self.smoothing

        return self.loss_function(input=i, target=t)

    # old version
    # def _calculate_value(self, target: torch.Tensor, input_: torch.Tensor):
    #     g1 = self.grouping[0]
    #     g2 = self.grouping[1]
    #
    #     # # task 1
    #     # task_mask = (target == g1[0]) | (target == g1[1])
    #     # i1 = torch.softmax(input_[task_mask][:, g1], dim=1)[:, 0]
    #     # t1 = (target[task_mask] == g1[0]).type(torch.FloatTensor).to(input_.device)
    #     # l1 = self.loss_function(input=i1, target=t1)
    #     #
    #     # # task 2
    #     # task_mask = ~task_mask
    #     # i2 = torch.softmax(input_[task_mask][:, g2], dim=1)[:, 0]
    #     # t2 = (target[task_mask] == g2[0]).type(torch.FloatTensor).to(input_.device)
    #     # l2 = self.loss_function(input=i2, target=t2)
    #     # return l1 + l2
    #
    #     # task 1
    #     task_mask = (target == g1[0]) | (target == g1[1])
    #     i1 = torch.softmax(input_[task_mask][:, g1], dim=1)[:, 0]
    #     t1 = (target[task_mask] == g1[0]).type(torch.FloatTensor).to(input_.device)
    #
    #     # task 2
    #     task_mask = ~task_mask
    #     i2 = torch.softmax(input_[task_mask][:, g2], dim=1)[:, 0]
    #     t2 = (target[task_mask] == g2[0]).type(torch.FloatTensor).to(input_.device)
    #
    #     i = torch.cat((i1, i2), 0)
    #     t = torch.cat((t1, t2), 0)
    #
    #     return self.loss_function(input=i, target=t)
    #


    def is_better_than(self, value: float):
        return self.get_results() < value


class HLoss(nn.Module):
    def __init__(self):
        super(HLoss, self).__init__()

    def forward(self, x):
        b = torch.softmax(x, dim=1) * torch.log_softmax(x, dim=1)
        b = -1.0 * b.sum()
        return b

@metrics_registry.register
class EntropyLossMultitaskMetric(SingleValueMetric):
    name = 'entropy_loss_multitask_metric'

    def __init__(self, **parameters: dict):
        super(EntropyLossMultitaskMetric, self).__init__(**parameters)
        self.loss_function = HLoss()
        self.grouping = parameters.get('loss_grouping', MULTITASK_LABEL_GROUPING)

    def _calculate_value(self, target: torch.Tensor, input_: torch.Tensor):
        i_list = []

        for g in self.grouping:
            task_mask = (target == g[0]) | (target == g[1])
            group_i = input_[task_mask][:, g]
            i_list.append(group_i)

        i = torch.cat(i_list, 0)

        return self.loss_function(i)

    def is_better_than(self, value: float):
        return self.get_results() < value

@metrics_registry.register
class AUCMetric(SingleValueMetric):
    name = 'auc_grouped_metric'

    def __init__(self, **parameters: dict):
        # should be called befor super class constructor because tag is used to create _table_logger
        self.group = parameters[names.LOSS_GROUPING]
        self.tag = '_group_' + '_'.join([str(g) for g in self.group])

        super(AUCMetric, self).__init__(**parameters)

        assert len(self.group) == 2, \
            f'Loss {self.name} works only for class groups of 2 elements, have {len(self.group)} instead: {self.group}'

        self.targets = []
        self.predictions = []
        self._total_number = 0

    def _calculate_value(self, target: torch.Tensor, input_: torch.Tensor):
        task_mask = (target == self.group[0]) | (target == self.group[1])
        group_input = torch.softmax(input_[task_mask][:, self.group], dim=1)[:, 1].numpy()
        group_target = (target[task_mask] == self.group[1]).type(torch.LongTensor).numpy()

        return sklearn.metrics.roc_auc_score(group_target, group_input)

    def accumulate(self, target: torch.Tensor, prediction: torch.Tensor,
                   input_: Optional[torch.Tensor] = None) -> torch.Tensor:
        batch_size = target.numel()

        self.targets.append(target.detach().to('cpu'))
        self.predictions.append(prediction.detach().to('cpu'))

        self._total_number += batch_size

        return None

    def reset(self):
        try:
            # TODO: check if is necessary (batch_size_search)
            del self.targets
            del self.predictions
        except AttributeError as e:
            pass

        self.targets = []
        self.predictions = []

        self._total_number = 0

    def get_results(self):
        if self._total_number == 0:
            raise ValueError('No values were accumulated. You need call `accumulate` at least on one non empty batch.')
        targets = torch.cat(self.targets, 0)
        predictions = torch.cat(self.predictions, 0)

        result = self._calculate_value(targets, predictions)

        return result

    def is_better_than(self, value: float):
        return self.get_results() < value


@metrics_registry.register
class ClassRepresentativeMetric(MetricAbstract):  # TODO: solve device issue
    name = 'class_representative_metric'

    def __init__(self, **parameters: dict):
        super(ClassRepresentativeMetric, self).__init__(**parameters)
        self.image_shape = parameters[names.IMAGE_SHAPE]
        self._dpi = parameters['dpi']

        self._representatives: dict[_Representative] = dict()

        self.table_file_name = '_'.join([self.set_name, self.name]) + names.METRIC_FILE_ENDING
        self.column_names = [names.EPOCH, names.TARGET, names.PREDICTED, names.IS_HIGHER, ID] + self.class_names
        self.table_logger = TableLogger(run_dir_path=self.run_dir_path, file_name=self.table_file_name,
                                        column_names=self.column_names, separator=TABLE_LOGGER_SEPARATOR)
        self.save_image_statistics = parameters[names.SAVE_IMAGE_STATISTICS]
        self.reset()

    def accumulate(self, target: torch.Tensor, prediction: torch.Tensor,
                   input_: Optional[torch.Tensor] = None) -> None:
        predictions_softmaxed = torch.nn.Softmax()(prediction)
        predicted_classes = prediction.argmax(dim=1)

        for target_class in range(self.number_of_classes):
            for predicted_class in range(self.number_of_classes):
                mask = (predicted_classes == predicted_class) & (target_class == target)

                if mask.sum().item() == 0:
                    continue

                predictions_subset = prediction[mask]
                predictions_softmaxed_subset = predictions_softmaxed[mask]

                ids_subset = input_[ID][mask]
                patterns_subset = input_[IMAGE][mask]

                for is_higher in [True, False]:
                    key = (target_class, predicted_class, is_higher)
                    representative: _Representative = self._representatives[key]
                    representative.update(predictions_subset, predictions_softmaxed_subset, ids_subset, patterns_subset)

    def get_results(self):
        return self._representatives

    def reset(self):
        for target in range(self.number_of_classes):
            for predicted in range(self.number_of_classes):
                for is_higher in [True, False]:
                    element = _Representative(target_class=target, predicted_class=predicted, is_higher=is_higher,
                                              number_of_classes=self.number_of_classes)
                    self._representatives[element.key()] = element

    def get_and_log_results(self, tensorboard_logger: TensorboardLogger, console_logger: logging.Logger, tag: str,
                            abscissa: int):
        for key, value in self.get_results().items():
            representative: _Representative = value
            target, predicted, is_higher = key
            image_tag = tag + '_target_{}_predicted_{}_is_higher_{}'.format(target, predicted, is_higher)
            predictions_image_tag = tag + '_target_{}_predicted_{}_is_higher_{}_predictions'.format(target, predicted,
                                                                                                    is_higher)

            image_to_log = representative.pattern
            if image_to_log is None:
                image_to_log = np.zeros(self.image_shape)
            else:
                image_to_log = image_to_log.numpy()

            tensorboard_logger.log_image(name=image_tag, image=image_to_log, abscissa=abscissa)
            data_row = [str(abscissa)] + representative.get_statistics()
            self.table_logger.append_row(data_row)

            if self.save_image_statistics:
                predictions_image_to_log = representative.get_predictions_as_image(self._dpi)
                tensorboard_logger.log_image(name=predictions_image_tag, image=predictions_image_to_log,
                                             abscissa=abscissa)


class _Representative:
    def __init__(self, target_class: int, predicted_class: int, is_higher: bool, number_of_classes: int,
                 pattern=None, prediction=None, id=None):
        self.target_class = target_class
        self.predicted_class = predicted_class
        self.is_higher = is_higher
        self.number_of_classes = number_of_classes

        self.pattern = pattern
        self.prediction = prediction
        self.id = id

    def key(self):
        return (self.target_class, self.predicted_class, self.is_higher)

    def is_empty(self):
        return self.pattern is None and self.prediction is None and self.id is None

    def update(self, predictions, predictions_softmaxed, ids, patterns):
        candidate_local_id = self._get_candidate_local_id(predictions_softmaxed)
        if self.is_empty() or self._should_be_updated_by(predictions_softmaxed[candidate_local_id]):
            self.pattern = patterns[candidate_local_id]
            self.prediction = predictions[candidate_local_id]
            self.id = ids[candidate_local_id]

    def _get_candidate_local_id(self, predictions_softmaxed):
        if self.is_higher:
            return predictions_softmaxed[:, self.target_class].argmax()
        return predictions_softmaxed[:, self.target_class].argmin()

    def _should_be_updated_by(self, class_distribution):
        if self.is_higher:
            return self.prediction.softmax(0)[self.target_class] < class_distribution[self.target_class]
        return self.prediction.softmax(0)[self.target_class] > class_distribution[self.target_class]

    def get_softmaxed_probabilities(self):
        return self.prediction.softmax(0)

    def get_predictions_as_image(self, dpi, class_names=None):
        if self.is_empty():
            return np.zeros((3, 5, 5))

        weights = self.get_softmaxed_probabilities()
        if class_names is None:
            class_names = [str(i) + ' ' + str(self.prediction[i]) for i in range(weights.shape[0])]

        figure = plt.figure(figsize=(8, 7), dpi=dpi)

        plt.hist(range(weights.shape[0]), facecolor='blue', alpha=0.5, weights=weights)
        tick_marks = np.arange(len(class_names))
        plt.xticks(tick_marks, class_names, rotation=45)
        plt.yticks(tick_marks, class_names)

        result = plot_to_image(figure)
        result = np.rollaxis(result, 2, 0)

        plt.close()

        return result

    def get_statistics(self, signs_after_comma=4) -> List[str]:
        if self.is_empty():
            class_predictions = ['nan'] * self.number_of_classes
        else:
            predictions = self.prediction
            class_predictions = [round(v.item(), signs_after_comma) for v in predictions]
        data_row = [self.target_class, self.predicted_class, self.is_higher, self.id] + class_predictions
        return [str(value_) for value_ in data_row]


@metrics_registry.register
class ConfusionMatrixMetric(MetricAbstract):  # TODO: solve device issue
    name = 'confusion_matrix_metric'

    def __init__(self, **parameters: dict):
        super(ConfusionMatrixMetric, self).__init__(**parameters)
        self._number_of_classes: int = parameters[names.NUMBER_OF_CLASSES]
        self._counts = None

        self._dpi = parameters['dpi']

        self.reset()

    def accumulate(self, target: torch.Tensor, prediction: torch.Tensor,
                   input_: Optional[torch.Tensor] = None) -> None:
        predicted_classes = prediction.argmax(dim=1)

        # Calculate the confusion matrix.
        cm = sklearn.metrics.confusion_matrix(target.cpu().numpy(), predicted_classes.cpu().numpy(),
                                              labels=list(range(self._number_of_classes)))

        if self._counts is None:
            self._counts = cm
        else:
            self._counts += cm

    def get_results(self):
        cm_image = self._plot_confusion_matrix(self._counts)

        return cm_image

    def reset(self):
        self._counts = None

    def get_and_log_results(self, tensorboard_logger: TensorboardLogger, console_logger: logging.Logger, tag: str,
                            abscissa: int):
        image = self.get_results()

        image = np.rollaxis(image, 2, 0)

        cm_tag = tag + '_confusion_matrix'

        tensorboard_logger.log_image(name=cm_tag, image=image, abscissa=abscissa)

    def _plot_confusion_matrix(self, cm, class_names=None):
        """
        Borrowed from https://www.tensorflow.org/tensorboard/r2/image_summaries.

        Returns a matplotlib figure containing the plotted confusion matrix.

        Args:
          cm (array, shape = [n, n]): a confusion matrix of integer classes
          class_names (array, shape = [n]): String names of the integer classes
        """
        if class_names is None:
            class_names = [str(i) for i in range(self._number_of_classes)]

        figure = plt.figure(figsize=(8, 7), dpi=self._dpi)
        plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
        plt.title("Confusion matrix")
        plt.colorbar()
        tick_marks = np.arange(len(class_names))
        plt.xticks(tick_marks, class_names, rotation=45)
        plt.yticks(tick_marks, class_names)

        # Normalize the confusion matrix.
        cm = np.around(cm.astype('float') / cm.sum(axis=1)[:, np.newaxis], decimals=2)

        # Use white text if squares are dark; otherwise black.
        threshold = cm.max() / 2.
        for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            color = "white" if cm[i, j] > threshold else "black"
            plt.text(j, i, cm[i, j], horizontalalignment="center", color=color)

        plt.ylabel('True label')
        plt.xlabel('Predicted label')

        res = plot_to_image(figure)
        plt.close()

        return res


@metrics_registry.register
class ConfusionMatrixMultitaskMetric(ConfusionMatrixMetric):
    name = 'confusion_matrix_multitask_metric'

    def __init__(self, **parameters: dict):
        super(ConfusionMatrixMultitaskMetric, self).__init__(**parameters)
        self.grouping = parameters.get('loss_grouping', MULTITASK_LABEL_GROUPING)

    def accumulate(self, target: torch.Tensor, prediction: torch.Tensor,
                   input_: Optional[torch.Tensor] = None) -> None:
        prediction = prediction - prediction.min(dim=1, keepdim=True)[0]

        g1 = self.grouping[0]
        g2 = self.grouping[1]

        # make values non relevant for task be equal to 0
        task_mask = (target == g1[0]) | (target == g1[1])
        subselection1 = prediction[task_mask]
        subselection1[:, g2] = 0
        subselection2 = prediction[~task_mask]
        subselection2[:, g1] = 0

        prediction[task_mask] = subselection1
        prediction[~task_mask] = subselection2

        super(ConfusionMatrixMultitaskMetric, self).accumulate(target, prediction, input_)
