from enum import Enum
from itertools import product
from PIL import Image

import png
import numpy as np
from tqdm import tqdm
import os

import pandas as pd

from src.data.CBIS.cbis_directory_structure_helper import CBISDicomPathHelper, DataSetTypes, AbnormalityType, \
    CbisDicomImageType, PngImageType
from src.data.dicom.dicom_helper import DicomHelper
from src.utils import check_dir

dicom_helper = DicomHelper()

METAFEATURE_STORAGE_NAME = 'metafeatures.h5'


class CBISFullSizePNGCreator:
    """
    Reads DICOMS and creates PNGS from them. It also loads case metadata and aggregates it into one HDFStore
    """
    def __init__(self, data_root_source: str, data_root_target: str):
        """

        :param data_root_source: dir with dicom images
        :param data_root_target: directory where png full images and masks would be stored
        """
        self.data_root_source = data_root_source
        self.data_root_target = data_root_target
        self.full_df = pd.DataFrame()

    def load_dicom(self, file_id, image_type: CbisDicomImageType, path_helper) -> np.ndarray:
        image_path = path_helper.get_patient_image_path(file_id, image_type)
        dicom_file = dicom_helper.read_dicom(image_path)
        return (255 * (dicom_file - np.min(dicom_file)) / np.max(dicom_file)).astype(np.uint8)

    def extract_one_patch(self, file_id, path_helper: CBISDicomPathHelper):
        # full_image_path = path_helper.get_patient_image_path(file_id, CbisDicomImageType.FULL)
        # mask_image_path = path_helper.get_patient_image_path(file_id, CbisDicomImageType.MASK)

        full_np_image = self.load_dicom(file_id, CbisDicomImageType.FULL, path_helper)

        mask_np_image = self.load_dicom(file_id, CbisDicomImageType.MASK, path_helper)
        if len(np.unique(mask_np_image)) > 2: # sometimes images are located on wrong place
            mask_np_image = self.load_dicom(file_id, CbisDicomImageType.CROPPED, path_helper)
        if len(np.unique(mask_np_image)) > 2:
            print(f'Unexpected format {file_id}, Path helper {path_helper.data_set_type.value}, {path_helper.malignancy_type.value}.')
            return


        self.check_or_store_full_image(file_id, full_np_image, path_helper)
        self.store_new_mask(file_id, path_helper, mask_np_image)

        for image_type in [CbisDicomImageType.CROPPED, CbisDicomImageType.MASK]:
            np_image = self.load_dicom(file_id, image_type, path_helper)
            shape = np_image.shape

            name = image_type.name + str(file_id) + '.png'
            path = os.path.join('data', 'CBIS', name)
            with open(path, 'wb') as png_file:
                w = png.Writer(shape[1], shape[0], greyscale=True)
                w.write(png_file, np_image)

    def check_or_store_full_image(self, file_id, full_image_to_store: np.ndarray, path_helper: CBISDicomPathHelper):
        """
        Check that case directory is created and full image inside it is created and equals to full_image_to_store
        :param file_id:
        :param full_image_to_store:
        :param path_helper:
        :return:
        """
        case_dir = path_helper.get_file_png_directory_by_index(file_id)
        full_img_path = os.path.join(self.data_root_target, case_dir, PngImageType.FULL.value + '.png')

        if not os.path.isdir(case_dir):
            check_dir(case_dir, create=True, recursively=True)

            im = Image.fromarray(full_image_to_store)
            im.save(full_img_path)
            return

        existing_full_img = Image.open(full_img_path)
        np.testing.assert_array_equal(full_image_to_store, existing_full_img)

    def store_new_mask(self, file_id, path_helper: CBISDicomPathHelper, np_image: np.ndarray):
        case_dir = path_helper.get_file_png_directory_by_index(file_id)
        mask_df = path_helper.get_mask_df(file_id)

        # png image path
        serial_id = self.get_number_of_stored_masks(case_dir)
        malignancy_type = path_helper.malignancy_type
        pathology = mask_df['pathology'].iloc[0]
        name = self.list_to_string([PngImageType.MASK, malignancy_type, pathology, serial_id]) + '.png'
        mask_img_path = os.path.join(self.data_root_source, case_dir, name)

        im = Image.fromarray(np_image)
        im.save(mask_img_path)

        # add new fields
        mask_df['case_dir'] = case_dir
        mask_df['mask_name'] = name
        mask_df['data_set_original_type'] = path_helper.data_set_type.value

        # so that in future we know how many of those have already been processed
        self.append_line_to_metadata_storage(mask_df)
        self.full_df = pd.concat([self.full_df, mask_df])

    def list_to_string(self, list, sep: str='|'):
        str_list = [str(s) for s in list]
        return sep.join(str_list)


    def get_number_of_stored_masks(self, case_dir):
        with pd.HDFStore(self.get_metadata_storage_path()) as storage:
            storage_is_not_empty = len(storage.keys()) > 0
            if storage_is_not_empty:
                df = storage.select('data')

                number_of_masks_stored_in_dir = len(df[df['case_dir'] == case_dir])

                return number_of_masks_stored_in_dir

            return 0

    def get_metadata_storage_path(self):
        return os.path.join(self.data_root_target, METAFEATURE_STORAGE_NAME)

    def append_line_to_metadata_storage(self, mask_df: pd.DataFrame):
        with pd.HDFStore(self.get_metadata_storage_path()) as storage:
            if storage.keys():
                df = storage.select('data')
                df = pd.concat([df, mask_df])
            else:
                df = mask_df

            storage.put('data', df, format='table')

    def create(self):
        for data_set_type in [DataSetTypes.TEST, DataSetTypes.TRAIN]:
            for malignancy_type in [AbnormalityType.CALCIFICATION_SHORT, AbnormalityType.MASS]:
                path_helper = CBISDicomPathHelper('/media/wm/Windows8_OS/CBIS_FULL', data_set_type, malignancy_type, data_root_target=self.data_root_target)

                for file_id in tqdm(range(len(path_helper._data_info))):
                    self.extract_one_patch(file_id, path_helper)

                # break
            # break

        # FIXME: remove
        self.full_df.to_csv('ololo.csv')

if __name__ == 'main':
    CBISFullSizePNGCreator(data_root_source='/media/wm/Windows8_OS/CBIS_FULL', data_root_target='/media/wm/Windows8_OS/CBIS_FULL/PNGS').create()