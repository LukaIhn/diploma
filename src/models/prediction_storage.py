from typing import List, Optional
import pandas as pd
import numpy as np
import sklearn
import sklearn.metrics
import torch

from src.constants import names
from src.constants.names import TestTimeAugmentations


def open_storage_decorator(function):
    """Handles opening the pd.HDFStore safely"""

    def decorated_function(self, **kwargs):
        if self.store is not None and self.store.is_open:
            return function(self, **kwargs)

        with pd.HDFStore(self.storage_path, mode='r') as store:
            self.store = store
            return function(self, **kwargs)

    return decorated_function


class Storage():
    def __init__(self, storage_path: str, metafeatures: Optional[pd.DataFrame] = None):
        self.storage_path = storage_path
        self.store = None

        self.metafeatures = metafeatures

    @open_storage_decorator
    def get_keys(self, **kwargs):
        return self.store.keys()

    @open_storage_decorator
    def get_stored_noise_stds(self, **kwargs) -> List[str]:
        keys = self.get_keys()
        noising_outputs_keys = [k for k in keys if k.startswith('/NOISE_STD')]
        from re import search as research
        stds = [research(r'[0-9]+', key).group(0) for key in noising_outputs_keys]  # find longest string of numbers
        float_stds = [float('0.' + std[1:]) if std.startswith('0') else float(std) for std in
                      stds]  # ('0.001' -> '0001')
        return np.unique(float_stds)

    def get_targets(self, drop_augmentation_info=True) -> pd.DataFrame:
        df = self._get_data(key=names.AnalysisStorageKeys.TARGETS, merge_with_targets=False)
        df['target'] = df[0]
        if drop_augmentation_info:
            return df[['target', 'id']].drop_duplicates()
        return df.drop(columns=[0])

    def get_unmodified_predictions(self, only_unaugmented: bool = False, merge_with_targets: bool = True, allow_horizontal=True) -> pd.DataFrame:
        predictions_df = self._get_data(names.AnalysisStorageKeys.PREDICTIONS, merge_with_targets)

        if only_unaugmented:
            mask = (predictions_df[TestTimeAugmentations.FLIP_VERTICALLY] == 0) & (
                           predictions_df[TestTimeAugmentations.ROTATE_90] == 0)
            if not allow_horizontal:
                mask = mask & (predictions_df[TestTimeAugmentations.FLIP_HORIZONTALLY] == 0)
            predictions_df = predictions_df[mask]
        return predictions_df

    def get_unmodified_features(self, merge_with_targets: bool = True) -> pd.DataFrame:
        return self._get_data(names.AnalysisStorageKeys.FEATURE_MAPS, merge_with_targets)

    def get_noised_predictions(self, std: float, merge_with_targets: bool = True) -> pd.DataFrame:
        key = self._get_noised_data_key(std, names.AnalysisStorageKeys.PREDICTIONS)
        return self._get_data(key, merge_with_targets)

    def get_noised_features(self, std: float, merge_with_targets: bool = True) -> pd.DataFrame:
        key = self._get_noised_data_key(std, names.AnalysisStorageKeys.FEATURE_MAPS)
        return self._get_data(key, merge_with_targets)

    def _get_noised_data_key(self, std: float, data_type: str) -> str:
        # OLD VERSION key = 'NOISE_STD_' + str(std).replace('.', '') + '__' + data_type
        key = 'NOISE_STD_' + str(f'{std:.{10}f}').replace('.', '') + '__' + data_type
        return key

    def _get_data(self, key: str, merge_with_targets: bool = True):
        df: pd.DataFrame = pd.read_hdf(self.storage_path, key=key)

        if merge_with_targets:
            return self._merge_with_targets(df)
        return df

    def _merge_with_targets(self, df: pd.DataFrame) -> pd.DataFrame:
        targets_df = self.get_targets()
        merged_df = df.merge(targets_df, how='left', left_on=['id'], right_on=['id'])
        return merged_df

    def get_noised_cross_entropy(self, std: float, merge_with_targets: bool = True):
        predictions_df = self.get_noised_predictions(std)
        return self._get_cross_entropy(predictions_df, merge_with_targets)

    def get_cross_entropy(self, merge_with_targets: bool = True):
        predictions_df = self.get_unmodified_predictions()
        return self._get_cross_entropy(predictions_df, merge_with_targets)

    def _get_cross_entropy(self, predictions_df: pd.DataFrame, merge_with_targets: bool = True):
        probabilities = torch.softmax(torch.Tensor(predictions_df[[0, 1]].values), 1).cpu().numpy()

        predictions_df['cross_entropy'] = -np.log(
            1e-6 + probabilities[range(len(predictions_df)), np.ix_(predictions_df['target'])][0,
                   :])  # [0, :] is for reshaping from (1, :) to shape (:)

        if merge_with_targets:
            return predictions_df
        else:
            return predictions_df[['cross_entropy', 'id']]

    def get_entropy(self):
        predictions_df = self.get_unmodified_predictions()
        predictions_tensor = torch.Tensor(predictions_df[[0, 1]].values)
        predictions_df['entropy'] = (-torch.softmax(predictions_tensor, dim=1) * torch.log_softmax(predictions_tensor, dim=1)).cpu().numpy().sum(1)

        return predictions_df

    def merge_with_metafeatures(self, df):

        meta_df = self.metafeatures.reset_index()
        merged_df = df.merge(meta_df, how='left', left_on=['id'], right_on=['index'])
        return merged_df

    def get_auc(self, only_unmodified: bool = True):
        predictions_df = self.get_unmodified_predictions(only_unmodified)

        return self._get_auc(predictions_df)

    def get_noised_auc(self, std: float):
        predictions_df = self.get_noised_predictions(std)
        return self._get_auc(predictions_df)

    def _get_auc(self, predictions_df: pd.DataFrame, aggregate_by_id: bool = True, aggregation_function = lambda a: a.mean()):
        predictions_df['probabilities'] = torch.softmax(torch.Tensor(predictions_df[[0, 1]].values), 1).cpu().numpy()[:,
                                          1]
        if aggregate_by_id:
            predictions_df = aggregation_function(predictions_df.groupby('id'))

        return sklearn.metrics.roc_auc_score(predictions_df['target'], predictions_df['probabilities'].values)
