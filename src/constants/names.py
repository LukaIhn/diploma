# DICTIONARY PARAMETERS NAMES
DATA_KEY_WORD = 'data%'

TASK = 'task'
METRIC_IS_SELECTIVE_PARAM = 'is_comparing_metric'
NAME = 'name'
PARAMETERS_NAME = 'parameters'
CALLBACK_NAMES = 'callback_names'
NUMBER_OF_CLASSES = 'number_of_classes'
IMAGE_SHAPE = 'image_shape'
RUN_DIR_PATH = 'run_dir_path'
SET_NAME = 'set_name'
EPOCH = 'epoch'
TARGET = 'target'
TARGETS = TARGET + 's'
PREDICTED = 'predicted'
PREDICTED_CLASSES = 'predicted_classes'
CLASS_NAMES = 'class_names'
IS_HIGHER = 'is_higher'
SAVE_IMAGE_STATISTICS = 'save_image_statistics'
DATAHASH = 'DATAHASH'
RUNHASH = 'RUNHASH'
GRIDSEARCH_UPDATED = 'gridsearch_updated'
TORCH_SEED = 'torch_seed'
TORCH_SAMPLING_SEED = 'torch_sampling_seed'
UNFREEZE_AFTER_EPOCH = 'unfreeze_on'
LR_SHEDULE = 'lr_shedule'
SEMISUPERVISED_SHEDULE = 'semisupervised_shedule'
UNSUPERVISED_WEIGHT = 'unsupervised_weight'
OPTIMIZER = 'optimizer'
CHOSEN_FOLD = 'chosen_fold'
WORKER = 'worker'
DEVICE = 'device'
RUN_NAME = 'run_name'
RUN_NUMBER = 'run_number'
EXPERIMENT_NAME = 'experiment_name'
DATA_DIRECTORY = 'data_directory'
RUN_PATH_RESOLVER = 'run_path_resolver'
GET_DATA_HASH = 'get_data_hash'

COMMON_PROCESSORS = 'common_processors'
TRAIN_PROCESSORS = 'train_processors'
VALIDATION_PROCESSORS = 'validation_processors'
TEST_PROCESSORS = 'test_processors'
TRAIN_ONLY = 'train_only'

TRAIN_LOSS_FUNCTION = 'train_loss_function'
ENTROPY_LOSS_FUNCTION = 'entropy_loss_function'
EVALUATION_METRICS = 'evaluation_metrics'

LOSS_GROUPING = 'loss_grouping'

METRIC_FILE_ENDING = '.metric'
UNMODIFIED = 'unmodified'

NUMBER_OF_FOLDS = 'number_of_folds'
SEED = 'seed'
BALANCE_CLASSES = 'balance_classes'
MODEL_NAME = 'model_name'
MODEL_PATH = 'model_path'
MODELS = 'models'
NOISE = 'noise'
NOISE_STD = 'noise_std'
NOISE_MEAN = 'noise_mean'
NUMBER_OF_REPEATS = 'number_of_repeats'
REPEAT_NUMBER = 'repeat_number'
LOADERS = 'loaders'

PREDICTIONS = 'predictions'
FEATURES = 'features'
FEATURE_MAP_DIM = 'hidden_layer_size'
# ERRORS
NON_IMPLEMENTED_ERROR_MESSAGE = 'Abstract method was not implemented'

MEMMAP_LOADING_ARGUMENTS = 'memmap_loading_arguments'
MEMAP_LOADING_ARGUMENT_NAMES = ['loader_prediction_storage',
                                'loader_feature_map_storage',
                                'loader_correct_number_storage',
                                'loader_class_predicted_counts_storage',
                                'loader_target_storage',
                                'loader_ids_storage']

RUN_PARAMETERS = 'run_parameters'
H5 = '.h5'


class AnalysisStorageKeys:
    TARGETS = 'targets'
    PREDICTIONS = PREDICTIONS
    FEATURE_MAPS = 'feature_maps'


class TestTimeAugmentations:
    KEY_IN_PARAMETERS_FILE = 'key_in_parameters_file'

    FLIP_HORIZONTALLY = 'flip_horizontally'
    FLIP_VERTICALLY = 'flip_vertically'
    ROTATE_90 = 'rotate_90'

    ALL = [FLIP_HORIZONTALLY, FLIP_VERTICALLY, ROTATE_90]
