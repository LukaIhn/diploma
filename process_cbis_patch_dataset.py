import click
import os

import torch

import src.constants.names as names

from src.constants.miscellaneous import DATA, PATCH_DDSM_CLEANED, PROCESSED
from src.journaling.logs import initialize_logger
from src.models.workers import DataSetModifier
from src.pathes.path_resolver import DummyHashResolver

PATH_TO_PROJECT = os.path.join(os.path.dirname(__file__))


@click.command()
@click.option('--use-cuda', type=bool, default=False)
@click.option('--experiment-name', default='model_analysis', type=str, help='Path of the experiment in the models directory.')
@click.option('--data-directory', default=os.path.join(PATH_TO_PROJECT, DATA), type=str,
              help='Location of the directory with the data.')
@click.option('--new_data_directory', default=os.path.join(PATH_TO_PROJECT, DATA, PROCESSED, PATCH_DDSM_CLEANED + '_resized'), type=str,
              help='Location of the directory with the data.')
@click.option('--gc-aggressive', default=False, type=bool, help='Chooses whether to collect garbage aggressively.')
@click.option('--scale', type=float, default=0.5, help='Scale of change in image shape.')
def main(**kwargs):
    experiment_directory = os.path.join(PATH_TO_PROJECT, names.MODELS, kwargs['experiment_name'])
    initialize_logger(os.path.join(experiment_directory, 'experiment.log'))

    param = {
        names.DEVICE: torch.device('cpu'),
        names.RUN_PATH_RESOLVER: DummyHashResolver(**kwargs),
        names.DATA_DIRECTORY: kwargs[names.DATA_DIRECTORY],
        names.IMAGE_SHAPE: (3, 900, 900),
        DATA:{'seed':42, names.NUMBER_OF_FOLDS:2, 'batch_size':100, 'evaluation_batch_size':100, 'balance_classes': False, names.CHOSEN_FOLD: 0},
        names.TRAIN_PROCESSORS:[],
        names.VALIDATION_PROCESSORS:[],
        names.TEST_PROCESSORS: [],
        'log_patterns': True,
        'num_workers': 1,
        'scale': kwargs['scale']
    }
    worker = DataSetModifier(**param)
    worker.load_dependencies()
    worker.start()

if __name__ == '__main__':
    main()
