from tensorboardX import SummaryWriter
import numpy as np
import os

from src.utils import check_dir


class TensorboardLogger():
    def __init__(self, experiment_dir_path: str):
        assert 'models' in experiment_dir_path, 'Experiments should stored be in directory `models`'
        assert check_dir(experiment_dir_path, create=False), f'Dirctory {experiment_dir_path} does not exist'

        self._writer = SummaryWriter(logdir=experiment_dir_path)
        self._experiment_name = os.path.basename(experiment_dir_path)

        # TODO: write model graph and distributions of train/validation/test sets

    def log_float(self, name: str, value: float, abscissa: int):
        self._writer.add_scalar(tag=name, scalar_value=value, global_step=abscissa)

    def log_named_dictionary(self, dictionary, abscissa):
        for key, value in dictionary.items():
            self.log_float(name=key, value=value, abscissa=abscissa)

    def log_model(self, name: str, model, abscissa: int):
        for layer, weights in model.state_dict().items():
            histogram_name = '/'.join([name, 'weights', layer])
            self.log_ndarray_as_histogram(name=histogram_name, data=weights, abscissa=abscissa)

    def log_ndarray_as_histogram(self, name: str, data: np.ndarray, abscissa: int):
        self._writer.add_histogram(tag=name, values=data, global_step=abscissa, bins='doane')

    def log_image(self, name: str, image, abscissa: int):
        self._writer.add_image(name, image, global_step=abscissa, walltime=None, dataformats='CHW')

    def close(self):
        self._writer.close()
