import pydicom

import png

class DicomHelper:
    # def __init__(self, image_path):
    #     self._image_path = image_path
    #     self.dicom_image

    @staticmethod
    def read_dicom(path_to_file):
        dicom = pydicom.dcmread(path_to_file)
        dicom.pixel_array.astype(float)
        return dicom.pixel_array.astype(float)

    @staticmethod
    def process_dicom(path_to_file):
        dicom = pydicom.dcmread(path_to_file)
        dicom.pixel_array.astype(float)
        return dicom.pixel_array.astype(float)