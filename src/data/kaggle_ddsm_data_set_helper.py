import numpy as np
import tensorflow as tf
from tqdm import tqdm
import os

from src.constants.miscellaneous import PATCH_DDSM_RAW, PATCH_DDSM_PROCESSED, PATCH_DDSM_INTERIM, \
    PATCH_DDSM_TRAIN_SIZE, PATCH_DDSM_ARCHIVE_NAME, PATCH_DDSM_KAGGLE
from src.data.kaggle_helper import KaggleDataLoader
from src.utils import check_dir, extract_nested_zip


class KagglePatchDatasetCreator():
    def __init__(self, data_path):
        self._data_path = data_path
        self._assert_directories_exist()

    def _assert_directories_exist(self):
        directories = [
            PATCH_DDSM_RAW,
            PATCH_DDSM_INTERIM,
            PATCH_DDSM_PROCESSED,
        ]

        for dir in directories:
            check_dir(self.get_path(dir), create=True)

    def get_path(self, relative_path: str, file_name: str = ''):
        return os.path.join(self._data_path, relative_path, file_name)

    def _list_to_numpy_arrays(self, data):
        image_shape = data[0][2].shape
        number_of_images = len(data)

        # check to be consistent with format (N, 299, 299, 1)
        assert len(image_shape) == 4

        assert image_shape[1] == 299
        assert image_shape[2] == 299
        assert image_shape[3] == 1

        labels = np.ndarray(shape=(number_of_images), dtype='uint8')
        full_labels = np.ndarray(shape=(number_of_images), dtype='uint8')
        images = np.ndarray(shape=(number_of_images, image_shape[1], image_shape[2], image_shape[3]), dtype='uint8')

        for i in tqdm(range(number_of_images)):
            label, full_label, image = data[i]
            images[i, :] = image
            labels[i] = label
            full_labels[i] = full_label

        return labels, full_labels, images

    @staticmethod
    def _read_and_decode_single_example(file_names):
        filename_queue = tf.train.string_input_producer(file_names, num_epochs=1)

        reader = tf.TFRecordReader()

        _, serialized_example = reader.read(filename_queue)
        features = tf.parse_single_example(
            serialized_example,
            features={
                'label': tf.FixedLenFeature([], tf.int64),
                'label_normal': tf.FixedLenFeature([], tf.int64),
                'image': tf.FixedLenFeature([], tf.string)
            })

        # now return the converted data
        label = features['label_normal']
        full_label = features['label']
        image = tf.decode_raw(features['image'], tf.uint8)
        image = tf.reshape(image, [1, 299, 299, 1])

        return full_label, label, image

    def tfrecord_to_list(self, file_names, number_of_elements):
        list_of_patterns = []
        # create readers
        full_label, label, image = self._read_and_decode_single_example(file_names)
        images_batch, labels_batch, full_label_batch = tf.train.batch([image, label, full_label], batch_size=1,
                                                                      capacity=1)

        i = 0
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())

            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(coord=coord)

            for i in tqdm(range(number_of_elements)):
                la_b, full_l_b, im_b = sess.run([labels_batch, full_label_batch, images_batch])
                list_of_patterns.append((la_b[0], full_l_b[0], im_b[0]))

            coord.request_stop()

            # Wait for threads to stop
            coord.join(threads)

        return list_of_patterns

    def get_raw_file_path_ending_with(self, ends_with: str = ''):
        all_files = []
        for root, dirs, files in os.walk(self.get_path(PATCH_DDSM_RAW)):
            for f in files:
                file_path = os.path.join(root, f)
                if os.path.isfile(file_path) and ('.zip' not in file_path) and f.endswith(ends_with):
                    all_files.append(file_path)

        return all_files

    def read_and_save_tf_records(self):
        # read and save train data
        tfrecord_raw_files = sorted(self.get_raw_file_path_ending_with('.tfrecords'))
        labels, full_labels, images = self._list_to_numpy_arrays(
            self.tfrecord_to_list(tfrecord_raw_files, PATCH_DDSM_TRAIN_SIZE))

        np.save(self.get_path(PATCH_DDSM_PROCESSED, 'train_data.npy'), images)
        np.save(self.get_path(PATCH_DDSM_PROCESSED, 'train_labels.npy'), full_labels)
        np.save(self.get_path(PATCH_DDSM_PROCESSED, 'train_labels_short.npy'), labels)

        # free the ram
        labels, full_labels, images = [None] * 3

        # read and save test data
        #
        # it is already in the '.npy' format
        # test set was incorrectly divided in two groups that have uneven counts of
        # patterns from different classes
        npy_raw_data_files = sorted(self.get_raw_file_path_ending_with('data.npy'))
        npy_raw_labels_files = sorted(self.get_raw_file_path_ending_with('labels.npy'))

        test_data = np.concatenate([np.load(f) for f in npy_raw_data_files])
        test_labels = np.concatenate([np.load(f) for f in npy_raw_labels_files])

        np.save(self.get_path(PATCH_DDSM_PROCESSED, 'test_data.npy'), test_data)
        np.save(self.get_path(PATCH_DDSM_PROCESSED, 'test_labels.npy'), test_labels)

    def extract_data(self):
        archive_path = self.get_path(PATCH_DDSM_RAW, PATCH_DDSM_ARCHIVE_NAME)
        destination_dir = self.get_path(PATCH_DDSM_RAW)
        extract_nested_zip(file_path=archive_path, destination_path = destination_dir)

    def download_data(self):
        loader = KaggleDataLoader(data_set_name=PATCH_DDSM_KAGGLE,
                                  destination_dir=os.path.join(self._data_path, PATCH_DDSM_RAW))
        loader.download_data()
