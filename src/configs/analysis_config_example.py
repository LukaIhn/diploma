import src.constants.names as names
import src.constants.miscellaneous as misc

RUN_PARAMETERS = {
    names.GET_DATA_HASH: False,
    'log_patterns': True,
    names.NOISE_MEAN: 0,
    names.NOISE_STD: [0.1, 0.05, 0.01, 0.005, 0.001],
    names.NUMBER_OF_REPEATS: 10,

    names.LOADERS: [misc.TRAIN, misc.VALIDATION, misc.TEST],

    'new_batch_size': 100,

    names.TestTimeAugmentations.KEY_IN_PARAMETERS_FILE: {
        names.TestTimeAugmentations.FLIP_HORIZONTALLY: [True, False],
        names.TestTimeAugmentations.FLIP_VERTICALLY: [True, False],
        names.TestTimeAugmentations.ROTATE_90: [True, False]
    },

    misc.DATA: {
        names.MODEL_NAME: '0.checkpoint',
    },
}

grid_search = {
}
