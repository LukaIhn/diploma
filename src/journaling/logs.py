import os
from collections import defaultdict

from torch.utils.data import DataLoader
from tqdm import tqdm

from src.constants.miscellaneous import TRAILING_HASHTAG
from src.utils import check_dir
import logging
import numpy as np


def log_named_dictionary(logger, name, dictionary):
    result = '\n{}\n{}\n{}\n'.format(TRAILING_HASHTAG, name, TRAILING_HASHTAG)

    for key, value in dictionary.items():
        result += '\n{}: {}'.format(str(key), str(value))

    logger.info(result)


def log_config(logger: logging.Logger, config: dict):
    logger.info('\n{}\nTrain run with parameters:\n{}\n'.format(TRAILING_HASHTAG, TRAILING_HASHTAG))
    result = ''
    result += '\n{}\nDATA\n'.format(TRAILING_HASHTAG)
    for key, value in config['data'].items():
        result += '\n{}: {}'.format(str(key), str(value))

    result += '\n{}\nTRAINING PARAMETERS\n'.format(TRAILING_HASHTAG)
    for key, value in config.items():
        if key == 'data':
            continue
        result += '\n{}: {}'.format(str(key), str(value))

    logger.info(result)


def log_model(logger: logging.Logger, model):
    logger.info('\n#############################\nModel:\n#############################\n')
    trainable_parameters_number = sum([p.numel() for p in model.parameters() if p.requires_grad])
    logger.info('\nNumber of trainable parameters: {}\n'.format(trainable_parameters_number))
    logger.info(str(model))


def log_data_set(logger: logging.Logger, loader: DataLoader):
    logger.info('\n#############################\n{} DATA LOADER:\n#############################\n'.format(loader.name))
    class_counts = defaultdict(int)

    # get label unique values and their counts
    for i, batch in enumerate(tqdm(loader)):
        labels = batch['label']
        unique_values, unique_counts = np.unique(labels, return_counts=True)
        for i in range(len(unique_values)):
            class_counts[unique_values[i]] += unique_counts[i]

    logger.info('Data  loader batch size is:\t{}'.format(loader.batch_size))
    for key, value in class_counts.items():
        logger.info('There is `{}` elements with label:\t{}'.format(value, key))


def initialize_logger(log_file_path: str = None, level=logging.DEBUG):
    check_dir(os.path.dirname(log_file_path), create=True)
    logger = logging.getLogger('main')
    logger.setLevel(level)

    if os.path.isfile(log_file_path):
        assert log_file_path.endswith('.log')
        os.remove(log_file_path)

    logger_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    # Create the Handler for logging data to a file
    file_handler = logging.FileHandler(log_file_path)
    file_handler.setLevel(logging.DEBUG)
    # Add the Formatter to the Handler
    file_handler.setFormatter(logger_formatter)
    logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(level)
    logger_formatter_2 = logging.Formatter('%(message)s')
    console_handler.setFormatter(logger_formatter_2)
    logger.addHandler(console_handler)

    logger.info('Logger initialized.')
