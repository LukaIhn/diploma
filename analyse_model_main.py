import click
import os
from src.constants import names

from src.constants.miscellaneous import DATA, DEFAULT_ANALYSIS_CONFIG_FILE_NAME
from src.experiment_runner import ExperimentRunner
from src.journaling.logs import initialize_logger
from src.models.workers import MNISTModelAnalyserWorker, PatchDDSMModelAnalyserWorker
from src.pathes.path_resolver import NoHashExperimentHashResolver
from src.utils import load_experiment_config, update_dictionary


PATH_TO_PROJECT = os.path.join(os.path.dirname(__file__))


class MNISTModelAnalysisRunner(ExperimentRunner):
    worker_class = MNISTModelAnalyserWorker
    path_resolver_class = NoHashExperimentHashResolver

class DDSMModelAnalysisRunner(ExperimentRunner):
    worker_class = PatchDDSMModelAnalyserWorker
    path_resolver_class = NoHashExperimentHashResolver


@click.command()
@click.option('--contact-me', type=str, default=None, help='This is visible in htop so that others would know who ran the process.')
@click.option('--use-cuda', type=bool, default=False)
@click.option('--experiment-name', default='R', type=str, help='Path of the experiment in the models directory.')
@click.option('--data-directory', default=os.path.join(PATH_TO_PROJECT, DATA), type=str,
              help='Location of the directory with the data.')
@click.option('--gc-aggressive', default=False, type=bool, help='Chooses whether to collect garbage aggressively.')
@click.option('--task', type=str, default='ddsm', help='mnist or ddsm')
def main(**kwargs):
    experiment_directory = os.path.join(PATH_TO_PROJECT, names.MODELS, kwargs['experiment_name'])
    initialize_logger(os.path.join(experiment_directory, 'experiment.log'))

    config = load_experiment_config(kwargs['experiment_name'], DEFAULT_ANALYSIS_CONFIG_FILE_NAME)
    update_dictionary(source_dict=kwargs, target_dict=config)

    if kwargs['task'] == 'mnist':
        runner = MNISTModelAnalysisRunner(**config)
    else:
        runner = DDSMModelAnalysisRunner(**config)

    runner.run_grid_search()

if __name__ == '__main__':
    # try:
    main()
    # except Exception as e:
    #     logger.error('Running script{} ended with exception {}'.format(__file__, e))
