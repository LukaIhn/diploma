import click
import os

import torch

import src.constants.names as names

from src.constants.miscellaneous import DATA, MNIST_IMAGE_SIZE
from src.experiment_runner import ExperimentRunner
from src.journaling.logs import initialize_logger
from src.models.workers import MNISTTrainerWorker
from src.utils import load_experiment_config, update_dictionary

PATH_TO_PROJECT = os.path.join(os.path.dirname(__file__))


class MNISTTrainRunner(ExperimentRunner):
    worker_class = MNISTTrainerWorker


@click.command()
@click.option('--use-cuda', type=bool, default=False)
@click.option('--experiment-name', default='mnist_simple', type=str, help='Path of the experiment in the models directory.')
@click.option('--dtype', default='float', type=str, help='Which dtype to use.')
@click.option('--data-directory', default=os.path.join(PATH_TO_PROJECT, DATA), type=str,
              help='Location of the directory with the data.')
@click.option('--gc-aggressive', default=False, type=bool, help='Chooses whether to collect garbage aggressively.')
@click.option('--reproducible', default=False, type=bool, help='Whether to allow nondeterminism.')
def main(**kwargs):
    if kwargs['reproducible']:
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False

    experiment_directory = os.path.join(PATH_TO_PROJECT, names.MODELS, kwargs['experiment_name'])
    initialize_logger(os.path.join(experiment_directory, 'experiment.log'))

    config = load_experiment_config(kwargs['experiment_name'])
    update_dictionary(source_dict=kwargs, target_dict=config)
    config[names.RUN_PARAMETERS]['number_of_classes'] = 10
    config[names.RUN_PARAMETERS]['image_size_flatten'] = MNIST_IMAGE_SIZE * MNIST_IMAGE_SIZE
    config[names.RUN_PARAMETERS][names.IMAGE_SHAPE] = (3, MNIST_IMAGE_SIZE, MNIST_IMAGE_SIZE)

    # removing data processors for mnist
    for processor in [names.TRAIN_PROCESSORS, names.VALIDATION_PROCESSORS, names.TEST_PROCESSORS]:
        try:
            processors_num_to_skip = config[names.RUN_PARAMETERS].get('skip_processors_in_mnist', 1)
            config[names.RUN_PARAMETERS][processor] = config[names.RUN_PARAMETERS][processor][processors_num_to_skip:]
            for i, processing_option in enumerate(config['grid_search'][processor]):
                config['grid_search'][processor][i] = processing_option[processors_num_to_skip:]

        except KeyError:
            pass

    runner = MNISTTrainRunner(**config)

    runner.run_grid_search()


if __name__ == '__main__':
    # try:
    main()
    # except Exception as e:
    #     logger.error('Running script{} ended with exception {}'.format(__file__, e))
