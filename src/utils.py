import importlib
import os
import io
import random
import time
import zipfile
import logging

import gc
from typing import List, Tuple, Any

import numpy as np
import pandas as pd
import torch
from torch.nn import Parameter

from src.constants.miscellaneous import CONFIG_ALLOWED_TYPES, DEFAULT_TRAINING_CONFIG_FILE_NAME, \
    DICTIONARY_NAME_SEPARATOR
from ctypes import cdll, CDLL

PATH_TO_PROJECT = os.path.join(os.path.dirname(__file__), '..')

try:
    cdll.LoadLibrary("libc.so.6")
    libc = CDLL("libc.so.6")
    libc.malloc_trim(0)
except (OSError, AttributeError) as e:
    libc = None

GARBAGE_COLLECTION_IS_ON = False


def switch_garbage_collection_mode(aggressive: bool = False):
    GARBAGE_COLLECTION_IS_ON = aggressive


def collect_garbage_decorator(function):
    """
    Enforces garbage collection. Can lead to performance issues when used too often.
    https://sourceware.org/bugzilla/show_bug.cgi?id=14827, https://github.com/pandas-dev/pandas/issues/2659
    """

    def wrapper(*args, **kwargs):
        if GARBAGE_COLLECTION_IS_ON:  # TODO: fix this
            libc.malloc_trim(0)
            gc.collect()
        return function(*args, **kwargs)

    return wrapper


def check_dir(dir: str, create: bool = False, recursively=False):
    result = os.path.isdir(dir)
    if result:
        return True

    if create:
        if os.path.exists(dir):
            raise ValueError('Path already exists and it is not a directory.')
        else:
            os.makedirs(dir)
            result = True

    return result


def extract_nested_zip(file_path, destination_path=None):
    if destination_path is None:
        destination_path = os.path.join(os.path.dirname(file_path), 'extracted')
    z = zipfile.ZipFile(file_path)

    for f in z.namelist():
        dirname = os.path.join(destination_path, os.path.splitext(f)[0])

        if f.endswith('.zip'):
            # extract nested zip
            check_dir(dirname)

            content = io.BytesIO(z.read(f))
            zip_file = zipfile.ZipFile(content)
            for i in zip_file.namelist():
                zip_file.extract(i, dirname)
        else:
            z.extract(f, dirname)


def load_experiment_config(experiment_name: str, default_config_name=DEFAULT_TRAINING_CONFIG_FILE_NAME):
    exp_config_name = '.'.join(['models', experiment_name, 'config'])

    result = {}

    try:
        experiment_config = importlib.import_module(exp_config_name)
    except ModuleNotFoundError:
        logging.warning(
            'Config should be created before running the experiment. Default is used. Name: {}'.format(experiment_name))
        exp_config_name = '.'.join(['src', 'configs', default_config_name])
        experiment_config = importlib.import_module(exp_config_name)

    for item in dir(experiment_config):
        attribute_value = getattr(experiment_config, item)
        if (type(attribute_value) in CONFIG_ALLOWED_TYPES) and (not item.startswith('__')):
            result[item.lower()] = getattr(experiment_config, item)

    return result


def update_dictionary(source_dict: dict, target_dict: dict):
    for key, value in source_dict.items():
        target_dict[key] = value


def log_spent_time_decorator(function):
    logger = logging.getLogger('main')

    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = function(*args, **kwargs)
        total_time = time.time() - start_time
        logger.info('Running function {} took {} seconds'.format(function.__name__, total_time))

        return result

    return wrapper


def plot_to_image(figure):
    figure.canvas.draw()
    return np.array(figure.canvas.renderer.buffer_rgba())


def update_nested_dictionary(dictionary, keys, value):
    """
    Function used to update perameters while a grid search
    :param dictionary: dictionary with nested sictionaries
    :param keys: keys that can be sequentially applied to the dictionary
    :param value: new value
    :return:
    """
    var_dictionary = dictionary
    for i, key in enumerate(keys, start=1):
        if i < len(keys):
            # open nested dictionary
            var_dictionary = var_dictionary[key]
        else:
            # update the value
            assert var_dictionary.get(key, None) is not dict
            var_dictionary[key] = value


def update_model_training_config(config: dict, new_values: List[Tuple[str, Any]]):
    for key, value in new_values:
        # dealing with data set parameters
        if DICTIONARY_NAME_SEPARATOR in key:
            keys = key.split(DICTIONARY_NAME_SEPARATOR)
            update_nested_dictionary(config, keys, value)
        else:
            config[key] = value


def read_hdf(file_path: str, key='data') -> pd.DataFrame:
    return pd.read_hdf(file_path, key)


def save_data_frame_as_hdf(df: pd.DataFrame, path: str, key='data') -> None:
    check_dir(os.path.dirname(path), create=True)
    df.to_hdf(path, key)


def set_torch_seed(seed: int):
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    random.seed(seed)
    np.random.seed(seed)  # max = 32 bit unsigned integers



def print_usage(msg = ''):
    """
    For debugging memory issues
    :param msg:
    :return:
    """
    print(msg+': \n')
    tensors = 0
    params_ = 0
    others_ = 0

    gpu_tensors = 0
    gpu_params_ = 0
    gpu_others_ = 0

    for obj in gc.get_objects():
        try:
            if type(obj) is Parameter:
                if obj.device.type == 'cpu':
                    params_+=1
                else:
                    gpu_params_+=1

            if torch.is_tensor(obj):
                if obj.device.type == 'cpu':
                    tensors+=1
                else:
                    gpu_tensors+=1

            if not torch.is_tensor(obj) and (hasattr(obj, 'data') and torch.is_tensor(obj.data)):
                # print(type(obj), obj.size())
                if obj.device.type == 'cpu':
                    others_+=1
                else:
                    gpu_others_+=1
        except:
            pass
    print(f'CPU      TENSORS: {tensors}, PARAMS:{params_}, OTHERS:{others_}')
    print(f'GPU      TENSORS: {gpu_tensors}, PARAMS:{gpu_params_}, OTHERS:{gpu_others_}')

