from src.constants.miscellaneous import PATCH_DDSM_RAW, PATCH_DDSM_ARCHIVE_NAME, PATCH_DDSM_KAGGLE
from src.data.kaggle_helper import KaggleDataLoader
from src.data.kaggle_ddsm_data_set_helper import KagglePatchDatasetCreator
import os
import click

PATH_TO_PROJECT = os.path.join(os.path.dirname(__file__))

@click.command()
@click.option('--download-data', type=bool, default=False)
def main(**kwargs):
    data_dir = kwargs.get('data_dir', os.path.join(PATH_TO_PROJECT, 'data'))

    helper = KagglePatchDatasetCreator(data_dir)

    if kwargs.get('download_data', False):
        helper.download_data()

    helper.extract_data()
    helper.read_and_save_tf_records()


if __name__ == '__main__':
    main()
