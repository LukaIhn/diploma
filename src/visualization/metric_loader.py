import json
import os
from typing import List
import pandas as pd

from src.constants.miscellaneous import CONFIG_FILE_NAME
from src.constants.names import METRIC_FILE_ENDING


class IndividualRun:
    _config = None

    def __init__(self, run_dir: str, drop_metrics=None):
        self.experiment_dir = os.path.join(*run_dir.split('/')[:-2])
        self.run_name = run_dir.split('/')[0]
        self.run_dir = run_dir
        if drop_metrics:
            self.drop_metrics = drop_metrics
        else:
            self.drop_metrics = 'test_entropy_loss_multitask_metric_.metric'

    @property
    def config(self) -> dict:
        if self._config is None:
            self._config = json.load(open(os.path.join(self.run_dir, CONFIG_FILE_NAME), 'rb'))

        return self._config

    def get_all_metrics(self) -> list:
        metrics_list = []
        for f in os.listdir(self.run_dir):
            if f.endswith(METRIC_FILE_ENDING) and f not in self.drop_metrics:
                metrics_list.append(f)

        return metrics_list

    def __repr__(self):
        return str(type(self)) + self.run_dir

    def load_metric(self, metric_name, best_or_last: str = 'best') -> float:
        df = self.load_metric_learning_curve(metric_name, drop_best=False)

        if best_or_last == 'best':
            return df.iloc[-1].values[1]
        else:
            return df.iloc[-2].values[1]

    def load_metric_learning_curve(self, metric_name, drop_best: bool = True):
        """

        :param metric_name:
        :param drop_best: last row in metrics belongs to the best model metric value, this parameter defines whether it should be dropped
        :return:
        """
        return pd.read_csv(os.path.join(self.run_dir, metric_name))[:-1]


class Experiment:
    def __init__(self, experiment_dir: str):
        self.experiment_dir = experiment_dir

    def get_all_runs(self) -> List[IndividualRun]:
        runs = []
        for root, subFolders, files in os.walk(self.experiment_dir):
            if any(f == CONFIG_FILE_NAME for f in files):
                runs.append(IndividualRun(root))

        return runs

    def get_all_metrics(self, best_or_last: str = 'best'):
        metric_names = self.get_all_runs()[0].get_all_metrics()
        all_values = []
        run_dirs = []

        for run in self.get_all_runs():
            run_metric_values = []
            for m in metric_names:
                run_metric_values.append(run.load_metric(m, best_or_last))

            run_dirs.append(run.run_dir)
            all_values.append(run_metric_values)

        result = pd.DataFrame(all_values, columns=metric_names)
        result['run_dir'] = run_dirs
        result['exp_dir'] = self.experiment_dir
        result['experiment name'] = self.experiment_dir.split(os.sep)[-1]
        result['best_or_last'] = best_or_last

        return result

    def get_learning_curves(self, metric_name: str):
        assert all(metric_name in r.get_all_metrics() for r in self.get_all_runs())

        learning_curves = dict()

        for run in self.get_all_runs():
            learning_curves[run.run_dir] = run.load_metric_learning_curve(metric_name)

        return learning_curves
