import gc
import itertools
import json
import logging
import os
import time
from abc import ABC, abstractmethod
from collections import defaultdict
from typing import List, Optional

import cv2
import dill as dill
import torch
import numpy as np
import pandas as pd
import torchvision

from class_registry import ClassRegistry
from torch.nn import Parameter
from tqdm import tqdm

import src.constants.names as names
from src.constants.miscellaneous import DATA, MODEL, PARAMS, TRAIN, VALIDATION, TEST, CONFIG_FILE_NAME, IMAGE, LABEL, \
    TRAILING_HASHTAG, IMAGE_DATA_FILE_NAME, LABELS_FILE_NAME, PATCH_DDSM_PROCESSED, CHECKPOINT_ENDING, PARAM_SEPARATOR, \
    ID, PATCH_DDSM_CLEANED_PROCESSED
from src.data.CBIS.cbis_directory_structure_helper import PNGMetafeatureColumns, PatchDatasetFileNames
from src.data.stratified_group_k_fold import stratified_group_k_fold
from src.data.torch_patch_ddsm_data_handler import DataHandler
from src.journaling.logs import log_config, log_model, log_data_set
from src.journaling.tensorboard_logger import TensorboardLogger
from src.models.metrics import MetricAbstract, metrics_registry
from src.models.patch_ddsm_models import model_registry, BaseModelAbstract
from src.models.torch_optimizers import get_optimizer
from src.pathes.path_resolver import RunPathResolver
from src.utils import log_spent_time_decorator, check_dir, collect_garbage_decorator, read_hdf, save_data_frame_as_hdf, \
    set_torch_seed, update_model_training_config

_PROJECT_PATH = os.path.join(os.path.dirname(__file__), '..', '..')
assert os.path.join('src', 'models') in _PROJECT_PATH, 'Directory structure has changed! Can`t load {}'.format(__file__)

worker_registry = ClassRegistry('worker_name')


class TorchModelLoader:
    @staticmethod
    def load(file_path, device: torch.device):
        assert os.path.isfile(file_path)

        loaded_data = torch.load(file_path, pickle_module=dill, map_location=torch.device('cpu'))

        params = loaded_data[PARAMS]
        model = model_registry.get(params[MODEL], params)
        state_dict = loaded_data['model_state_dict']

        # Hack for loading EfficientNet
        state_dict = {key.replace('classifier.0', 'classifier.1'): value for key, value in state_dict.items()}

        model.load_state_dict(state_dict)
        model.to(device)
        return model, params


class DataProvider(ABC):
    def __init__(self, data_directory, **kwargs):
        # super().__init__(**kwargs)

        self.train_metafeatures, self.test_metafeatures = None, None
        self.data_directory = data_directory

    @abstractmethod
    def load_datasets(self, params: dict = None):
        pass

    def load_metafeatures(self):
        return None, None

    def get_data_handler(self, data_directory: str, params: dict, initialize_loaders: bool = True):
        self.train_metafeatures, self.test_metafeatures = self.load_metafeatures()
        train_data, train_labels, test_data, test_labels = self.load_datasets(params)
        data_handler = DataHandler(data_directory=data_directory,
                                   params=params,
                                   train_data=train_data,
                                   train_labels=train_labels,
                                   test_data=test_data,
                                   test_labels=test_labels,
                                   provided_fold_id_initializer=self.initialize_fold_id_dict,
                                   train_metafeatures=self.train_metafeatures,
                                   test_metafeatures=self.test_metafeatures)

        if initialize_loaders:
            data_handler.initialize_loaders(fold_number=params[DATA][names.CHOSEN_FOLD])

        return data_handler

    @staticmethod
    def initialize_fold_id_dict(**kwargs):
        """
        This function will be called inside DataHandler.

        For each unique label separate data into k-folds.
        Creates dictionary with unique label values as keys and k arrays(folds) of idxs of the patterns labeled
        the same as their key value
        :return:
        """
        data_handler: DataHandler = kwargs['data_handler']

        number_of_train_patterns = len(data_handler.train_labels)
        idxs = np.arange(start=0, stop=number_of_train_patterns, dtype=np.uint32)

        data_handler.label_fold_dictionary = {}

        for label in np.unique(data_handler.train_labels):
            class_mask = data_handler.train_labels == label
            class_idxs = idxs[class_mask]

            # reset random seed, shuffle the ids
            data_handler._reset_data_seed()
            np.random.shuffle(class_idxs)

            # divides into almost even folds
            folds = np.array_split(class_idxs, data_handler._number_of_folds)
            data_handler.label_fold_dictionary[label] = folds

        if data_handler._balance_classes:
            minimal_number_of_patterns = None
            for label in np.unique(data_handler.train_labels):
                number = len(data_handler.label_fold_dictionary[label][0])
                if minimal_number_of_patterns is None or minimal_number_of_patterns > number:
                    minimal_number_of_patterns = number
            data_handler.logger.info(
                'Ballancing the dataset to have {} patterns in each fold'.format(minimal_number_of_patterns))

            new_dictionary = {}
            for label, folds in data_handler.label_fold_dictionary.items():
                new_array = []
                for fold in folds:
                    new_array.append(fold[:minimal_number_of_patterns, ...])
                new_dictionary[label] = new_array
            data_handler.label_fold_dictionary = new_dictionary

            data_handler.logger.info('Ballancing done.'.format(minimal_number_of_patterns))


class DDSMLoader(DataProvider):

    def get_npy(self, file_name: str):
        return np.load(os.path.join(self.data_directory, PATCH_DDSM_PROCESSED, file_name))

    def load_datasets(self, config: dict = None):
        train_data = self.get_npy(IMAGE_DATA_FILE_NAME.format(TRAIN))  # FIXME: change TEST to TRAIN
        train_labels = self.get_npy(LABELS_FILE_NAME.format(TRAIN))

        test_data = self.get_npy(IMAGE_DATA_FILE_NAME.format(TEST))
        test_labels = self.get_npy(LABELS_FILE_NAME.format(TEST))

        return train_data, train_labels, test_data, test_labels


class DDSMCleanedLoader(DataProvider):
    def __init__(self, task_type='both', **kwargs):
        super().__init__(**kwargs)

        self.task_type = task_type

        if 'data_set_directory_name' in kwargs[DATA] and kwargs[DATA]['data_set_directory_name'] is not None:
            self.data_set_directory_name = kwargs[DATA]['data_set_directory_name']
        else:
            self.data_set_directory_name = PATCH_DDSM_CLEANED_PROCESSED

    def load_datasets(self, config: dict = None):
        if config is None:
            config = self.params

        assert config[names.IMAGE_SHAPE][0] == 3
        patch_size = config[names.IMAGE_SHAPE][-1]

        train_data = self.get_npy(PatchDatasetFileNames.data_for('train'),
                                  shape=(len(self.train_metafeatures), patch_size, patch_size))
        train_labels = self.get_npy(PatchDatasetFileNames.label_for_set('train'), shape=(len(self.train_metafeatures)))

        test_data = self.get_npy(PatchDatasetFileNames.data_for('test'),
                                 shape=(len(self.test_metafeatures), patch_size, patch_size))
        test_labels = self.get_npy(PatchDatasetFileNames.label_for_set('test'), shape=(len(self.test_metafeatures)))

        return train_data, train_labels, test_data, test_labels

    def get_npy(self, file_name: str, shape: tuple, dtype: str = 'uint8'):
        return np.memmap(os.path.join(self.data_directory, self.data_set_directory_name, file_name), dtype=dtype,
                         mode='r', shape=shape)

    def get_metafeature_path(self, name):
        return os.path.join(self.data_directory, self.data_set_directory_name, PatchDatasetFileNames.metadata_for(name))

    def load_metafeatures(self):
        train_metafeatures = read_hdf(self.get_metafeature_path('train'))
        test_metafeatures = read_hdf(self.get_metafeature_path('test'))

        return train_metafeatures, test_metafeatures

    @staticmethod
    def initialize_fold_id_dict(**kwargs) -> None:
        """
        This function will be called inside DataHandler.

        For each unique label separate data into k-folds.
        Creates dictionary with unique label values as keys and k arrays(folds) of idxs of the patterns labeled
        the same as their key value
        :return:
        """
        data_handler: DataHandler = kwargs['data_handler']
        train_mf = data_handler.train_metafeatures
        number_of_folds = data_handler._number_of_folds

        ids = np.arange(len(train_mf))
        labels = train_mf['new_labels'].values

        assert len(ids) == len(data_handler.train_data)  # TODO: move to better place

        groups = np.array(train_mf[PNGMetafeatureColumns.PATIENT_ID].values)

        data_handler.label_fold_dictionary = defaultdict(list)
        for fold_ind, (_, val_ind) in \
                enumerate(stratified_group_k_fold(ids, labels, groups, k=number_of_folds, seed=data_handler.np_seed)):
            for label in np.unique(data_handler.train_labels):
                fold_class_idxs = val_ind[labels[val_ind] == label]
                data_handler.label_fold_dictionary[label].append(fold_class_idxs)


class MNISTLoader(DataProvider):
    def load_datasets(self, config: dict = None):
        train_loader = torch.utils.data.DataLoader(
            torchvision.datasets.MNIST(self.data_directory, train=True, download=True,
                                       transform=torchvision.transforms.Compose(
                                           [torchvision.transforms.ToTensor()])),
            batch_size=100, shuffle=False)

        test_loader = torch.utils.data.DataLoader(
            torchvision.datasets.MNIST(self.data_directory, train=False, download=True,
                                       transform=torchvision.transforms.Compose(
                                           [torchvision.transforms.ToTensor()])),
            batch_size=100, shuffle=False)

        train_data, train_labels = [], []
        for image, labels in tqdm(train_loader):
            train_data.extend(image.numpy())
            train_labels.extend(labels.numpy())
        train_data, train_labels = np.array(train_data), np.array(train_labels)

        test_data, test_labels = [], []
        for image, labels in tqdm(test_loader):
            test_data.extend(image.numpy())
            test_labels.extend(labels.numpy())
        test_data, test_labels = np.array(test_data), np.array(test_labels)

        return train_data, train_labels, test_data, test_labels


class BaseWorker(ABC):
    worker_name = None

    def __init__(self,
                 device: torch.device,
                 run_path_resolver: RunPathResolver,
                 **kwargs):
        super().__init__(**kwargs)

        self.logger = logging.getLogger('main')

        self.params = kwargs

        self.device = device

        self.run_path_resolver = run_path_resolver
        self.run_path = run_path_resolver.get_run_path()

    @abstractmethod
    def load_dependencies(self) -> None:
        pass

    @abstractmethod
    def start(self):
        pass


class SimpleModelAnalyser(BaseWorker, DataProvider, ABC):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.model_name = kwargs[names.MODEL_NAME]
        self.model: BaseModelAbstract = None  # it will be loaded in load_dependencies
        self.loaders_to_analyse: List[str] = kwargs[names.LOADERS]

        assert not self.params[names.NOISE_STD] or (
                np.array(self.params[names.NOISE_STD]) < 1).all(), 'STDS bigger than 1 are not meaningfull.'

    def load_dependencies(self) -> None:
        """
        Model, optimizer, metrics, data handler, callbacks
        :return:
        """
        assert check_dir(self.run_path), f'Run path should exist and contain model to analyse. Path:{self.run_path}'
        model_path = os.path.join(self.run_path_resolver.experiment_path, self.model_name)
        assert os.path.isfile(
            model_path), f'Run path should exist and contain model to analyse. Path:{self.run_path}'

        params = self.params

        self.model, self.model_params = TorchModelLoader.load(model_path, self.device)
        self.data_set_directory_name = self.model_params[DATA]['data_set_directory_name']  # hack, should be rewritten

        update_model_training_config(self.model_params, self.model_params[names.GRIDSEARCH_UPDATED].items())

        self.logger.debug('Initializing data handler...')
        self.data_handler = self.get_data_handler(self.data_directory, self.model_params)

        # update batch_sizes to allow quicker iteration over sets
        new_batch_size = params['new_batch_size']
        if new_batch_size:
            self.data_handler.update_batch_sizes(
                {DATA: {'batch_size': new_batch_size, 'evaluation_batch_size': new_batch_size}})
        self.data_handler.initialize_loaders(fold_number=self.model_params[DATA][names.CHOSEN_FOLD])

        # log patterns and their processed versions for each target class
        self.data_handler.log_set_idxs(self.run_path)

        images_dir = os.path.join(self.run_path, 'patterns')
        check_dir(images_dir, create=True)
        if params['log_patterns']:
            self.data_handler.log_loader_pattern_examples(images_dir)

    def start(self):
        loaders = self.data_handler.get_loaders()
        for loader in loaders:
            if loader.name in self.loaders_to_analyse:
                self.logger.info(f'Creating data for {loader.name} loader')
                self.create_predictions(loader)

    def create_predictions(self, loader):
        """
        One model would have one HDFStore that would contain predicions, feature maps and target values
        :param loader:
        :return:
        """
        run_path = self.run_path
        storage_path = os.path.join(run_path, loader.name + names.PREDICTIONS + names.H5)

        # switch to eval mode to avoid updating running averages and running means of batchnorm
        self.model.eval()

        with pd.HDFStore(storage_path, mode='w') as store:
            with torch.no_grad():
                for i, batch in enumerate(tqdm(loader)):
                    # create augmented predictions and featuremaps
                    for modification in self._generate_all_image_modifications():
                        predicted_values, feature_maps, ids = self.get_modified_predictions(batch, **modification)

                        self.append_to_store(store, names.AnalysisStorageKeys.FEATURE_MAPS,
                                             self._create_df(feature_maps, ids, **modification))
                        self.append_to_store(store, names.AnalysisStorageKeys.PREDICTIONS,
                                             self._create_df(predicted_values, ids, **modification))

                    # save_targets
                    self.append_to_store(store, names.AnalysisStorageKeys.TARGETS,
                                         self._create_df(batch[LABEL], batch[ID]))
                    # create_noised
                    for noise_std in self.params[names.NOISE_STD]:
                        for repeat_number in range(self.params[names.NUMBER_OF_REPEATS]):
                            predicted_values, feature_maps, ids = self.get_modified_predictions(batch,
                                                                                                noise_std=noise_std)
                            prediction_df = self._create_df(feature_maps, ids, repeat_number=repeat_number)
                            feature_maps_df = self._create_df(feature_maps, ids, repeat_number=repeat_number)

                            # old version: storage_key_prefix = f'NOISE_STD_{str(noise_std)}__'
                            storage_key_prefix = 'NOISE_STD_' + f'{noise_std:.{10}f}'
                            self.append_to_store(store, storage_key_prefix + names.AnalysisStorageKeys.PREDICTIONS,
                                                 prediction_df)
                            self.append_to_store(store, storage_key_prefix + names.AnalysisStorageKeys.FEATURE_MAPS,
                                                 feature_maps_df)

                    assert len([k for k in store.keys() if k.startswith('/NOISE_STD_')]) / 2 == \
                           len(self.params[names.NOISE_STD]), \
                        'There is error in implementation. Different noise stds should have different storage keys.'

    def append_to_store(self, store: pd.HDFStore, key: str, df: pd.DataFrame):
        key = key.replace('.', '')
        store.append(key, df)

    def _generate_all_image_modifications(self):
        keys = self.params[names.TestTimeAugmentations.KEY_IN_PARAMETERS_FILE].keys()
        values = self.params[names.TestTimeAugmentations.KEY_IN_PARAMETERS_FILE].values()

        all_combinations = list(itertools.product(*values))

        list_of_configurations = []
        for combination in all_combinations:
            image_modification_configuration = {k: v for k, v in zip(keys, combination)}
            list_of_configurations.append(image_modification_configuration)

        return list_of_configurations

    def _create_df(self, values: torch.Tensor, indices, **modification: dict):
        df = pd.DataFrame(values.cpu().numpy())
        df[ID] = indices

        for key, value in modification.items():
            df[key] = value

        return df

    # def create_predictions_old(self, loader):
    #     run_path = self.run_path
    #     loader_name = loader.name
    #     number_of_classes = self.model_params[names.NUMBER_OF_CLASSES]
    #     number_of_patterns = len(loader.dataset)
    #     feature_map_dimension = self.model.num_ftrs
    #     mode = 'w+'
    #
    #     memmap_opening_arguments = (run_path, loader_name, number_of_classes, number_of_patterns,
    #                                 feature_map_dimension, mode)
    #
    #     loader_prediction_storage, \
    #     loader_feature_map_storage, \
    #     loader_correct_number_storage, \
    #     loader_class_predicted_counts_storage, \
    #     loader_target_storage, \
    #     loader_ids_storage = self.open_memmap_storages(*memmap_opening_arguments)
    #
    #     with open(os.path.join(run_path, names.MEMMAP_LOADING_ARGUMENTS), mode='w') as file:
    #         mem_load_args = {k: v for k, v in zip(names.MEMAP_LOADING_ARGUMENT_NAMES, memmap_opening_arguments)}
    #         json.dump(mem_load_args, file, indent=2)
    #
    #     current_id = 0
    #
    #     torch.cuda.empty_cache()
    #     self.model.eval()
    #     with torch.no_grad():
    #         for i, batch in enumerate(tqdm(loader)):
    #             predicted_values, ids, batch_size, targets = self.predict_batch(batch)
    #             feature_maps = self.predict_feature_map(batch)
    #
    #             loader_prediction_storage[current_id:current_id + batch_size, ...] = predicted_values.cpu().numpy()
    #             loader_feature_map_storage[current_id:current_id + batch_size, ...] = feature_maps.cpu().numpy()
    #             loader_ids_storage[current_id:current_id + batch_size] = ids.cpu().numpy()
    #             loader_target_storage[current_id:current_id + batch_size] = targets.cpu().numpy()
    #
    #             correctness_full = torch.zeros_like(ids, dtype=torch.int32, device=self.device)
    #             prediction_one_hot_full = torch.zeros((batch_size, number_of_classes), dtype=torch.int32,
    #                                                   device=self.device)
    #
    #             for i in range(self.params[names.NUMBER_OF_REPEATS]):
    #                 batch[names.NOISE] = self.params[names.NOISE_STD] * torch.randn_like(batch[IMAGE],
    #                                                                                      device=self.device) + \
    #                                      self.params[
    #                                          names.NOISE_MEAN]  # TODO: move to processors
    #
    #                 correctness, one_hot_predictions = self.get_noised_predictions(batch)
    #
    #                 correctness_full += correctness.type(torch.int32)
    #                 prediction_one_hot_full += one_hot_predictions.type(torch.int32)
    #
    #             loader_correct_number_storage[current_id:current_id + batch_size] = correctness_full.cpu().numpy()
    #             loader_class_predicted_counts_storage[current_id:current_id + batch_size,
    #             ...] = prediction_one_hot_full.cpu().numpy()
    #
    #             current_id += batch_size

    @collect_garbage_decorator
    def predict_batch(self, batch: dict):
        input_ = torch.tensor(batch[IMAGE], requires_grad=False, dtype=torch.float, device=self.device)
        return self.model(input_), batch[ID], batch[ID].numel(), batch[LABEL]

    @collect_garbage_decorator
    def predict_feature_map(self, batch: dict):
        input_ = torch.tensor(batch[IMAGE], requires_grad=False, dtype=torch.float, device=self.device)
        return self.model.get_flatten_feature_map(input_)

    @collect_garbage_decorator
    def get_noised_predictions(self, batch: dict):
        self.model.eval()

        input = torch.tensor(batch[IMAGE], requires_grad=False, dtype=torch.float, device=self.device)
        noise = torch.tensor(batch[names.NOISE], requires_grad=False, dtype=torch.float, device=self.device)
        target = torch.tensor(batch[LABEL], requires_grad=False, dtype=torch.long, device=self.device)

        prediction = self.model(input + noise)
        max_index = prediction.argmax(dim=1)

        target_one_hot_encoded = torch.zeros_like(prediction, dtype=torch.int32)
        target_one_hot_encoded.scatter_(1, max_index.unsqueeze(1), 1)

        return max_index == target, target_one_hot_encoded

    @collect_garbage_decorator
    def get_modified_predictions(self, batch: dict, flip_horizontally: bool = False, flip_vertically: bool = False,
                                 rotate_90: int = 0, noise_std: float = 0, **kwargs):
        initial_input = batch[IMAGE]  # saved to restore unmodified input on the end of this function

        input = torch.tensor(batch[IMAGE], requires_grad=False, dtype=torch.float, device=self.device)
        # from PIL import Image
        # im = Image.fromarray(np.moveaxis(torch.flip(input, (1,)).numpy()[0,...], 0, -1), mode='L')
        # im.save('ololo.jpeg')
        # import matplotlib.pyplot as plt
        # plt.imshow(np.moveaxis(torch.flip(input, (1,)).numpy()[0,...], 0, -1))
        # plt.savefig('ololo.png')
        # plt.close()

        if flip_horizontally:
            input = torch.flip(input, (2,))
        if flip_vertically:
            input = torch.flip(input, (3,))
        if rotate_90:
            input = torch.rot90(input, 1, [2, 3])
        if noise_std:
            noise = noise_std * torch.randn_like(input, device=self.device, dtype=input.dtype)
            input = input + noise

        batch[IMAGE] = input
        predicted_values, ids, batch_size, _ = self.predict_batch(batch)
        feature_maps = self.predict_feature_map(batch)

        batch[IMAGE] = initial_input
        return predicted_values, feature_maps, ids

    # @staticmethod
    # def open_memmap_storages(self, run_path: str, loader_name: str, number_of_classes: int,
    #                          number_of_patterns: int, feature_map_dimension: int, mode='r') -> 6 * (np.memmap,):
    #     """
    #     Model predictions would be stored in
    #     :param self:
    #     :param run_path:
    #     :param loader_name:
    #     :param number_of_classes:
    #     :param number_of_patterns:
    #     :param feature_map_dimension:
    #     :param mode:
    #     :return:
    #     """
    #     predictions_file = os.path.join(run_path, '_'.join([loader_name, names.PREDICTIONS]))
    #     targets_file = os.path.join(run_path, '_'.join([loader_name, names.TARGETS]))
    #     prediction_feature_map_file = os.path.join(run_path, '_'.join([loader_name, names.FEATURES]))
    #     prediction_ids_file = os.path.join(run_path, '_'.join([loader_name, names.PREDICTIONS, ID]))
    #     prediction_classes_file = os.path.join(run_path, '_'.join([loader_name, names.PREDICTED_CLASSES]))
    #     prediction_classes_counts_file = os.path.join(run_path,
    #                                                   '_'.join([loader_name, names.PREDICTED_CLASSES, 'counts']))
    #
    #     loader_prediction_storage = np.memmap(predictions_file, dtype='float32', mode=mode,
    #                                           shape=(number_of_patterns, number_of_classes))
    #     loader_feature_map_storage = np.memmap(prediction_feature_map_file, dtype='float32', mode=mode,
    #                                            shape=(number_of_patterns, feature_map_dimension))
    #     loader_correct_number_storage = np.memmap(prediction_classes_file, dtype='int32', mode=mode,
    #                                               shape=(number_of_patterns))
    #     loader_class_predicted_counts_storage = np.memmap(prediction_classes_counts_file, dtype='int32', mode=mode,
    #                                                       shape=(number_of_patterns, number_of_classes))
    #     loader_target_storage = np.memmap(targets_file, dtype='int32', mode=mode,
    #                                       shape=(number_of_patterns))
    #     loader_ids_storage = np.memmap(prediction_ids_file, dtype='int32', mode=mode, shape=(number_of_patterns))
    #
    #     return loader_prediction_storage, \
    #            loader_feature_map_storage, \
    #            loader_correct_number_storage, \
    #            loader_class_predicted_counts_storage, \
    #            loader_target_storage, \
    #            loader_ids_storage


@worker_registry.register
class PatchDDSMModelAnalyserWorker(SimpleModelAnalyser, DDSMLoader):
    worker_name = 'ddsm_analyser'


@worker_registry.register
class PatchDDSMModelAnalyserWorker(SimpleModelAnalyser, DDSMCleanedLoader):
    worker_name = 'ddsm_analyser'


@worker_registry.register
class MNISTModelAnalyserWorker(SimpleModelAnalyser, MNISTLoader):
    worker_name = 'mnist_analyser'


class SimpleTrainerWorker(BaseWorker, DataProvider, ABC):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        kwargs[names.TASK] = self.worker_name
        self.continue_training = True  # this variable should be used for the training interruption by the callbacks

        self.torch_sampling_seed = self.params[DATA][names.TORCH_SAMPLING_SEED]

    def load_dependencies(self) -> None:
        """
        Model, optimizer, metrics, data handler, callbacks
        :return:
        """
        check_dir(self.run_path, create=True)

        params = self.params

        self._model = model_registry.get_class(params['model'])(params)
        self._model.to(self.device)

        self.optimizer = get_optimizer(self._model.parameters(), params)

        self._train_metrics, self._validation_metrics, self._test_metrics = self._get_metrics(params)
        self._all_metrics_dict = {TRAIN: self._train_metrics, VALIDATION: self._validation_metrics,
                                  TEST: self._test_metrics}

        self.logger.debug('Initializing data handler...')
        self.data_handler = self.get_data_handler(self.data_directory, params)

        self.logger.debug('Initializing tensorboard logger...')
        self.tensorboard_logger = TensorboardLogger(experiment_dir_path=self.run_path)

        self.logger.debug('Initializing callbacks...')
        from src.models.callbacks import callback_registry
        callback_names = params[names.CALLBACK_NAMES]
        self.callbacks: List['Callback'] = [callback_registry.get(callback_name, self) for callback_name in
                                            callback_names]

        # log patterns and their processed versions for each label
        self.data_handler.log_set_idxs(self.run_path)
        images_dir = os.path.join(self.run_path, 'patterns')
        check_dir(images_dir, create=True)
        if params['log_patterns']:
            self.data_handler.log_loader_pattern_examples(images_dir)

    def _get_metrics(self, config: dict):
        """
        Creates metric instances from class names and for train and validation/test sets
        :return: train_metrics, validation_metrics, test_metrics
        """
        general_loss_parameters = {
            names.NUMBER_OF_CLASSES: config[names.NUMBER_OF_CLASSES],
            names.IMAGE_SHAPE: config[names.IMAGE_SHAPE],  # is this still needed?
            names.RUN_DIR_PATH: self.run_path,
            names.CLASS_NAMES: [str(i) for i in range(config[names.NUMBER_OF_CLASSES])]
        }

        def get_parameters(specific_loss_params, set_name: str):
            result = dict(specific_loss_params, **general_loss_parameters)
            result[names.SET_NAME] = set_name
            return result

        train_metrics = [metrics_registry.get(config[names.TRAIN_LOSS_FUNCTION][names.NAME], device=self.device,
                                              **get_parameters(config[names.TRAIN_LOSS_FUNCTION][PARAMS], TRAIN))]
        validation_metrics = [
            metrics_registry.get(loss[names.NAME], device=self.device, **get_parameters(loss[PARAMS], VALIDATION))
            for loss in config[names.EVALUATION_METRICS]]

        test_metrics = [
            metrics_registry.get(loss[names.NAME], device=self.device, **get_parameters(loss[PARAMS], TEST))
            for loss in config[names.EVALUATION_METRICS]]

        return train_metrics, validation_metrics, test_metrics

    def start(self):
        self._log_training_configuration()

        # Fixes order of random sampling of data by loaders
        set_torch_seed(self.torch_sampling_seed)

        # GET THE METRICS AND RUN CALLBACKS FOR THE NET BEFORE THE TRAINING
        for loader in self.data_handler.get_loaders():
            self._evaluate_metrics_for_loader(loader, self._all_metrics_dict[loader.name])
        self._call_callbacks()

        # TRAINING
        for epoch_number in range(self.params['number_of_epochs']):
            if not self.continue_training:
                break

            # train metrics are calculated while training epoch
            self.train_epoch()
            for loader in self.data_handler.get_loaders()[1:]:
                self._evaluate_metrics_for_loader(loader, self._all_metrics_dict[loader.name])

            # logging the metrics and saving best model
            self._call_callbacks()

            self._reset_metrics()

        # ON TRAINING END
        self._load_model('best')
        self.logger.info('\n{}\nRun finished.\n{}'.format(TRAILING_HASHTAG, TRAILING_HASHTAG))

        for set_loader in self.data_handler.get_loaders():
            metrics = self._evaluate_metrics_for_loader(loader=set_loader,
                                                        metrics=self._all_metrics_dict[set_loader.name])
            for metric in metrics:
                metric.get_and_log_results(tensorboard_logger=self.tensorboard_logger, console_logger=self.logger,
                                           tag='BEST_' + set_loader.name, abscissa=0)

        self.tensorboard_logger.close()

    @log_spent_time_decorator
    def train_epoch(self):
        torch.cuda.empty_cache()

        self._model.train()

        for batch in tqdm(self.data_handler._train_loader):
            self.optimizer.zero_grad()
            torch.cuda.empty_cache()

            batch_losses = self.predict_and_get_losses(batch, self._train_metrics)

            for loss_name, loss in batch_losses.items():  # TODO fix to be able to work for multiple losses on the train set
                loss.backward()

            self.optimizer.step()

            # setting it to None frees the memory
            del batch_losses
            del batch
            del loss

    @log_spent_time_decorator
    def _evaluate_metrics_for_loader(self, loader, metrics: Optional[List[MetricAbstract]]) -> List[MetricAbstract]:
        torch.cuda.empty_cache()

        self._model.eval()

        with torch.no_grad():
            for i, batch in enumerate(tqdm(loader)):
                self.predict_and_get_losses(batch, metrics)

        return metrics

    @log_spent_time_decorator
    def _call_callbacks(self):
        for callback in self.callbacks:
            callback.run()

    def _save_model(self, name='best'):
        file_path = os.path.join(self.run_path, name + CHECKPOINT_ENDING)
        check_dir(os.path.dirname(file_path), create=True)

        torch.save({
            'model_state_dict': self._model.state_dict(),
            PARAMS: self.params,
            names.CHOSEN_FOLD: self.params[DATA][names.CHOSEN_FOLD],
            names.NUMBER_OF_FOLDS: self.params[DATA][names.NUMBER_OF_FOLDS],
            # TODO: add git commit hash
            # TODO: add dataset hash
        }, file_path, pickle_module=dill)

        self.logger.info('Model with name `{}` saved'.format(name))

    def _load_model(self, name: str):
        file_path = os.path.join(self.run_path, name + '.checkpoint')
        self._model, _ = TorchModelLoader.load(file_path, self.device)
        self.logger.info('Model with name `{}` loaded'.format(name))

    @collect_garbage_decorator
    def predict_and_get_losses(self, batch: dict, metrics: List[MetricAbstract],
                               move_to_the_cpu: bool = False):
        """
        This is used to make sure memory allocations are done correctly.
        Data are moved to the self._device, then for each loss_function result is calculated.
        :param batch: dictionary object containing image and it's label
        :param metrics: list of functions that maps batch of predictions and target values to the one value
        :param move_to_the_cpu: when True, before returning the values are moved from the GPU
        :return: list containing results of computation of each of the loss functions loss value
        """
        input = torch.tensor(batch[IMAGE], requires_grad=False, dtype=torch.float, device=self.device)
        target = torch.tensor(batch[LABEL], requires_grad=False, dtype=torch.long, device=self.device)

        model_output = self._model(input)

        results = {}
        for metric in metrics:
            results[metric.name] = metric.accumulate(target=target, prediction=model_output, input_=batch)

        if move_to_the_cpu:
            for metric_name, result in enumerate(results.items()):
                results[metric_name] = result.item()

        return results

    def _log_metrics(self, metric_lists: List[List[MetricAbstract]] = None, names: List[str] = None,
                     epoch_number: int = None):
        """
        Goes through the metrics and their names one by one ang logs their values.
        There should be equal number of elements in metrics and names lists or the method raises an exception.
        :param metric_lists:
        :param names:
        :param epoch_number: If not none, the epoch number is also logged.
        :return:
        """
        if metric_lists is None:
            metric_lists = [self._train_metrics, self._validation_metrics, self._test_metrics]
            if names is None:
                names = [TRAIN, VALIDATION, TEST]

        if epoch_number is not None:
            self.logger.info('\n{}\nEpoch {}:\n{}'.format(TRAILING_HASHTAG, epoch_number, TRAILING_HASHTAG))

        for metrics, name in zip(metric_lists, names):
            for metric in metrics:
                metric.get_and_log_results(self.tensorboard_logger, self.logger, tag=TRAIN, abscissa=epoch_number)

    def _log_training_configuration(self):
        """
        Logs parameters, model and data loaders to the console and file
        :return:
        """
        self.logger.debug('\n' + TRAILING_HASHTAG + '\nTraining run configurations:\n')
        log_config(self.logger, self.params)
        log_model(self.logger, self._model)
        for loader in self.data_handler.get_loaders():
            log_data_set(self.logger, loader)

        # save configurations to the pickle file
        new_config_path = os.path.join(self.run_path, CONFIG_FILE_NAME)
        json.dump(self.params, open(new_config_path, 'w'), indent=2)

    def _reset_metrics(self):
        for metric_list in [self._train_metrics, self._validation_metrics, self._test_metrics]:
            for metric in metric_list:
                metric.reset()


@worker_registry.register
class PatchDDSMTrainerWorker(SimpleTrainerWorker, DDSMCleanedLoader):
    worker_name = 'patch_ddsm_trainer'


@worker_registry.register
class PatchDDSMOldTrainerWorker(SimpleTrainerWorker, DDSMLoader):
    worker_name = 'patch_ddsm_old_trainer'

@worker_registry.register
class MNISTTrainerWorker(SimpleTrainerWorker, MNISTLoader):
    worker_name = 'mnist_trainer'


@worker_registry.register
class PatchDDSMEntropyMinWorker(PatchDDSMTrainerWorker):
    def load_dependencies(self) -> None:
        super(PatchDDSMEntropyMinWorker, self).load_dependencies()

        self.unsupervised_is_on = True
        self.supervised_is_on = True
        self.unsupervised_weight = self.params[names.UNSUPERVISED_WEIGHT]

        config = self.params
        general_loss_parameters = {
            names.NUMBER_OF_CLASSES: config[names.NUMBER_OF_CLASSES],
            # names.IMAGE_SHAPE: config[names.IMAGE_SHAPE],  # is this still needed?
            names.RUN_DIR_PATH: self.run_path,
            names.CLASS_NAMES: [str(i) for i in range(config[names.NUMBER_OF_CLASSES])]
        }

        def get_parameters(specific_loss_params, set_name: str):
            result = dict(specific_loss_params, **general_loss_parameters)
            result[names.SET_NAME] = set_name
            return result

        self._entropy_minimization_metrics = [metrics_registry.get(config[names.ENTROPY_LOSS_FUNCTION][names.NAME], device=self.device,
                                                                   **get_parameters(config[names.ENTROPY_LOSS_FUNCTION][PARAMS], TEST))]

    def supervised_train(self):
        super(PatchDDSMEntropyMinWorker, self).train_epoch()

    def unsupervised_train(self):
        torch.cuda.empty_cache()

        self._model.train()

        for batch in tqdm(self.data_handler._augmented_test_loader):
            self.optimizer.zero_grad()
            torch.cuda.empty_cache()

            batch_losses = self.predict_and_get_losses(batch, self._entropy_minimization_metrics)

            for loss_name, loss in batch_losses.items():  # TODO fix to be able to work for multiple losses on the train set
                (self.unsupervised_weight * loss).backward()

            self.optimizer.step()

            # setting it to None frees the memory
            del batch_losses
            del batch
            del loss



    @log_spent_time_decorator
    def train_epoch(self):
        if self.unsupervised_is_on:
            self.unsupervised_train()

        if self.supervised_is_on:
            self.supervised_train()

class DataSetModifier(BaseWorker, DDSMCleanedLoader):
    """
    This worker is intended to preproces data set before using it in training.
    It can save time that is usually spent on processing while creating batch of data.

    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.scale = kwargs['scale']

    def load_dependencies(self) -> None:
        """
        Model, optimizer, metrics, data handler, callbacks
        :return:
        """
        self.logger.debug('Initializing data handler...')
        self.data_handler = self.get_data_handler(self.data_directory, self.params, initialize_loaders=False)

    def start(self):
        assert self.data_handler.train_data.shape[-2:] == self.data_handler.test_data.shape[-2:], \
            'Test data differs from train data.'

        # self.run_path
        data = [self.data_handler.train_data, self.data_handler.test_data]
        labels = [self.data_handler.train_labels, self.data_handler.test_labels]
        metadata = [self.train_metafeatures, self.test_metafeatures]
        set_names = [TRAIN, TEST]

        for (set_name, set_images, set_labels, set_metadata) in zip(set_names, data, labels, metadata):
            new_shape = (set_images.shape[0],
                         int(set_images.shape[1] * self.scale), int(set_images.shape[2] * self.scale))

            images_path = PatchDatasetFileNames.data_for(set_name, self.run_path)
            labels_path = PatchDatasetFileNames.label_for_set(set_name, self.run_path)
            metadata_path = PatchDatasetFileNames.metadata_for(set_name, self.run_path)

            save_data_frame_as_hdf(set_metadata, metadata_path)

            images_memmap = np.memmap(images_path, dtype='uint8', mode='w+',
                                      shape=new_shape)
            labels_memmap = np.memmap(labels_path, dtype='uint8', mode='w+',
                                      shape=(new_shape[0]))

            labels_memmap[:] = set_labels[:]

            self._fill_memmap_with_modified_images(source=set_images, destination=images_memmap)

    def _fill_memmap_with_modified_images(self, source: np.memmap, destination: np.memmap):
        for i, image in tqdm(enumerate(source)):
            destination[i, ...] = self._process_image(image)

    def _process_image(self, image: np.ndarray):
        new_shape = (int(image.shape[0] * self.scale), int(image.shape[1] * self.scale))
        return cv2.resize(image, new_shape, interpolation=cv2.INTER_AREA)

@worker_registry.register
class ModelBatchSizeAndEvaluationTimeAanlyser(PatchDDSMTrainerWorker):
    worker_name = 'max_batch_size_search_and_stats'

    def start(self):
        # self.load_dependencies()
        self.model_name__ = self.params['model']
        self.parameters_number__ = sum([p.numel() for p in self._model.parameters()])
        self.batch_size__ = self._find_best_batch_size()
        self.evaluation_time__ = self._find_one_evaluation_time()
        self.training_time__ = self._find_one_training_epoch_time()
        self.scale__ = [tp for tp in self.params[names.TRAIN_PROCESSORS] if tp['name'] == 'resize_image'][0][PARAMS]['scale']

        res = {
            'image_size': self._get_image_side_size(),
            'batch_size': self.batch_size__,
            'training_time': self.training_time__,
            'eval_time': self.evaluation_time__,
            'model_parameters': self.parameters_number__,
            'scale': self.scale__,
            'model_name': self.params['model'],
        }

        print('\n', 'best_batch_size', self.batch_size__, '\n')
        print('scale', self.scale__, '\n\n')

        pd.DataFrame(res, index = [0]).to_csv(os.path.join(self.run_path, 'metrics.csv'))

        self.tensorboard_logger.close()
        del self.tensorboard_logger
        del self._model
        del self.optimizer
        del self.data_handler

        del self._all_metrics_dict
        del self._train_metrics
        del self._validation_metrics
        del self._test_metrics

        if self.device.type != 'cpu':
            torch.cuda.empty_cache()
            torch.cuda.synchronize('cuda')
            torch.cuda.empty_cache()

        gc.collect()

    def try_train(self, batch_size):
        try:
            self._reset_metrics()
            if self.device.type != 'cpu':
                torch.cuda.empty_cache()
            import gc
            gc.collect()
            self.optimizer.zero_grad()

            self.data_handler.update_batch_sizes(
                {DATA: {'batch_size': batch_size, 'evaluation_batch_size': batch_size}})
            self.data_handler.initialize_loaders(fold_number=self.params[DATA][names.CHOSEN_FOLD])

            self.train_epoch()
            self._reset_metrics()

            return True
        except:
            return False

    def _find_best_batch_size(self) -> (int, float):
        batch_size = 2
        final_batch_size = self.params.get('final_batch_size', 40)
        previous_try_succeded = False

        while batch_size < final_batch_size:
            succeeded = self.try_train(batch_size)

            if succeeded:
                batch_size += 1
                previous_try_succeded = True
            else:
                if previous_try_succeded:
                    batch_size -= 1
                else:
                    batch_size = 0
                break

            self._reset_metrics()

        return batch_size

    def _find_one_training_epoch_time(self) -> float:
        """
        Evaluates time spent on one training epoch with same batch size
        :return:
        """
        if not self.batch_size__:
            return 0

        self._reset_metrics()
        if self.device.type != 'cpu':
            torch.cuda.empty_cache()

        self.data_handler.update_batch_sizes(
            {DATA: {'batch_size': 2, 'evaluation_batch_size': 2}})
        self.data_handler.initialize_loaders(fold_number=self.params[DATA][names.CHOSEN_FOLD])

        if self.device.type != 'cpu':
            torch.cuda.synchronize('cuda')
        init_time = time.time()

        if self.batch_size__ == 0:
            return 0

        self.train_epoch()
        self._reset_metrics()

        self.train_epoch()
        self._reset_metrics()

        if self.device.type != 'cpu':
            torch.cuda.synchronize('cuda')
        res = (time.time() - init_time) / 2

        return res

    def _find_one_evaluation_time(self):
        if self.batch_size__ == 0:
            return 0

        self.data_handler.update_batch_sizes(
            {DATA: {'batch_size': 2, 'evaluation_batch_size': 2}})
        self.data_handler.initialize_loaders(fold_number=self.params[DATA][names.CHOSEN_FOLD])

        train_loader = self.data_handler.get_loaders()[0]

        if self.device.type != 'cpu':
            torch.cuda.empty_cache()
            torch.cuda.synchronize('cuda')
        init_time = time.time()

        self._evaluate_metrics_for_loader(train_loader, self._all_metrics_dict[train_loader.name])
        self._reset_metrics()

        self._evaluate_metrics_for_loader(train_loader, self._all_metrics_dict[train_loader.name])
        self._reset_metrics()

        if self.device.type != 'cpu':
            torch.cuda.synchronize('cuda')
        res = (time.time() - init_time) / 2

        return res

    def _get_image_side_size(self) -> int:
        for batch in tqdm(self.data_handler._train_loader):
            img = batch['image'][0]
            return max(img.shape)